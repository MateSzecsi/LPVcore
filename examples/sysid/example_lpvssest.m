clearvars; close all; clc; rng(1);

p = preal('p', 'dt');

A = 0.1 + 0.1 * p;
B = 1;
C = 1;
D = 1;
K = 0;
NoiseVariance = 0.01;
Ts = 1;

template_sys = lpvidss(A, B, C, D, 'innovation', K, [], NoiseVariance,Ts);

N = 10;
u = randn(N, template_sys.Nu);
p = rand(N, template_sys.Np);
y = lsim(template_sys, p, u);
data = lpviddata(y, p, u, template_sys.Ts);

initSearchMethod = 'gradient';

sys = lpvssest(data, template_sys, ...
    lpvssestOptions(...
        'Display', 'on'));

compare(data, sys);







