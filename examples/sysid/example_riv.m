close all; clearvars; clc; rng(1);
%% Refined-IV method for identification of LPV Box-Jenkins models
%
% This example script demonstrates the functionality of the LPVRIV command,
% which implements the identification approach proposed in:
%
%   [1] "Refined instrumental variable methods for identification of LPV
%   Box-Jenkins models" by V. Laurain et al. (2010)
%

%% Simulation example 1: SISO
% This simulation example is described in [1, Section 5].
fprintf('---- Simulation example 1 ----\n');
p = preal('p', 'dt');
a1 =   1 - 0.5 * p - 0.1 * p^2;
a2 = 0.5 - 0.7 * p - 0.1 * p^2;
b0 = 0.5 - 0.4 * p + 0.01 * p^2;
b1 = 0.2 - 0.3 * p - 0.02 * p^2;
sys = lpvidpoly([], {b0, b1}, {1}, {1}, {1, a1, a2});
% sys.NoiseVariance = 0.0008;   % 15 dB
% sys.NoiseVariance = 0.003;   % 10 dB
% sys.NoiseVariance = 0.009;   % 5 dB
sys.NoiseVariance = 0.026;   % 0 dB
rho0 = getpvec(sys, 'free');
% Simulation data
Nmc = 10;  % Number of Monte Carlo trials
N = 4000; % Number of time step per trial
rho = NaN(nparams(sys, 'free'), Nmc);
for i=1:Nmc
    rng(i);
    fprintf('Monte Carlo trial %i / %i... ', i, Nmc); tic;
    k = linspace(0, N-1, N).';
    p = 0.5*sin(0.35*pi*k) + 0.5;
    u = rand(N, 1);
    [y, ~, e, yp] = lsim(sys, p, u, k);
    fprintf('SNR is %.2f dB... ', LPVcore.snr(yp, e));
    
    data = lpviddata(y, p, u);
    template_sys = sys;
    
    sys_est = lpvriv(data, template_sys, ...
        lpvrivOptions(...
        'Implementation', 'lpvcore', ...
        'Initialization', 'template', ...
        'Focus', 'prediction', ...
        'MaxIterations', 10, ...
        'Display', 'on'));
    rho_est = getpvec(sys_est, 'free');
    % Only store the values if rho ~= 0 (otherwise, algorithm did not
    % converge)
    if norm(rho_est) ~= 0
        rho(:, i) = rho_est;
    end
    toc;
end

figure;
compare(data, sys_est, 1);
figure;
compare(data, sys_est, Inf);

% Remove NaN columns as they did not converge
rho = rho(:, ~isnan(rho(1, :)));

BN = norm(rho0 - mean(rho, 2));
VN = norm(mean(rho - mean(rho, 2)));
fprintf('Converged runs: %d / %d\n', size(rho, 2), Nmc);
fprintf('Bias Norm     BN = %.4f\n', BN);
fprintf('Variance Norm VN = %.4f\n', VN);

%% Simulation example 2: MIMO 
fprintf('---- Simulation example 2 ----\n');
% IO size
ny = 2;
nu = 2;
% Scheduling signal
p = preal('p', 'dt');
% Process part
B = {randn(ny, nu) + p * randn(ny, nu)};
F = {eye(ny), 0.1 * eye(ny) + p * diag([0.1, -0.05])};
% Noise part (must be scheduling-independent)
C = eye(ny);
D = eye(ny);
% Create LPVIDPOLY object
NoiseVariance = 1;
Ts = 1;
sys = lpvidpoly([], B, C, D, F, NoiseVariance, Ts, 'ZeroIsNonFree', true);
sys.OutputName = {'yA', 'yB'};
% Construct template system object from which scheduling dependency is extracted 
% (alternatively, an ARX pre-initialization is done by LPVRIV to find
% initial values for the iterative process).
template_sys = sys;

%% Estimation
% Create dataset for estimation
N = 100;  % num. time samples
u = randn(N, sys.Nu);
p = rand(N, sys.Np);
[y, t, ~, yNoiseFree] = lsim(sys, p, u);
fprintf('SNR is %.2f dB\n', LPVcore.snr(y, yNoiseFree - y));

max_iter = 4;
data = lpviddata(y, p, u);
[sys_est, pvec, theta] = lpvriv(data, template_sys, ...
    lpvrivOptions(...
        'Implementation', 'lpvcore', ...
        'Initialization', 'polypre', ...
        'Display', 'on', ...
        'MaxIterations', max_iter));

%% Validation
u_val = randn(N, sys.Nu);
p_val = rand(N, sys.Np);
y_val = lsim(sys, p_val, u_val, [], zeros(size(y)));
data_val = lpviddata(y_val, p_val, u_val);
% Simulation error
figure;
compare(data_val, sys_est); grid on;