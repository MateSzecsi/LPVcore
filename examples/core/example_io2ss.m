clearvars; close all; clc;

%% Example of lpvioreal function (input-output form to state-space form conversion)
% For applications such as control synthesis, state-space forms are often
% required. To convert input-output forms into state-space forms, the
% LPVIOREAL function can be used to create a state-space realization of a
% given input-output model. The input-output model must be either:
%   * An LPVIO object
%   * An LPVIDPOLY object representing an LPV-OE (Output Error) model
%       structure
% For more information on the syntax and algorithm, type 'doc LPVIOREAL'
% Note that the realization is generally non-minimal, as will be
% illustrated by this example. Thus, a realization should typically be
% followed by a reduction step (using the LPVMODRED function which will be
% released in the near future).

%% Step 1: create input-output form
% Create a discrete-time scheduling signal
% Only discrete-time models are supported
p = preal('p', 'dt');
% Create input and output polynomials
% Note that the max. input shift is higher than the max. output shift (Nb >
% Na). This is automatically handled in LPVIOREAL by appending additional
% 0s to A.
A = {1, p, p^2};
B = {1, 0, 0   ,p + 1};
Ts = 2;  % seconds
% Signal properties specified, such as input and output name, are preserved
% after realization
sysio = lpvio(A, B, Ts, 'InputName', 'w', 'OutputName', 'z');

%% Step 2: realization of state-space form
sysss = lpvioreal(sysio);

%% Step 3: check minimality
% The resulting state-space model is non-minimal. Note that the state-space
% model has an LPV-LFR form (all LPVCORE.LPVSS objects are stored 
% internally as LPVLFR objects). In this case, a necessary condition for
% state-minimality is that the LTI block of the LPV-LFR form is
% state-minimal. This can be achieved by running MINREAL directly on the
% LPVCORE.LPVSS object
disp(sysss.Nx);
sysss = minreal(sysss);  % "1 state removed"
disp(sysss.Nx);

%% Step 4: verify equivalent input-output behavior
% Through a simulation, the equivalent input-output behavior of sysio and
% sysss will be demonstrated
% Nr. of time steps
N = 100;
% Input data
u = rand(N, 1);
p = rand(N, 1);
[yio, t] = lsim(sysio, p, u);
yss = lsim(sysss, p, u);
figure;
plot(t, yio, 'k-'); hold on;
plot(t, yss, 'k--');
plot(t, yio - yss, 'k-*');
xlabel('Time (s)'); ylabel('Output'); legend('LPVIO', 'LPVSS', 'Difference'); grid on;
