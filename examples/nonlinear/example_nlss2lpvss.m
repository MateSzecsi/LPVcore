% Example script: Embedding NL state-space rep. of unbalanced disk in an 
% LPV state-space representation.
clear; close all;

%% Unbalanced disk NL state-space rep.
par = load("parameters.mat");

nlsys = LPVcore.nlss(@(x,u) ubdiskFun(x, u, par), @(x,u) x(1), 2, 1, 1, 0);

%% Embedding/conversion to lpvss rep.
% Standard option: The integral is computed numerically for the embedding
% (part of the scheduling map) and each (nonlinear) element of the matrix
% functions is taken as scheduling-variable (except when multiple elements
% are the same function).
[lpvsys1,eta1] = LPVcore.nlss2lpvss(nlsys);

fprintf('Default options (lpvsys1 & eta1):\n\n');
fprintf('lpvsys1.A=\n\n');
display(lpvsys1.A);
fprintf('eta1.map=\n\n');
display(eta1.map);

% Different option: The integral is computed analytically for the embedding
% and each nonlinear element is factorized (somewhat) to obtain a simpeler
% scheduling map (compared to taking the whole element of the matrix as
% scheduling variable).
[lpvsys2,eta2] = LPVcore.nlss2lpvss(nlsys,"analytical","factor");

fprintf('----------------------------------------------------\n')
fprintf('Alternative options (lpvsys2 & eta2):\n\n');
fprintf('lpvsys2.A=\n\n');
display(lpvsys2.A);
fprintf('eta2.map = :\n\n');
display(eta2.map);

%%
function dx = ubdiskFun(x, u, p)
    theta = x(1);
    omega = x(2);

    thetadot = omega;
    omegadot = p.M*p.g*p.l/p.J*sin(theta) - 1/p.tau*omega + p.Km/p.tau*u;

    dx = [thetadot; omegadot];
end