% Example script: Differential form construction and available methods
clear; close all;

% Nonlinear state-space representation
sys = LPVcore.nlss(@(x,u) [x(2); -x(1)-x(2)^3 + u], ...
                   @(x,u) x(2)^2 + u^2, 2, 1, 1)

% Construct differential form
dfsys = LPVcore.difform(sys)

% By default dfsys.A, dfsys.B, etc. depends on both (the full) x and u. The
% real dependency can be checked by calling dependency
dep = dependency(dfsys)

% It can be seen that the A-matrix depends on x2, the C-matrix on
% x2, and the D-matrix on u1

% We can simplify the differential form such that dfsys.A, dfsys.B, etc.
% also only depend on x2 and u1, by calling simplify.
dfsys2 = simplify(dfsys)

% By default simplify ensures that each (nonlinear) matrix functions
% depends on the same inputs, i.e., in this example, even though A-matrix
% only depends on x2, it still takes (x2, u1) as input (like C and D. 
% By calling simplify with the option 'full', each matrix function is fully
% simplified. Such that the A-matrix now only requires x2 as input (and for
% this example the D-matrix now only requires u1 as input).
dfsys3 = simplify(dfsys,'full')