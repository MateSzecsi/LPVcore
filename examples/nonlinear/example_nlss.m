%% Example of LPVcore.nlss class
clear; close all;

%% System creation
% Continuous-time (driven) Van der Pol Oscillator 
f = @(x, u) [-x(2); x(1) - 0.3 * (1 - x(1)^2) * x(2) + u];
h = @(x,u) x(1);

nx = 2;
nu = 1;
ny = 1;

sys = LPVcore.nlss(f, h, nx, nu, ny);

% Discrete-time nonlinear system
f2 = @(x, u) [-0.5 * x(2) + u(1); 0.9 * sin(x(1)) + u(2)];
h2 = @(x,u) [x(1); x(2)];

nx2 = 2;
nu2 = 2;
ny2 = 2;
Ts2 = 0.1;

sys2 = LPVcore.nlss(f2, h2, nx2, nu2, ny2, Ts2);

% Continuous-time using non-differntiable function
f3 = @(x, u) [-interp1([0 1 2 3], [1 3 4 9], x(1, :)); ...
              -x(1, :) - x(2, :) + u(1, :)];
h3 = @(x,u) x(1, :);

nx3 = 2;
nu3 = 1;
ny3 = 1;

sys3 = LPVcore.nlss(f3, h3, nx3, nu3, ny3, 0, false);

%% Methods
%% C2D
% conversion from continous-time to discrete-time nonlinear system
f = @(x, u) [-x(2); x(1) - 0.3 * (1 - x(1)^2) * x(2) + u];
h = @(x,u) x(1);

nx = 2;
nu = 1;
ny = 1;

sys = LPVcore.nlss(f, h, nx, nu, ny);

sysdt1 = c2d(sys, 0.1, 'eul');      % Using (forward) Euler discretization
sysdt2 = c2d(sys, 0.1, 'rk4');      % Using 4th order Runge-Kutta method

%% SIM
% Simulation of nonlinear system
f = @(x, u) [-x(2); x(1) - 0.3 * (1 - x(1)^2) * x(2) + u];
h = @(x,u) x(1);

nx = 2;
nu = 1;
ny = 1;

sys = LPVcore.nlss(f, h, nx, nu, ny);

x0 = [1;0];
u = @(t) sin(2 * pi * t);
tspan = [0, 10];

[y, tout, x] = sim(sys, u, tspan, x0);

figure;
plot(tout, y); grid on;
xlabel("Time t"); ylabel("Output y");

% Simulation of nonilnear system with input given as vector
t = linspace(0, 10, 1e2)';
u = sin(0.1 * pi * t);
x0 = [1;0];

[y, tout, x] = sim(sys, u, t, x0);

figure;
plot(tout, y); grid on;
xlabel("Time t"); ylabel("Output y");
