% Example script: Embedding NL state-space rep. of (driven) Van der Pol 
% oscillator in an (grid-based) LPV state-space representation.
% LPV representation.
clear; close all;

%% (Driven) Van der Pol Oscillator 
sys = LPVcore.nlss(@(x,u) [-x(2); x(1)-.3*(1-x(1)^2)*x(2) + u], ...
                   @(x,u) x(1), 2, 1, 1);

%% Create grid
grid = struct('x1', -1:.2:1, 'x2', -.5:.1:.5, 'u', 0);

%% Obtain lpvgridss
lpvsys = LPVcore.nlss2lpvgridss(sys, grid);

%% Compute L2-gain
gam = lpvnorm(lpvsys);