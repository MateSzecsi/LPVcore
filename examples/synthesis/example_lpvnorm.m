%% In these examples, we show the LPVNORM function
clear; close all; clc;

%% Example 1: Induced L2-gain analysis
%
% Induced L2-gain analysis of mass-spring-damper system with varying
% spring constant.

%
% Mass-spring damper system:
%
%                  ┆┈┈▶ x(t), F(t)
%   ╲│             ┆
%   ╲│    k   ┌─────────┐ 
%   ╲│─╱╲╱╲╱╲─│         │
%   ╲│        |    m    |
%   ╲│──[| ]──│         │
%   ╲│    d   └─────────┘
%   ╲│
%
%   (varying) spring constant:  k       [N/m]
%   damping constant:           d       [Ns/m]
%   mass:                       m       [kg]
%   position:                   x       [m]
%   input force:                F       [N]
%
% Differential equation:
%   ẍ = - k*x/m - d*ẋ/m + F/m
%

fprintf([repelem('-',76),'\n'])
fprintf('Example 1:\n')  
fprintf(['Induced L2-gain analysis of mass-spring-damper system\n', ...
         'varying spring constant\n\n']);

% Parameters
k  = preal('k', 'ct', 'Range', [1 12]);  % We assume 1 <= k <= 12
d  = 2;
m  = 0.5;

% Create LPV model
A = [0 1;-k/m -d/m];
B = [0;1/m];
C = [1 0];
D = 0;

sys = LPVcore.lpvss(A,B,C,D);

% Compute induced L2-gain
[gamma,X] = lpvnorm(sys,'l2');

% Showing results
fprintf('\nPrinting results:\n\n')
fprintf('L2-gain: %.3g\n\n',gamma)

fprintf('Storage/Lyapunov function:\n\n')
fprintf(['\t       [ %.3f  %.3f ] [ x ]\n',...
         '\t[x  ẋ] [ %.3f  %.3f ] [ ẋ ]\n'],X(1),X(2),X(3),X(4))

%% Example 2: Induced L2-gain analysis with known parameter rate variation
%
% Induced L2-gain analysis of mass-spring-damper system with varying
% spring constant. (See also Example 1)
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 2:\n')
fprintf(['Induced L2-gain analysis of mass-spring-damper with known\n', ...
    'parameter rate variation\n\n'])   

% parameters
k  = preal('k', 'ct', 'Range', [1 12],...       % We assume 1 <= k <= 12
                      'RateBound', [-8 8]);     % We assume -8 <= k̇ <= 8
d  = 2;
m  = 0.5;

% Create LPV model
A = [0 1;-k/m -d/m];
B = [0;1/m];
C = [1 0];
D = 0;

sys = LPVcore.lpvss(A,B,C,D);

% Compute induced L2-gain
[gamma,X] = lpvnorm(sys,'l2');

% Showing results
fprintf('\nPrinting results:\n\n')
fprintf('L2-gain: %.3g\n\n',gamma)

fprintf('Storage/Lyapunov function:\n\n')
fprintf(['\t         ( [ %.3f  %.3f ]   [ %.3f   %.3f ]   ) [  x   ]\n',...
         '\t[x  xdot]( [ %.3f  %.3f ] + [ %.3f  %.3f ]*k ) [ xdot ]\n'],...
         X.matrices(1),X.matrices(2),X.matrices(5),X.matrices(6),...
         X.matrices(3),X.matrices(4),X.matrices(7),X.matrices(8));

%% Example 3: Passivitiy analysis
%
% Passivitity analysis of RLC circuit with varying resistor.
%
% RLC circuit:
%
%    I(t) ─▶     R            L
%   ──────────╱╲╱╲╱╲╱╲─────@@@@@@@───────┐
%   +                                    |     +
%    Vi(t)                              ─── C    Vc(t)
%                                       ───
%   -                                    |     -
%   ─────────────────────────────────────┘
%
%   (Varying) Resistance:   R       [Ohm]
%   Inductance:             L       [H]
%   Capacitance:            C       [F]
%   Capacitor voltage:      Vc      [V]
%   Current:                I       [A]
%   Input voltage:          Vi      [V]
%
% Differential equations:
%   İ  = -R*I/L - Vc/L + Vi/L
%   V̇c = I/C
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 3:\n')
fprintf('Passivity analysis of RLC circuit with varying resistor\n\n')

% Parameters
R = preal('R', 'ct', 'Range', [100 200]);    % We assume 100 <= R <= 200
Lgrid = 0.5;
C = 0.1;

% Create LPV model
A = [-R/Lgrid, -1/Lgrid; 1/C, 0];
B = [1/Lgrid;0];
C = [1 0];
D = 0;

sys = LPVcore.lpvss(A,B,C,D);

% Check if system is passive
[out,X] = lpvnorm(sys,'passive');

% Showing results
fprintf('\nPrinting results:\n\n')
if out; fprintf('Passive: true\n\n');else; fprintf('Passive: false\n\n');end

fprintf('Storage/Lyapunov function:\n\n')
fprintf(['\t        [ %.3f  %.3f ] [ i  ]\n',...
         '\t[i  Vc] [ %.3f  %.3f ] [ Vc ]\n'],X(1),X(2),X(3),X(4))

%% Example 4: Discrete-time LPV model analysis
%
% Generalized H2 analysis of discrete-time LPV system with affine
% scheduling dependency.
% 
% x(k+1) = A(p) x(k) + B(p) u(k);
%   y(k) = C(p) x(k);
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 4:\n')
fprintf('Generalized H2 analysis of discrete-time LPV system\n\n')

% parameters
p1 = preal('p1','dt','Range',[-3 -2]);
p2 = preal('p2','dt','Range',[-1 2]);

% LPV system
A = [-0.1*p1, 0.2;
     -0.3*p2, 0.8];
B = [0.2*p1+p2;
     0.5];
C = [1+p1 -p2];
D = 0;

Ts = 0.1;   % Sampling-time of system

sys = LPVcore.lpvss(A,B,C,D,Ts);

% Compute generalized H2 norm
[gamma,X] = lpvnorm(sys,'h2');

% Showing results
fprintf('\nPrinting results:\n\n')
fprintf('Generalized H2 norm: %.3g\n\n',gamma)

fprintf('Storage/Lyapunov function:\n\n')
fprintf(['\t         [ %.3f  %.3f ] [ x1 ]\n',...
         '\t[x1  x2] [ %.3f  %.3f ] [ x2 ]\n'],X(1),X(2),X(3),X(4))

%% Example 5: Linf analysis of grid-based model
%
% Linf analysis of grid-based LPV model of container crane load swing
% model. 
% 
% Model from: Hoffman, Radisch, Werner, "Active Damping of Container
% Crane Load Swing by Hoisting Modulatoin - An LPV Approach," CDC 2012.
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 5:\n')
fprintf(['Linf analysis of grid-based LPV model of container crane load',...
        'swing model.\n\n'])

% Parameters
tau = 0.1;
d = 1.4;
g = 9.81; 

% System model functions
Afun = @(phi,phidot,L) [0,                  1, 0, 0;
                        -g*sincFun(phi)/L, -d, 0, -2*phidot/L;
                        0,                  0, 0, 1;
                        0,                  0, 0, -1/tau];
B = [0;
     0;
     0;
     1/tau];
C = [1 0 0 0];
D = 0;

% Sampling grid
phiGrid = [-pi/4, 0, pi/4];
phidotGrid = [-0.25, 0, 0.25];
Lgrid = [3, 7.5, 12];

grid.phi = phiGrid;
grid.phidot = phidotGrid;
grid.L = Lgrid;

% Sample A-matrix
for i = 1:numel(phiGrid)
    for j = 1:numel(phidotGrid)
        for k = 1:numel(Lgrid)
            A(:,:,i,j,k) = Afun(phiGrid(i),phidotGrid(j),Lgrid(k));
        end
    end
end

% Create sampled model
sysSample = ss(A,B,C,D);

% Create gridded LPV model
sys = lpvgridss(sysSample,grid);

% Compute induced Linf norm
[gamma,X] = lpvnorm(sys,'linf');

% Showing results
fprintf('\nPrinting results:\n\n')
fprintf('Linf gain: %.3g\n\n',gamma)

fprintf('Storage/Lyapunov function:\n\n')
fprintf(['                      [ %.3f\t%.3f\t%.3f\t%.3f\t] [  phi   ]\n',...
         '                      [ %.3f\t%.3f\t%.3f\t%.3f\t] [ phidot ]\n',...
         '                      [ %.3f\t%.3f\t%.3f\t%.3f\t] [   L    ]\n',...
         '  [phi phidot L Ldot] [ %.3f\t%.3f\t%.3f\t%.3f\t] [  Ldot  ]\n'],...
         X(1),X(2),X(3),X(4),X(5),X(6),X(7),X(8),X(9),X(10),X(11),...
         X(12),X(13),X(14),X(15),X(16));

% Bonus: analysis with parameter-varying storage
phi = preal('phi', ...
    'Range', [phiGrid(1), phiGrid(2)], ...
    'RateBound', [phidotGrid(1) phidotGrid(2)]);
L = preal('L', ...
    'Range', [Lgrid(1), Lgrid(2)], ...
    'RateBound', [-0.1 0.1]);
storageTemplate = 1 + phi + phi^2 + L;
[gamma2,X] = lpvnorm(sys,'linf', 'ParameterVaryingStorage', storageTemplate);

fprintf('Linf gain with paramter-varying storage: %.3g\n\n',gamma2)

%% Local functions
%% sinc function
function  y = sincFun(x)
    if x == 0
        y = ones(numel(x),1);
    else
        y = sin(x)./x;
    end
end