%% In these examples, we show the LPVSYN function
clear; close all; clc;

%% Example 1: L2-gain optimal LPV controller design
%
% In this example we're going to design an L2-gain optimal LPV controller 
% for a mass-spring-damper (MSD) system with varying spring constant, see 
% also Example 1 in example_lpvnorm.m. The objective for the controller
% will be to track (constant) references and  reject (constant) 
% disturbances.
%

fprintf([repelem('-',76),'\n'])
fprintf('Example 1:\n')  
fprintf(['L2-gain optimal LPV controller synthesis for mass-spring-', ...
    'damper system\nwith varying spring constant\n\n']);

% Parameters
k  = preal('k', 'ct', 'Range', [1 12]);
d  = 2;
m  = 0.5;

% Create LPV model of MSD system
A = [0 1;-k/m -d/m];
B = [0;1/m];
C = [1 0];
D = 0;

G = LPVcore.lpvss(A,B,C,D);     % plant to be controlled
G.InputName = 'F';      % input force
G.OutputName = 'x';     % position

% Generalized plant design (P)
%
%                     d ──┐
%                         │
%            e ┌ ─ ─ ┐    ▼    ┌─────┐
%   r ──▶(+)──▶   K   ──▶(+)──▶│  G  │──┐ x
%         ▲    └ ─ ─ ┘ u     F └─────┘  │
%         │  ┌────┐                     │
%         └──│ -1 │◀────────────────────┘
%            └────┘
%
%   G: plant (MSD)
%   K: To-be-synthesized LPV controller
%
%   r: reference
%   e: tracking error
%   u: control input
%   d: disturbance
%   F: input force of MSD
%   x: position of MSD
%

P = connect(G, ...
            sumblk('e = r - x'), ...
            sumblk('F = d + u'), ...
            {'r','d','u'},...
            {'e','u','e'});

ny = 1;     % number of measured outputs ('e' with size 1 goes into controller)
nu = 1;     % number of controlled inputs ('u' with size 1 comes out of controller)

% Weighting filters
Wr = 1;
Wd = 1;
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(60),[0.5*2*pi, db2mag(0)], db2mag(-6));
Wu = makeweight(db2mag(-30), [10*2*pi, db2mag(0)], db2mag(40));
Wz = blkdiag(We, Wu);

% Weighted generalized plant (Pw)
%
%                 ┌─────┐        ┌─────┐
%                 │ We  │─▶ z1   │  Wd │◀─ w2
%                 └─────┘        └─────┘
%                    ▲              │   
%       ┌────┐       │  ┌ ─ ─ ┐     ▼    ┌─────┐
% w1 ──▶│ Wr │──▶(+)─┴─▶   K   ─┬─▶(+)──▶│  G  │──┐ 
%       └────┘    ▲     └ ─ ─ ┘ │        └─────┘  │
%                 │             │  ┌────┐         │
%                 │             └─▶│ Wu │─▶ z2    │
%                 │  ┌────┐        └────┘         │
%                 └──│ -1 │◀──────────────────────┘
%                    └────┘
%

Pw = blkdiag(Wz,eye(ny))*P*blkdiag(Ww,eye(nu));

% Set synthesis options
synOpt = lpvsynOptions('performance', 'l2');

% Synthesis
%
% Synthesize an LPV controller that optimizes the closed-loop L2-gain from 
% w to z, i.e.
%
% min γ  s.t. ‖ z ‖_2 <= γ ‖ w ‖_2
%

fprintf('Synthesizing controller:\n\n')

[K, gamma, X] = lpvsyn(Pw, ny, nu, synOpt);

fprintf('\nPrinting results:\n\n')
fprintf('Obtained closed-loop L2-gain: %.3g\n\n',gamma)

% Verify
CLw = lft(Pw,K);    % weighted closed-loop
gammaVerify = lpvnorm(CLw,'l2');    % Might be slightly different to 
                                    % numerical computation.

fprintf('Verified closed-loop L2-gain: %.3g\n\n',gammaVerify)

% Create closed-loop
CL = lft(P,K);

% Frozen frequency-response analysis
f1 = figure('Name','Example 1 - Bode'); 
bodemag(CL,(1:12)');
hold on;
bodemag([inv(We)*inv(Wr), inv(We)*inv(Wd); ...
         inv(Wu)*inv(Wr), inv(Wu)*inv(Wd)]);
grid on;
title('Closed-loop Bode diagram')

% Simulation
t = linspace(0, 10, 200)';  % time vector
k = stepFun(5, 3, 8, t);    % scheduling-variable (spring constant k)

r = stepFun(2, 0, 1, t);    % reference
d = stepFun(7, 0, 1, t);    % disturbance

yCL = lsim(CL, k, [r,d], t);    % Simulate

% Get signals
e = yCL(:,1);       
u = yCL(:,2);
xCL = r - e;

% Plot simulation results
f2 = figure('Name','Example 1 - Simulation');
tiledlayout(3,1);

nexttile
plot(t, xCL);hold on;
plot(t, r, '--');
legend('Response','Reference')
xlabel('Time $t$ [seconds]','Interpreter','latex')
ylabel('Position $x$ [m]','Interpreter','latex')
grid on;

nexttile
plot(t,u);
xlabel('Time $t$ [seconds]','Interpreter','latex')
ylabel('Control input $u$ [N]','Interpreter','latex')
grid on;

nexttile
plot(t,k);
xlabel('Time $t$ [seconds]','Interpreter','latex')
ylabel('Scheduling-variable $k$ [N$\cdot$m$^{-1}$]','Interpreter','latex')
grid on;
ylim([0,10]);

% Show figures next to each other
f2.Position(2) = f1.Position(2);
f2.Position(1) = f1.Position(1)+f1.Position(3);

%% Example 2: Generalized H2 optimal LPV controller design for DT plant 
%             using a parameter dependent storage function.
%
% In this example we're going to design a generalized H2 norm optimal LPV
% controller for an unbalanced disk (UD) system. The objective for the
% controller is to keep the disk balanced and reject disturbances.
%
% Model from: Koelewijn, Tóth, "Physical Parameter Estimation of an
% Unbalanced Disc System," TUe CS Report, 2019.
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 2:\n')  
fprintf(['L2-gain optimal LPV controller synthesis for mass-spring-', ...
    'damper system\nwith varying spring constant\n\n']);

% Plan parameters
J = 2.4e-4;
Km = 11;
M = 7.6e-2;
g = 9.8;
l = 4.1e-2;
tau = 4.0e-1;

% Scheduling-variable
p = preal('sinc(θ)','dt',...
          'Range', [0.9, 1], ...    % θ ∈ [-.8 .8] rad => p ∈ [0.9 1]
          'Ratebound', [-0.1, 0.1]);  % p(k+1)-p(k) ∈ [-0.1 0.1]


% Construct discrete time plant (using forward Euler discretization)
Ts = 0.05;  % Sampling time for discretization;

A = eye(2)+Ts*[0, 1;M*g*l*p/J, -1/tau];
B = Ts*[0; Km/tau];
C = [1 0];
D = 0;

G = LPVcore.lpvss(A,B,C,D,Ts);

G.InputName = 'V';      % input voltage
G.OutputName = 'θ';     % angle of the disk

% Generalized plant design (P)
%
%             d ──┐
%                 │
%      ┌ ─ ─ ┐    ▼    ┌─────┐
%  ┌──▶   K   ──▶(+)──▶│  G  │──┐ θ
%  │   └ ─ ─ ┘ u     V └─────┘  │
%  │                            │
%  └────────────────────────────┘
%   
%   G: plant (UD)
%   K: To-be-synthesized LPV controller
%
%   u: control input
%   d: disturbance
%   V: input voltage to UD
%   theta: angle of UD
%

P = connect(G, ...
            sumblk('V = d + u'), ...
            {'d','u'},...
            {'θ','u','θ'});

ny = 1;     % number of measured outputs ('theta' with size 1 goes into controller)
nu = 1;     % number of controlled inputs ('u' with size 1 comes out of controller)

% Weighting filters
Wd = 1;
Ww = Wd;

Wtheta = c2d(makeweight(db2mag(60),[10, db2mag(0)], db2mag(-6)), Ts);
Wu = c2d(makeweight(db2mag(-30), [20, db2mag(-3)], db2mag(0)), Ts);
Wz = blkdiag(Wtheta, Wu);

% Weighted generalized plant (Pw)
%
%                   ┌────┐
%                 ┌─│ Wd │◀── w
%                 │ └────┘
%      ┌ ─ ─ ┐    ▼     ┌─────┐      ┌────────┐
%  ┌──▶   K   ─┬─▶(+)──▶│  G  │──┬──▶│ Wtheta │──▶ z1
%  │   └ ─ ─ ┘ │        └─────┘  │   └────────┘
%  │           │  ┌────┐         │
%  │           └─▶│ Wu │──▶ z2   │
%  │              └────┘         │
%  └─────────────────────────────┘
% 

Pw = blkdiag(Wz,eye(ny))*P*blkdiag(Ww,eye(nu));

% Synthesis
%
% Synthesize an LPV controller that optimizes the closed-loop generalized
% H2 norm from w to z, i.e.
%
% min γ  s.t. ‖ z ‖_∞ <= γ ‖ w ‖_2
%

fprintf('Synthesizing controller:\n\n')

% Set synthesis options
synOpt = lpvsynOptions('performance', 'h2', ...
                       'ParameterVaryingStorage', 1);

[K, gamma, X] = lpvsyn(Pw, ny, nu, synOpt);

fprintf('\nPrinting results:\n\n')
fprintf('Obtained closed-loop generalized H2 norm: %.3g\n\n',gamma)

% Verify
CLw = lft(Pw,K);    % weighted closed-loop
gammaVerify = lpvnorm(CLw,'h2');    % Might be slightly different to 
                                    % numerical computation.

fprintf('Verified closed-loop generalized H2 norm: %.3g\n\n',gammaVerify)

% Create closed-loop
CL = lft(P,K);

% Frozen frequency-response analysis
f1 = figure('Name','Example 2 - Bode'); 
bodemag(CL,(-0.22:.2:1)');
grid on;
title('Closed-loop Bode diagram')

% Simulation
N = 10/Ts;  % number of samples to simulate;
k = 0:N;    % time index vector

xCL = zeros(CL.Nx, N);
yCL = zeros(CL.Ny, N);
p = zeros(CL.Np, N);
d = stepFun(10, 0, 1, k) - stepFun(20, 0, 1, k);    % impulse

for i = k+1
    p(i) = sincFun(xCL(1,i));

    xCL(:,i+1) = peval(CL.A, p(i))*xCL(:,i) + peval(CL.B, p(i)) * d(i);
    yCL(:,i)   = peval(CL.C, p(i))*xCL(:,i);
end

% Get signals
theta = yCL(1,:);       
u = yCL(2,:);

% Plot simulation results
f2 = figure('Name','Example 2 - Simulation');
tiledlayout(2,2);

nexttile
stairs(k, theta);hold on;
xlabel('Time instant $k$ [-]','Interpreter','latex')
ylabel('Angle $\theta$ [rad]','Interpreter','latex')
grid on;

nexttile
stairs(k,u); hold on;
stairs(k, d, '--');
legend('Response','Disturbance')
xlabel('Time instant $k$ [-]','Interpreter','latex')
ylabel('Control input $u$ [V]','Interpreter','latex')
grid on;

nexttile
stairs(k,p);
xlabel('Time instant $k$ [-]','Interpreter','latex')
ylabel('Scheduling-variable $p$ [V]','Interpreter','latex')
grid on;

nexttile
stairs(k(1:end-1),diff(p)); 
xlabel('Time instant $k$ [-]','Interpreter','latex')
ylabel('Scheduling-var. rate $p_{k+1}-p_k$ [-]','Interpreter','latex')
grid on;

% Show figures next to each other
f2.Position(2) = f1.Position(2);
f2.Position(1) = f1.Position(1)+f1.Position(3);

%% Example 3: Grid-based LPV control of NASA X-53 Active Aeroelastic Wing (AAW)
% This example is based on the control example described in Chapter 19 of
% [1], specifically section 19.6. In the example, an L2-gain optimal LPV 
% controller is designed for the rigid body dynamics of the AAW to match an
% ideal response.
%
% [1]: Mohammadpour Velni, J. and Scherer, C. W., "Control of Linear 
% Parameter Varying Systems with Applications," Springer, 2012.
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 3:\n')  
fprintf('Grid-based LPV control of NASA X-53 Active Aeroelastic Wing (AAW)\n\n');

% Rigid body model  - eq. (19.2)
LpMat = [-0.5652 -0.4614 -0.4009;
         -0.5415 -0.4363 -0.3737;
         -0.5165 -0.4128 -0.3606;
         -0.5034 -0.3982 -0.3531];

LdMat = [1.2916 1.3756 1.2425;
         0.9305 1.0524 1.1958;
         0.6032 0.7009 0.8326;
         0.3056 0.4110 0.5258];

pvGrid = struct('h',[10e3, 15e3, 20e3, 25e3],...
                'M',[1.1, 1.2, 1.3]);

A = reshape(LpMat,[1 1 numel(pvGrid.h) numel(pvGrid.M)]);
B = reshape(LdMat,[1 1 numel(pvGrid.h) numel(pvGrid.M)]);
C = 1;
D = 0;
Ggrid = ss(A,B,C,D);

Grig = lpvgridss(Ggrid, pvGrid);
Grig.InputName = 'delta';
Grig.OutputName = 'p';

% Weighted generalized plant
%
%                           ┌────────┐               ┌────┐
%          ┌───────────────▶│ Gideal │────────▶(+)──▶│ Wp │───▶ z1
%          │                └────────┘          ▲    └────┘
%          │                                    │
%          │                  ┌─────┐         ┌────┐
%          │                  │ Wd  │◀─ d     │ -1 │ 
%          │                  └─────┘         └────┘
%          │                     │              ▲
%          │         ┌ ─ ─ ┐     ▼    ┌──────┐  │
%  pcmd ───┴─▶(+)───▶   K   ─┬─▶(+)──▶│ Grig │──┤ p
%              ▲     └ ─ ─ ┘ │        └──────┘  │
%              │             │  ┌────┐          │
%              │             └─▶│ Wa │─▶ z2     │
%              │  ┌────┐        └────┘          ▼    ┌────┐
%              └──│ -1 │◀──────────────────────(+)◀──│ Wn │◀─── n
%                 └────┘                             └────┘
%

wd = 1.25;
zeta = 0.8;
Gideal = tf(wd^2,[1 2*zeta*wd wd^2]);
Gideal.InputName = 'pcmd';
Gideal.OutputName = 'pf';

Wa = tf([100 25], [1 2500]);
Wa.InputName = 'u';
Wa.OutputName = 'z2';

Wp = tf([0.01 12.5], [1 0.125]);
Wp.InputName = 'e';
Wp.OutputName = 'z1';

Wd = ss(0.1);
Wd.InputName = 'd';
Wd.OutputName = 'dt';

Wn = ss(0.01);
Wn.InputName = 'n';
Wn.OutputName = 'nt';

Pw = connect(Grig, Gideal, Wa, Wp, Wd, Wn, ...
             sumblk('y = pcmd - pt'),...
             sumblk('delta = dt + u'),...
             sumblk('pt = p + nt'),...
             sumblk('e = pf - p'),...
             {'pcmd','d','n','u'},{'z1','z2','y'});

ny = 1;
nu = 1;

% Synthesis
fprintf('Synthesizing controller:\n\n')

% Set synthesis options
synOpt = lpvsynOptions('performance', 'l2');

[K, gamma, X] = lpvsyn(Pw, ny, nu, synOpt);

fprintf('\nPrinting results:\n\n')
fprintf('Obtained closed-loop L2-gain: %.3g\n\n',gamma)
% Note that obtained gamma is (nearly) the same as in [1].

% Verify
CLw = lft(Pw,K);    % weighted closed-loop
gammaVerify = lpvnorm(CLw,'l2');    % Might be slightly different to 
                                    % numerical computation.

fprintf('Verified closed-loop L2-gain: %.3g\n\n',gammaVerify)

% Frozen frequency-response analysis of LPV controller
% Similar to Figure 19.9 in [1]
f1 = figure('Name','Example 3 - Controller Bode'); 
bodeopt = bodeoptions;
bodeopt.PhaseMatching = 'on';

bode(K,bodeopt);
grid on;
title('LPV controller Bode diagram');

f1.Children(3).XLim = [1e-2, 1e3];
f1.Children(3).YLim = [-80, 40];
f1.Children(2).XLim = [1e-1, 1e3];
f1.Children(2).YLim = [-180,-45];

% Bonus: Synthesis with parameter-varying storage function

% Create template storage function
h = preal('h', ...
    'Range', [min(pvGrid.h), max(pvGrid.h)], ...
    'RateBound', [-100, 100]);
M = preal('M', ...
    'Range', [min(pvGrid.M), max(pvGrid.M)], ...
    'RateBound', [-0.01 0.05]);

storageTemplate = 0 + h + M^2;

synOpt.ParameterVaryingStorage = storageTemplate;
[K2, gamma2] = lpvsyn(Pw, ny, nu, synOpt);

fprintf('Obtained closed-loop L2-gain with parameter-varying storage: %.3g\n\n',gamma2)


%% Example 4 (Advanced): Pole contraints and controller restrictions
% We take the system from Example 1, but this this time we add constraints
% to ensure all the (for a frozen scheduling-variable) closed-loop poles 
% are within a certain region, and the controller is of the form 
%
%    (d/dt)x = A(p) x + B u
%          y = C x 
%
% i.e., the B and C matrix are constant and the D-matrix is zero.
%

clearvars;
fprintf([repelem('-',76),'\n'])
fprintf('Example 4:\n')  
fprintf('Synthesis with pole contraints and controller restrictions\n\n');

% Create weighted generalized plant (from Example 1)
k  = preal('k', 'ct', 'Range', [1 12]);
d  = 2;
m  = 0.5;

A = [0 1;-k/m -d/m];
B = [0;1/m];
C = [1 0];
D = 0;

G = LPVcore.lpvss(A,B,C,D);
G.InputName = 'F';
G.OutputName = 'x';

P = connect(G, ...
            sumblk('e = r - x'), ...
            sumblk('F = d + u'), ...
            {'r','d','u'},...
            {'e','u','e'});

ny = 1;
nu = 1;

Wr = 1;
Wd = 1;
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(60),[0.5*2*pi, db2mag(0)], db2mag(-6));
Wu = db2mag(-30);       % We've changed Wu to a constant weight compared to example 1
Wz = blkdiag(We, Wu);

Pw = blkdiag(Wz,eye(ny))*P*blkdiag(Ww,eye(nu));

% Create pole constraint options
% Ensure that all (frozen) closed-loop poles are within the region right of
% -10 and within a (conic) sector with an angle of 65 degrees. 
% See also the figure below.
%
%                                        
%           │╲       ▲ Im                
%                    │                   
%           │ ╱╲     │                   
%            ╱       │                   
%           │  ╱ ╲   │                   
%             ╱  ╱   │                   
%           │╱  ╱  ╲ │                   
%              ╱   ╱ │               Re  
%    ◀──────┼─╱───╱──╳───────────────▶   
%       -10  ╱   ╱ ◟ │                   
%           │   65°╱ │                   
%              ╱     │                   
%           │ ╱  ╱   │                   
%            ╱       │                   
%           │  ╱     │                   
%                    │                   
%           │╱       ▼                   
%    
%

poleOpt = poleConstraintOptions('rightHalfPlane', -10,...
                                'sector', deg2rad(65));

% Set synthesis options
%
% Ensure the controller is of the form
%
%    (d/dt)x = A(p) x + B u
%          y = C x 
%
% To ensure this, we set the depency of the transformed controller matrices
% to be parameter-independent, i.e., K, L, M, N (see eq. (4.2.14) in [2])
% are set to be parameter-indpendent. This will result in the B and C
% matrix of our controller to be parameter-independent.
%
% [2]: Scherer, C. W. and Weiland, S., "Linear Matrix Inequalities in 
% Control, Jan. 2015.
% url: https://www.imng.uni-stuttgart.de/mst/files/LectureNotes.pdf
%  

synOpt = lpvsynOptions('performance', 'l2', ...
                       'directFeedThrough', 0,...
                       'Dependency', [0 0 0 0],...
                       'PoleConstraint', poleOpt);

fprintf('Synthesizing controller:\n\n')

[K, gamma, X] = lpvsyn(Pw, ny, nu, synOpt);

fprintf('\nPrinting results:\n\n')
fprintf('Obtained closed-loop L2-gain: %.3g\n\n',gamma)

% Verify
CLw = lft(Pw,K);    % weighted closed-loop
gammaVerify = lpvnorm(CLw,'l2');    % Might be slightly different to 
                                    % numerical computation.

fprintf('Verified closed-loop L2-gain: %.3g\n\n',gammaVerify)

% Create closed-loop
CL = lft(P,K);

% Frozen frequency-response analysis
f1 = figure('Name','Example 4 - Bode'); 
bodemag(CL,(1:12)');
hold on;
bodemag([inv(We)*inv(Wr), inv(We)*inv(Wd); ...
         inv(Wu)*inv(Wr), inv(Wu)*inv(Wd)]);
grid on;
title('Closed-loop Bode diagram')

% Frozen eigen value analysis
eigVal = computeEigenValues(CLw, 1:12);
eigVal = reshape(eigVal,[],1);

f2 = figure('Name','Example 4 - Eigenvalues');
patch([-10, 0, -10],[tan(deg2rad(65))*-10, 0, tan(deg2rad(65))*10],'y','FaceAlpha', 0.1);
hold on;
scatter(real(eigVal),imag(eigVal));
grid on;
legend('Pole constraint region', '(Frozen) eigenvalues')

% Simulation
t = linspace(0, 10, 200)';  % time vector
k = stepFun(5, 3, 8, t);    % scheduling-variable (spring constant k)

r = stepFun(2, 0, 1, t);    % reference
d = stepFun(7, 0, 1, t);    % disturbance

yCL = lsim(CL, k, [r,d], t);    % Simulate

% Get signals
e = yCL(:,1);       
u = yCL(:,2);
xCL = r - e;

% Plot simulation results
f3 = figure('Name','Example 4 - Simulation');
tiledlayout(3,1);

nexttile
plot(t, xCL);hold on;
plot(t, r, '--');
legend('Response','Reference')
xlabel('Time $t$ [seconds]','Interpreter','latex')
ylabel('Position $x$ [m]','Interpreter','latex')
grid on;

nexttile
plot(t,u);
xlabel('Time $t$ [seconds]','Interpreter','latex')
ylabel('Control input $u$ [N]','Interpreter','latex')
grid on;

nexttile
plot(t,k);
xlabel('Time $t$ [seconds]','Interpreter','latex')
ylabel('Scheduling-variable $k$ [N$\cdot$m$^{-1}$]','Interpreter','latex')
grid on;
ylim([0,10]);

% Show figures next to each other
f2.Position(2) = f1.Position(2);
f2.Position(1) = f1.Position(1)+f1.Position(3);

%% Local functions
% Step function
function y = stepFun(tStep,a,b,t)
%   Step from 'a' to 'b' at time 'tStep' evaluated at time 't'.
    y = (min(t,tStep) >= tStep)*(b-a)+a;
end

% Compute eigenvalues
function eigVal = computeEigenValues(sys, p)
% Returns eigenvalues of lpvss system 'sys' for frozen scheduling-variables
% 'p'.
    A = sys.A;

    eigVal = NaN(sys.Nx,numel(p));
    for i = 1:numel(p)
        eigVal(:,i) = eig(peval(A,p(i),0));
    end
end

% sinc function
function  y = sincFun(x)
    if x == 0
        y = ones(numel(x),1);
    else
        y = sin(x)./x;
    end
end