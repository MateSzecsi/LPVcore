clearvars; close all; clc; rng(2);

%% Example 1: LPVLFR reduction
% Create full-order model
p = preal('p', 'dt');
Delta = diag([p, p^2, p^3]);
nz = size(Delta, 1);
nw = size(Delta, 2);
ny = 1;
nu = 2;
nx = 10;
% State reduction order
rx = 7;
% Generate a random G block
G = drss(nx, nz + ny, nw + nu);
assert(isstable(G));
% Scale G to ensure a stable LPV-LFR form
G = G / hinfnorm(G);
% Construct LPV-LFR form
sys = lpvlfr(Delta, G);

% Reduction of state
[sysr_ltibalred, info_ltibalred] = lpvmodred(sys, rx, [], 'ltibalred');

% Show Hankel Singular Values (HSV) of G
figure;
semilogy(info_ltibalred.HSV); grid on;
xline(rx, 'r', 'rx');
xlabel('Order (Number of States)');
ylabel('State Contribution');
title('Hankel Singular Values of sys.G (ltibalred)');

% Reduction of Delta block
rd = 2;
[sysr_deltabalred, info_deltabalred] = lpvmodred(sys, [], rd, 'deltabalred');

% Show Hankel Singular Values (HSV) of Delta balancing system
figure;
semilogy(info_deltabalred.DeltaHankelSingularValues); grid on;
xline(rd, 'r', 'rd');
xlabel('Order (Number of States)');
ylabel('State Contribution');
title('Hankel Singular Values of Delta balancing system (deltabalred)');

% Simulation
% Nr. of time steps
N = 20;
% Input data
u = rand(N, nu);
p = rand(N, sys.Np);
[y, t] = lsim(sys, p, u);
yr_ltibalred = lsim(sysr_ltibalred, p, u);
yr_deltabalred = lsim(sysr_deltabalred, p, u);

figure;
plot(t, y, 'k-'); hold on;
plot(t, yr_ltibalred, 'r--');
plot(t, yr_deltabalred, 'b--');
grid on; legend('sys', 'sysr (ltibalred)', 'sysr (deltabalred)'); xlabel('Time (s)'); ylabel('Output');
title('Time-domain Validation of Reduced Model');

%% Example 2: LPVSS reduction with combined Delta and state reduction
% This example shows the sequential reduction of Delta block size and state
% dimension of an LPVSS model.
% The reduction is achieved by 2 calls to LPVMODRED. The first call used
% the 'deltasvdred' method to reduced the Delta size. The result is then
% passed to a second call using the 'momentmatching' method to reduce the
% state size.

% First, generate a random LPVCORE.LPVSS full-order model
p = preal('p', 'dt');
nx = 10;
rng(1);
A = p * diag(rand(nx, 1)) + (0.1 + 0.1 * p^2) * rand(nx);
B = randn(nx, 1);
C = randn(1, nx);
D = 0;
sys = LPVcore.lpvss(A, B, C, D, 1);

% Delta block reduction step
rd = 10;
[sysr_rd, info] = lpvmodred(sys, [], rd, 'deltasvdred');

% State reduction step
rx = 6;
% The returned model is in LPVLFR form.
% Convert back to LPVCORE.LPVSS as required by the 'momentmatching' state
% reduction method.
sysr_rd = LPVcore.lpvss(sysr_rd);
sysr_rx = lpvmodred(sysr_rd, rx, [], 'momentmatching');

% Show Delta Singular Values
figure;
semilogy(info.DeltaSingularValues); grid on;
xline(rd, 'r', 'rd');
xlabel('Order (Size of Delta block)');
ylabel('Delta Element Contribution');
title('Singular Values of Scheduling-Dependent System Matrix Coefficients');

% Simulation
% Nr. of time steps
N = 50;
% Input data
u = rand(N, sys.Nu);
p = rand(N, sys.Np);
[y, t] = lsim(sys, p, u);
yr_rd = lsim(sysr_rd, p, u);
yr_rx = lsim(sysr_rx, p, u);

figure;
plot(t, y, 'k-'); hold on;
plot(t, yr_rd, 'r--');
plot(t, yr_rx, 'b-*');
grid on; legend(sprintf('sys  (Delta: %i, State: %i)', size(sys.Delta, 1), sys.Nx), ...
    sprintf('sysr (Delta: %i, State: %i)', size(sysr_rd.Delta, 1), sysr_rd.Nx), ...
    sprintf('sysr (Delta: %i, State: %i)', size(sysr_rx.Delta, 1), sysr_rx.Nx));
xlabel('Time (s)'); ylabel('Output');
title('Time-domain Validation of Reduced Model');