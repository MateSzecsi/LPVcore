%% Example script demonstrating usage of LPVPCARED
%
% The LPVPCARED script simplifies the scheduling dependence of a given
% LPVLFR object by 1) lowering the dimension of the scheduling signal; and
% 2) 
clearvars; close all; rng(1);

% Set up the full model to be reduced with LPVPCARED
p = preal('p', 'ct');
q = preal('q', 'ct');
% State and input-output dimensions are parametrized
nx = 1;
ny = 1;
nu = 1;
% Generate state-space coefficients
A = (p + q + 0.5 * p^2) * eye(nx);
B = p * ones(nx, nu);
C = p^2 * ones(ny, nx);
D = 10 * ones(ny, nu);
sys = LPVcore.lpvss(A, B, C, D);

% Generate simulation data
N = 100;
u = randn(N, sys.Nu);
rho = randn(N, sys.Np);
t = linspace(0, 1, N);
y = lsim(sys, rho, u, t);
data = lpviddata(y, rho, u, t(2)-t(1));

% Use LPVPCARED to generate a system SYSR with the following reductions in
% complexity:
%   1) Lower scheduling signal dimension from 2 to 1
%   2) Affine dependence in scheduling signal (instead of basis-affine
%           dependence in SYS)
[sysr, mapping_fnc, info] = lpvpcared(sys, 1, data);
% Besides the reduced-complexity model SYSR, a function handle MAPPING_FNC
% is returned that is used to convert the original scheduling signal
% (associated with SYS) to the scheduling signal associated with SYSR.
% It takes as input argument an N-by-Nrho matrix of N time samples of the
% original scheduling signal (dimension Nrho) and returns an N-by-Nrho_sysr
% matrix with the reduced scheduling signal.
rho_sysr = mapping_fnc(rho);
% Subsequently, we can perform simulations with SYSR
yr = lsim(sysr, rho_sysr, u, t);

% The third output of LPVPCARED is a structure with additional information.
% For example, the singular values of the data matrix, which can be plotted
% to determine an appropriate scheduling signal dimension.
figure;
semilogy(info.DataMatrixSingularValues, '*-'); grid on;
title('Singular values of data matrix');
xline(sysr.Np, 'r--', 'sysr.Np');
xline(sys.Np, 'b--', 'sys.Np');

% Validate the reduced-complexity model by comparing a time-domain
% simulation of the input-output behavior between SYS and SYSR
figure;
subplot(2, 1, 1);
plot(t, y, 'k-'); hold on; grid on;
plot(t, yr, 'k--');
title('Output response');
legend(sprintf('Original (Np = %i)', sys.Np), ...
    sprintf('Affine/reduced (Np = %i)', sysr.Np));
subplot(2, 1, 2);
plot(t, y - yr, 'r'); grid on;
title(sprintf('Error (fit = %.3f)', goodnessOfFit(yr, y, 'NRMSE')));

%% Example 1 from [1]
% TODO: fix issues with this example
% [1] "Affine linear parameter-varying embedding of non-linear models with
% improved accuracy and minimal overbounding" (IET Control Theory &
% Applications, 2021)
clearvars; close all; rng(1);

a1 = preal('a1', 'ct');
a2 = preal('a2', 'ct');
a3 = preal('a3', 'ct');

A = [1 + 2 * a1, 3 + a2; 2 + 3 * a3, 20 * a1 + 5 * a2];
B = [3 * a3 + 7 * a2; 1];
C = [a1, 0];
D = 0;

sys = LPVcore.lpvss(A, B, C, D, 0, 'SVDTolerance', -1);

N = 3000;
Ts = 1E-3;
t = linspace(0, (N-1)*Ts, N).';

alpha = [2 * sin(10*t).^2, 5*cos(20*t + pi/5).^2, sin(10*t).*cos(20*t)];

[sysr, mapping_fnc, info] = lpvpcared(sys, 2, alpha);

rho = mapping_fnc(alpha);

a11 = 1 + 2 * alpha(:, 1);
a22 = 20 * alpha(:, 1) + 5 * alpha(:, 2);
b11 = 3 * alpha(:, 3) + 7 * alpha(:, 2);

Atilde = feval(sysr.A, rho);
Btilde = feval(sysr.B, rho);
a11_tilde = reshape(Atilde(1, 1, :), [], 1);
a22_tilde = reshape(Atilde(2, 2, :), [], 1);
b11_tilde = reshape(Btilde(1, 1, :), [], 1);

figure;
subplot(2, 3, 1);
plot(t, a11, 'k--'); grid on; hold on;
plot(t, a11_tilde, 'g');

subplot(2, 3, 2);
plot(t, a22, 'k--'); grid on; hold on;
plot(t, a22_tilde, 'g');

subplot(2, 3, 3);
plot(t, b11, 'k--'); grid on; hold on;
plot(t, b11_tilde, 'g');

subplot(2, 3, 4);
plot(t, a11 - a11_tilde, 'g'); grid on;
ylim([-1.5, 2]);

subplot(2, 3, 5);
plot(t, a22 - a22_tilde, 'g'); grid on;
ylim([-6, 8]);

subplot(2, 3, 6);
plot(t, b11 - b11_tilde, 'g'); grid on;
ylim([-15, 12]);