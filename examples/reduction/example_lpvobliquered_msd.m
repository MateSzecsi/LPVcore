%% Example script showing LPVOBLIQUERED command
%
% In this script, we reproduce the results of the reference of
% LPVOBLIQUERED:
%
%   [1] "LPV Model Order Reduction by Parameter-Varying Oblique Projection"
%   by J. Theis, P. Seiler and H. Werner (2018)
%
% We implement the first example (Section IV.A): a nonlinear mass-spring
% damper system where the stiffness is a cubic function of displacement.
%
clearvars; close all; clc; rng(1);
set(0, 'DefaultLineLineWidth', 1.5);

%% Define system parameters
N = 50;     % [-] Number of blocks
m = 1;      % [kg] Mass of each block
d = 1;      % [Ns/m] Damping constant of linear dampers
k1 = 0.5;   % [N/m] Linear stiffness coefficient
k2 = 1;     % [N/m^3] Cubic stiffness coefficient

%% Define model structure
M = m * speye(N);  % Mass matrix
% The linear force terms (damping and linear stiffness)
% can be expressed in terms of the following structure matrix S
% such that Fdamp = d*S and Flinstiff = k1*S:
%
%   S = [2, -1, 0, ..., 0; ...
%        -1, 3, -1, 0, ..., 0; ...
%        0, -1, 3, -1, ..., 0; ...
%
%       0, ..., 0, -1, 3, -1; ...
%       0, ..., 0, 0, -1, 2]
S = gallery('tridiag', N, -1, 3, -1);
S(1, 1) = 2;
S(end, end) = 2;

D = d * S;                  % Damping matrix
K1 = k1 * S;                % Linear stiffness matrix
K2 = @(q) get_K2(q, k2);    % Nonlinear stiffness
J_K2 = @(q) get_K2_J(q, k2);% Jacobian of nonlinear stiffness
B = [zeros(N-1, 1); 1];     % Input-to-force matrix
F = [zeros(1, N-1), 1];     % State-to-output matrix (qN is output)

% Validate the correctness of the Jacobian
% If correct, the following should hold (for sufficiently small dq):
%   K2(q0 + dq) = K2(q0) + J_K2(q0) dq
%   ||K2(q0 + dq) - K2(q0) - J_K2(q0) dq)|| / || K2(q0) ||
q0 = randn(N, 1); q0 = q0 / norm(q0);
for i=1:10
    dq = randn(N, 1); dq = dq / (norm(dq) * 1E10);
    J_K2_error = norm(K2(q0 + dq) - K2(q0) - J_K2(q0)*dq) / norm(K2(q0));
    fprintf('Jacobian relative 2-norm error is %.3e\n', J_K2_error);
end

% Using the above matrices and function handles,
% the nonlinear dynamics can be described by the following differential
% equation:
%
%   M ddqdtt + D dqdt + K1 q + K2(q) = B (u + rho)
%
% The scheduling signal is rho. To obtain local models at frozen values of
% rho, we need to find the equilibrium displacement q0 for a given rho
% (with u = 0). This is done by solving for q the following
% nonlinear system of equations:
%
%           K1 q + K2(q) = B rho
%
% The solution to this system is q0.
fun = @(q, rho) K1*q + K2(q) - B*rho;
q0 = @(rho) fsolve(@(q) fun(q, rho), zeros(N, 1), ...
    optimoptions('fsolve', 'Display', 'off'));

% sys is the linearized scheduling-dependent model
% Using full(), we convert it to first-order form as is required by
% LPVGRIDSS and LPVOBLIQUERED.
sys = @(rho) full(mechss(M, D, K1 + J_K2(q0(rho)), B, F));

%% Build grid container of the linearized models
pgrid = struct('rho', [0, 1, 2]);
models(:, :, 1) = sys(pgrid.rho(1));
models(:, :, 2) = sys(pgrid.rho(2));
models(:, :, 3) = sys(pgrid.rho(3));
sysgrid = lpvgridss(models, pgrid);

%% Reduction
r = 4;
sysrgrid = lpvobliquered(sysgrid, r, struct('Verbose', 1));

%% Show results (figure 1 from [1])
figure;
w = {1E-2, 1E2};
bodemag(models, 'k-', w); grid on; hold on;
bodemag(sysrgrid, 'r-', w);
bodemag(models - sysrgrid, 'r--', w);
legend(sprintf('Full model (%d states)', 2*N), ...
    sprintf('Reduced model (%d states)', r), ...
    'Reduction error');
title('Linearized MSD system for different external forces \rho');

%% Show results (Table 1 from [1])
% Calculate maximum local Hinf error
hinf_errors = hinfnorm(models - sysrgrid.ModelArray);
max_hinf_error = max(hinf_errors);
fprintf('Local Hinf norm error: %.3e\n', max_hinf_error);

%% Local functions and linearization details
function K2 = get_K2(q, k2)
    % Return the nonlinear stiffness force:
    %   K2 = get_K2(q, k2)
    % where K2 is an N-by-1 column vector of nonlinear stiffness forces.
    q = q(:);
    K2 = q.^3;
    K2(2:end)   = K2(2:end)   + (q(2:end)   - q(1:end-1)).^3;
    K2(1:end-1) = K2(1:end-1) + (q(1:end-1) - q(2:end)  ).^3;
    K2 = k2 * K2;
end

function J = get_K2_J(q, k2)
    % Return the nonlinear stiffness Jacobian as a matrix.
    % We have a nonlinear component of the force caused by the nonlinear
    % stiffness:
    %   Fnl(q) = k2 * [Fnl,1(q); ...; Fnl,N(q)]
    %
    % with
    %   Fnl,1(q) = q1^3 + (q1 - q2)^3
    %   Fnl,i(q) = qi^3 + (qi - q(i-1))^3 + (qi - q(i+1))^3,  1<i<N
    %   Fnl,N(q) = qN^3 + (qN - q(N-1)^3
    %
    % for which the Jacobian is given by:
    %
    %   dFnl/dq1(q) = 3 * [ q1^2 + (q1 - q2)^2; ...
    %                       -(q2-q1)^2; 
    %                       0; 
    %                       ...; 
    %                       0]
    %   dFnl/dqi(q) = 3 * [ 0;
    %                       ...
    %                       0;
    %                            - (q(i-1)-qi)^2; ...
    %                       qi^2 + (qi-q(i-1))^2 + (qi-q(i+1))^2;
    %                                            - (q(i+1)-qi)^2; ...
    %                       0;
    %                       ...; 
    %                       0]
    q = q(:);
    N = numel(q);
    J = sparse(N, N);
    % dq = [(q2-q1)^2; ...; (qN-q(N-1))^2]
    dq = (q(2:end) - q(1:end-1)).^2;
    % Loop over differentiation variables
    for i=1:N
        % Loop over rows of Jacobian
        for j=1:N
            Jji = 0;
            if i == j
                if i == 1
                    Jji = Jji + q(i)^2 + dq(1);
                elseif i == N
                    Jji = Jji + q(i)^2 + dq(end);
                else
                    Jji = Jji + q(i)^2 + dq(i-1) + dq(i);
                end
            elseif j == i-1
                Jji = Jji - dq(i-1);
            elseif j == i+1
                Jji = Jji + dq(i);
            end
            % TODO: make this more efficient
            J(j, i) = Jji; %#ok<SPRIX>
        end
    end
    J = k2*3*J;
end
