%% Example script showing LPVOBLIQUERED command
clearvars; close all; clc; rng(1);

%% Define full-order model (FOM)
% The FOM is a SISO synthetic example with 2 scheduling signals and 10 states.
% Full order
nx = 10;
% Reduced order
r = 1;
% Scheduling signals
p = preal('p', 'ct');
q = preal('q', 'ct');
A = (-10 * eye(nx) + rand(nx)) + (0.4 * p + 0.4 * q) * (rand(nx));
B = (1 + q) * ones(nx, 1);
C = ones(1, nx);
D = 1;

sys = LPVcore.lpvss(A, B, C, D, 0);
pgrid = struct('p', [-1, 0, 1], 'q', [-1, 0, 1]);
models = extractLocal(sys, pgrid);
assert(isstable(models));
sysgrid = lpvgridss(models, pgrid);
sys = lpvinterpss(sysgrid, 'pwlinear');

%% Reduction
sysrgrid = lpvobliquered(sysgrid, r, struct('Verbose', 1));

% Form global models
sysr = lpvinterpss(sysrgrid, 'pwlinear');

% Define validation grid
pgridVal = struct('p', [-0.5, 0.5], 'q', [0.5, 0.5]);

%% Local validation
figure;
subplot(2, 1, 1);
bode(sysgrid); hold on; grid on;
bode(sysrgrid, 'r');
legend('FOM', 'ROM');
title('Reduction grid');
subplot(2, 1, 2);
bode(extractLocal(sys, pgridVal)); hold on; grid on;
bode(extractLocal(sysr, pgridVal));
title('Validation grid');

%% Global validation
N = 100;
M = 5;
tData = linspace(0, 2, N * M);
uData = kron(randn(1, N), ones(1, M)).';
fp = 3;  % Hz
fq = 2;  % Hz
pData = [sin(2 * pi * fp * tData.'), cos(2 * pi * fq * tData.')];
[yData, t] = lsim(sys, pData, uData, tData);
yrData = lsim(sysr, pData, uData, tData);

figure;
subplot(3, 1, 1);
plot(t, uData, 'k'); grid on; title('Input signal');
subplot(3, 1, 2);
plot(t, pData); grid on; title('Scheduling signal');
legend('p', 'q');
subplot(3, 1, 3);
plot(t, yData, 'k-'); hold on; grid on; title('Output signal');
plot(t, yrData, 'k--');
xlabel('time (s)');
legend('FOM', 'ROM');