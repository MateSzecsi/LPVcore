function tests = conversionLpvssTest
%CONVERSIONLPVSSTEST Verify conversionLpvss function
    tests = functiontests(localfunctions);
end

%% Setup
function setupOnce(testCase)
rng(2);

nx = 4;
nu = 2;
ny = 3;

np = 3;
nd = 2;

Delta = pmatrix(randn(nd,nd,np), 'SchedulingDimension', np);

G1 = rss(nx, ny + nd, nu + nd);
G1 = G1 / (2 * hinfnorm(G1));
G2 = G1;
G1.D(1:nd,1:nd) = 0;

sys1 = LPVcore.lpvss(lpvlfr(Delta, G1));
sys2 = lpvlfr(Delta, G2);

sys1.StateName = 'a';
sys1.StateUnit = 'cm';
sys1.TimeUnit = 'hours';
sys1.InputName = 'b';
sys1.InputUnit = 'N';
sys1.InputGroup = struct('groupIn1',1,'groupIn2',1);
sys1.OutputName = 'c';
sys1.OutputUnit = 'km';
sys1.OutputGroup = struct('groupOut1',[1 2],'groupOut2',3);
sys1.Name = 'dynamical systemm';
sys1.Notes = 'A note';
sys1.UserData = 'user data';

% Add data to testcase
testCase.TestData.sys1 = sys1;
testCase.TestData.sys2 = sys2;
end

function testLpvss(testCase)
sys = testCase.TestData.sys1;

if ~isMATLABReleaseOlderThan("R2023a")
    sysConv = LPVcore.conversionLpvss(sys);
    
    n = 5;
    np = sys.Np;
    
    rng(1);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = mat2cell(pCheck',ones(np,1), n);
    
    s = extractLocal(sys, pCheck1); s = cat(4, s{:});
    sc = sample(sysConv, 0, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
    
    % Check properties
    propertyNames = {'StateName','StateUnit','TimeUnit','InputName',...
            'InputUnit','InputGroup','OutputName','OutputUnit',...
            'OutputGroup','Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        verifyEqual(testCase, sys.(propertyNames{i}), sysConv.(propertyNames{i}));   
    end

    % Time-domain simulation to verify intput-output behavior matches
    % approximately. Won't be exactly the same because MATLAB uses
    % different time-domain simulation technique (discretization of system
    % with fixed time step).
    N = 100;
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    t = linspace(0, 1, N);
    y = lsim(sys, p, u, t);
    yConv = lsim(sysConv, u, t, zeros(sys.Nx, 1), p);
    
    fit = goodnessOfFit(y, yConv, 'NMSE');
    
    for i=1:sys.Ny
        verifyLessThan(testCase, fit(i), 2E-3)
    end

else
    % LPVSS was introduced in R2023a --> no test needed
    verifyTrue(testCase, true);
end
end

function testLpvlfr(testCase)
sys = testCase.TestData.sys2;
if ~isMATLABReleaseOlderThan("R2023a")
    sysConv = LPVcore.conversionLpvss(sys);
    
    n = 5;
    np = sys.Np;
    
    rng(2);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = mat2cell(pCheck',ones(np,1), n);
    
    s = extractLocal(sys, pCheck1); s = cat(4, s{:});
    sc = sample(sysConv, 0, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
else
    % LPVSS was introduced in R2023a --> no test needed
    verifyTrue(testCase, true);
end
end