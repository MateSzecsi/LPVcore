function tests = conversionPssTest
%CONVERSIONPSSTEST Verify conversionPss function (LPVTools must be
%added to the path for these functions to pass).
    tests = functiontests(localfunctions);
end

%% Setup
function setupOnce(testCase)
rng(2);

nx = 4;
nu = 2;
ny = 3;

nP = {5 3 4};

G1 = rss(nx, ny, nu, nP{:});
grid = struct('p1', linspace(-5,2,nP{1}), ...
              'p2', linspace(-3,2,nP{2}), ...
              'p3', linspace(-1,1,nP{3}));

G2 = c2d(G1, .1);
sys1 = lpvgridss(G1, grid);
sys2 = lpvgridss(G2, grid);

sys1.StateUnit = repmat({'cm'},nx,1);
sys1.InputName = 'b';
sys1.InputUnit = repmat({'N'},nu,1);
sys1.OutputName = 'c';
sys1.OutputUnit = repmat({'km'},ny,1);
sys1.Name = 'dynamical systemm';
sys1.Notes = 'A note';
sys1.UserData = 'user data';

% Add data to testcase
testCase.TestData.sys1 = sys1;
testCase.TestData.sys2 = sys2;
end

function testCT(testCase)
    sys = testCase.TestData.sys1;
    sysConv = LPVcore.conversionPss(sys);
    
    verifyTrue(testCase, all(norm(sys.ModelArray-sysConv.Data,'inf') <= 1e-12, "all"));
    
    % Check properties
    propertyNames = {'StateUnit','InputName',...
        'InputUnit','OutputName','OutputUnit',...
        'Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        verifyEqual(testCase, sys.(propertyNames{i}), sysConv.(propertyNames{i}));   
    end
end

function testDt(testCase)
    sys = testCase.TestData.sys2;
    sysConv = LPVcore.conversionPss(sys);

    verifyEqual(testCase, sysConv.Ts, sys.Ts)
    
    verifyTrue(testCase, all(norm(sys.ModelArray-sysConv.Data,'inf') <= 1e-12, "all"));
end