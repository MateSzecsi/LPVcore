function tests = conversionPlftssTest
%CONVERSIONPLFTSSTEST Verify conversionPlftss function (LPVTools must be
%added to the path for these functions to pass).
    tests = functiontests(localfunctions);
end

%% Setup
function setupOnce(testCase)
rng(2);

nx = 4;
nu = 2;
ny = 3;

nd = 2;

p1 = preal('par1','Range', [-3 3]);
p2 = preal('par2','Range', [-5 10],'RateBound', [-10 2]);
p3 = preal('par3','Range', [-9 12],'RateBound', [-1 9]);

Delta = randn(nd) + randn(nd) * p1 + randn(nd) * p2 + randn(nd) * p3;

G1 = rss(nx, ny + nd, nu + nd);
G1 = G1 / (2 * hinfnorm(G1));
G2 = G1;
G1.D(1:nd,1:nd) = 0;
G3 = G2; G3.Ts = .1;

sys1 = LPVcore.lpvss(lpvlfr(Delta, G1));
sys2 = lpvlfr(Delta, G2);
sys3 = c2d(sys2, .1);

sys1.StateName = 'a';
sys1.StateUnit = 'cm';
sys1.TimeUnit = 'hours';
sys1.InputName = 'b';
sys1.InputUnit = 'N';
sys1.InputGroup = struct('groupIn1',1,'groupIn2',1);
sys1.OutputName = 'c';
sys1.OutputUnit = 'km';
sys1.OutputGroup = struct('groupOut1',[1 2],'groupOut2',3);
sys1.Name = 'dynamical systemm';
sys1.Notes = 'A note';
sys1.UserData = 'user data';

% Add data to testcase
testCase.TestData.sys1 = sys1;
testCase.TestData.sys2 = sys2;
testCase.TestData.sys3 = sys3;
end

function testLpvss(testCase)
    sys = testCase.TestData.sys1;
    sysConv = LPVcore.conversionPlftss(sys);
    
    n = 5;
    np = sys.Np;
    
    rng(1);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = reshape([sys.SchedulingName';mat2cell(pCheck',ones(np,1), n)'],1,[]);
    
    s = extractLocal(sys, pCheck1); s = cat(4, s{:});
    sc = usubs(sysConv, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
    
    % Check properties
    propertyNames = {'StateName','StateUnit','TimeUnit','InputName',...
            'InputUnit','InputDelay','InputGroup','OutputName','OutputUnit',...
            'OutputGroup','Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        verifyEqual(testCase, sys.(propertyNames{i}), sysConv.(propertyNames{i}));   
    end

    % Time-domain simulation to verify intput-output behavior matches
    % approximately. Won't be exactly the same because LPVTools uses
    % slightly different approach.
    N = 100;
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    t = linspace(0, 1, N);
    y = lsim(sys, p, u, t);
    yConv = lpvlsim(sysConv, struct('par1',p(:,1),'par2',p(:,2), ...
        'par3',p(:,3),'time',t), u, t, zeros(sys.Nx, 1));
    
    fit = goodnessOfFit(y, yConv, 'NMSE');
    
    for i=1:sys.Ny
        verifyLessThan(testCase, fit(i), 3E-3)
    end
end

function testLpvlfr(testCase)
    sys = testCase.TestData.sys2;
    sysConv = LPVcore.conversionPlftss(sys);
    
    n = 5;
    np = sys.Np;
    
    rng(2);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = reshape([sys.SchedulingName';mat2cell(pCheck',ones(np,1), n)'],1,[]);
    
    s = extractLocal(sys, pCheck1); s = cat(4, s{:});
    sc = usubs(sysConv, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
end

function testDt(testCase)
    sys = testCase.TestData.sys3;
    sysConv = LPVcore.conversionPlftss(sys);

    verifyEqual(testCase, sys.Ts, sysConv.Ts);
    
    n = 5;
    np = sys.Np;
    
    rng(2);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = reshape([sys.SchedulingName';mat2cell(pCheck',ones(np,1), n)'],1,[]);
    
    s = extractLocal(sys, pCheck1); s = cat(4, s{:});
    sc = usubs(sysConv, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
end