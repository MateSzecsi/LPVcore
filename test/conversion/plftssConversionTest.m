function tests = plftssConversionTest
%plftssConversionTest Verify plftssConversionTest function (LPVTools must 
%be added to the path for these functions to pass).
    tests = functiontests(localfunctions);
end

%% Setup
function setupOnce(testCase)
rng(2);

nx = 4;
nu = 2;
ny = 3;

nd = 2;

p1 = tvreal('par1', [-3 3]);
p2 = tvreal('par2',[-5 10], [-10 2]);

Delta = randn(nd) + randn(nd) * p1 + randn(nd) * p2;

G1 = rss(nx, ny + nd, nu + nd);
G1 = G1 / (2 * hinfnorm(G1));
G2 = G1;
G1.D(1:nd,1:nd) = 0;
G3 = G2; G3.Ts = 0.1; % DT

sys1 = lft(Delta, G1);
sys2 = lft(Delta, G2);
sys3 = lft(Delta, G3);

sys1.StateName = {'a1','a2','a3','a4'};
sys1.StateUnit = repmat({'cm'},nx,1);
sys1.TimeUnit = 'hours';
sys1.InputName = 'b';
sys1.InputUnit = {'N1','N2'};
sys1.InputGroup = struct('groupIn1',1,'groupIn2',1);
sys1.OutputName = {'c1','c2','c3'};
sys1.OutputUnit = repmat({'km'},ny,1);
sys1.OutputGroup = struct('groupOut1',[1 2],'groupOut2',3);
sys1.Name = 'dynamical systemm';
sys1.Notes = "A note";
sys1.UserData = 'user data';

% Add data to testcase
testCase.TestData.sys1 = sys1;
testCase.TestData.sys2 = sys2;
testCase.TestData.sys3 = sys3;
end

function testLpvss(testCase)
    sys = testCase.TestData.sys1;
    sysConv = LPVcore.plftssConversion(sys);
    
    n = 5;
    np = length(fieldnames(sys.Parameter));
    
    rng(1);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = reshape([fieldnames(sys.Parameter)';mat2cell(pCheck',ones(np,1), n)'],1,[]);
    
    sc = extractLocal(sysConv, pCheck1); sc = cat(4, sc{:});
    s = usubs(sys, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
    
    % Check properties
    propertyNames = {'StateName','StateUnit','TimeUnit','InputName',...
            'InputUnit','InputDelay','InputGroup','OutputName','OutputUnit',...
            'OutputGroup','Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        verifyEqual(testCase, sys.(propertyNames{i}), sysConv.(propertyNames{i}));   
    end

    % Time-domain simulation to verify intput-output behavior matches
    % approximately. Won't be exactly the same because LPVTools uses
    % slightly different approach.
    N = 100;
    u = randn(N, size(sys, 2));
    p = rand(N, np);
    t = linspace(0, 1, N);
    yConv = lsim(sysConv, p, u, t);
    y = lpvlsim(sys, struct('par1',p(:,1),'par2',p(:,2),'time',t), ...
        u, t, zeros(size(sys.A,1), 1));
    
    fit = goodnessOfFit(y, yConv, 'NMSE');
    
    for i=1:size(sys,1)
        verifyLessThan(testCase, fit(i), 7E-2)
    end
end

function testLpvlfr(testCase)
    sys = testCase.TestData.sys2;
    sysConv = LPVcore.plftssConversion(sys);
    
    n = 5;
    np = length(fieldnames(sys.Parameter));
    
    rng(2);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = reshape([fieldnames(sys.Parameter)';mat2cell(pCheck',ones(np,1), n)'],1,[]);
    
    sc = extractLocal(sysConv, pCheck1); sc = cat(4, sc{:});
    s = usubs(sys, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
end

function testDt(testCase)
    sys = testCase.TestData.sys3;
    sysConv = LPVcore.plftssConversion(sys);

    verifyEqual(testCase, sys.Ts, sysConv.Ts);
    
    n = 5;
    np = length(fieldnames(sys.Parameter));
    
    rng(2);
    pCheck = randn(n, np);
    pCheck1 = mat2cell(pCheck,ones(n,1), np);
    pCheck2 = reshape([fieldnames(sys.Parameter)';mat2cell(pCheck',ones(np,1), n)'],1,[]);
    
    sc = extractLocal(sysConv, pCheck1); sc = cat(4, sc{:});
    s = usubs(sys, pCheck2{:});
    
    verifyTrue(testCase, all(norm(s-sc,'inf') <= 1e-12));
end