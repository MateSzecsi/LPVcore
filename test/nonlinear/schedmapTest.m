function tests = schedmapTest
%nlssTest Test nlss class.
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    np = 3;
    map = @(x,u) [sin(x(1,:)).^2; cos(x(2,:)); 3 * u(1,:)];
    schmap = LPVcore.schedmap(map, np);

    testCase.TestData.np = np;
    testCase.TestData.map = map;
    testCase.TestData.schmap = schmap;
end

%% Test SCHEDMAP creation
function testCreation(testCase)
    np = testCase.TestData.np;
    map = testCase.TestData.map;
    schmap = testCase.TestData.schmap;

    % Test properties
    verifyEqual(testCase, schmap.Np, np);
    verifyEqual(testCase, schmap.map, map);

    % Test errors
    verifyError(testCase, @() LPVcore.schedmap(@(x) x, 2), 'LPVcore:funCheck');
    verifyError(testCase, @() LPVcore.schedmap(@(u) u, 2), 'LPVcore:funCheck');
end

%% Test EVAL method
function testEval(testCase)
    map = testCase.TestData.map;
    schmap = testCase.TestData.schmap;

    xTest = randn(2,1);
    uTest = randn(1);

    pTest = map(xTest, uTest);

    verifyEqual(testCase, eval(schmap, xTest, uTest), pTest);
end