classdef nlss2lpvgridssTest < matlab.unittest.TestCase
    properties
        testIndex
        xDotData
        yData
        grid
        testSystems
    end
    
    properties (TestParameter)
        errorsys = {LPVcore.nlss(@(x,u) -3 + x, @(x,u) x, 1, 1, 1), ... % f(0,0) ≠ 0
                    LPVcore.nlss(@(x,u) x, @(x,u) -1 + x, 1, 1, 1), ... % h(0,0) ≠ 0
                    LPVcore.nlss(@(x,u) x, @(x,u) x, 1, 1, 1, 0, 0)};   % No symbolic rep.
    end
    
    methods(TestClassSetup)
        % Shared setup for the entire test class
        function setupTestData(testCase)
            testCase.grid = struct('x1', -3:.5:3, 'x2', 1:1:5, 'u', -5:2:5);
            
            ind = {1 2 3};
            testCase.testIndex = ind;
            xdat = [testCase.grid.x1(ind{1}); testCase.grid.x2(ind{2})];
            udat = testCase.grid.u(ind{3});

            f = @(x,u) [-0.1 * x(2, :) + 10 * x(1,:); -0.3 * sin(x(1, :)) - x(2,:).^3 + u(1, :).^2];
            h = @(x,u) [cos(x(1, :)) - 1;x(1, :);x(2, :)];
    
            testCase.xDotData = f(xdat, udat);
            testCase.yData = h(xdat, udat);

            testCase.testSystems = {LPVcore.nlss(f, h, 2, 1, 3), LPVcore.nlss(f, h, 2, 1, 3, 0.1)};
        end
    end
    
    methods(Test)
        % Test conversion for different options
        function testNonlinearConversion(testCase)
            for i = 1:2
                syslpv = LPVcore.nlss2lpvgridss(testCase.testSystems{i}, ...
                    testCase.grid);

                % Check CT/DT
                testCase.verifyEqual(syslpv.Ts, testCase.testSystems{i}.Ts);
                
                % Check if f(x,u) = A(map(x,u))*x + B(map(x,u))*u and
                % h(x,u) = C(map(x,u))*x + D(map(x,u))*u
                ind = testCase.testIndex;
                xdat = [testCase.grid.x1(ind{1}); testCase.grid.x2(ind{2})];
                udat = testCase.grid.u(ind{3});

                sysLocal = syslpv.ModelArray(:,:,ind{:});

                testCase.verifyEqual(sysLocal.A * xdat + ...
                    sysLocal.B * udat, testCase.xDotData, "RelTol", eps); % test xdot value
                testCase.verifyEqual(sysLocal.C * xdat + ...
                    sysLocal.D * udat, testCase.yData, "RelTol", eps);    % test y value
            end
        end

        function checkLtiCaseConversion(testCase)
            rng(2);
            nx = 3;
            nu = 2;
            ny = 1;
            pvgrid = struct('x1',[1 2], 'x2', 1, 'x3', 2, 'u1', .5, 'u2', .21);

            % CT
            sysCT = rss(nx, ny, nu);
            sysCTnl = LPVcore.nlss(@(x,u) sysCT.A * x + sysCT.B * u, ...
                                   @(x,u) sysCT.C * x + sysCT.D * u, ...
                                   nx, nu, ny);

            syslpvCT = LPVcore.nlss2lpvgridss(sysCTnl, pvgrid);
            testCase.verifyLessThanOrEqual(hinfnorm(syslpvCT.ModelArray(:,:,1) - sysCT), 1e-10);

            % DT
            sysDT = drss(nx, ny, nu);
            sysDTnl = LPVcore.nlss(@(x,u) sysDT.A * x + sysDT.B * u, ...
                                   @(x,u) sysDT.C * x + sysDT.D * u, ...
                                   nx, nu, ny, sysDT.Ts);
            syslpvDT = LPVcore.nlss2lpvgridss(sysDTnl, pvgrid);
            testCase.verifyLessThanOrEqual(hinfnorm(syslpvDT.ModelArray(:,:,1) - sysDT), 1e-10);
        end
        
        % Test if errors for certain systems
        function testError(testCase, errorsys)
            fun = @() LPVcore.nlss2lpvss(errorsys);
            testCase.verifyError(fun, 'LPVcore:systemCheck');
        end
    end
    
end