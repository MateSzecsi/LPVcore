classdef nlss2lpvssTest < matlab.unittest.TestCase
    properties
        xData
        uData
        xDotData
        yData
        testSystems
    end
    
    properties (TestParameter)
        opt = {'numerical', 'analytical'};
        factorOpt = {'element', 'factor'};
        errorsys = {LPVcore.nlss(@(x,u) -3 + x, @(x,u) x, 1, 1, 1), ... % f(0,0) ≠ 0
                    LPVcore.nlss(@(x,u) x, @(x,u) -1 + x, 1, 1, 1), ... % h(0,0) ≠ 0
                    LPVcore.nlss(@(x,u) x, @(x,u) x, 1, 1, 1, 0, 0)};   % No symbolic rep.
    end
    
    methods(TestClassSetup)
        % Shared setup for the entire test class
        function setupTestData(testCase)
            rng(1);

            xdat = randn(2,1);
            udat = randn(1);
            
            testCase.xData = xdat;
            testCase.uData = udat;

            f = @(x,u) [-0.1 * x(2, :) + 10 * x(1,:); -0.3 * sin(x(1, :)) - x(2,:).^3 + u(1, :).^2];
            h = @(x,u) [cos(x(1, :)) - 1;x(1, :);x(2, :)];
    
            testCase.xDotData = f(xdat, udat);
            testCase.yData = h(xdat, udat);

            testCase.testSystems = {LPVcore.nlss(f, h, 2, 1, 3), LPVcore.nlss(f, h, 2, 1, 3, 0.1)};
        end
    end
    
    methods(Test)
        % Test conversion for different options
        function testNonlinearConversion(testCase, opt, factorOpt)
            for i = 1:2
                [syslpv, map] = LPVcore.nlss2lpvss(testCase.testSystems{i}, ...
                    opt, factorOpt);

                % Check CT/DT
                testCase.verifyEqual(syslpv.Ts, testCase.testSystems{i}.Ts);
                
                % Check if f(x,u) = A(map(x,u))*x + B(map(x,u))*u and
                % h(x,u) = C(map(x,u))*x + D(map(x,u))*u
                xdat = testCase.xData;
                udat = testCase.uData;

                pVal = eval(map, xdat, udat);
                
                sysLocal = extractLocal(syslpv, pVal');

                testCase.verifyEqual(sysLocal.A * xdat + ...
                    sysLocal.B * udat, testCase.xDotData); % test xdot value
                testCase.verifyEqual(sysLocal.C * xdat + ...
                    sysLocal.D * udat, testCase.yData);    % test y value
            end
        end

        function testExampleIssue467(testCase, opt, factorOpt)
            % The example from issue 467
            % https://gitlab.com/tothrola/LPVcore/-/issues/467

            % System
            f = @(x,u) [x(1)^2 - x(2)^2 + u; -3*x(1)^2 - x(2)^2];
            h = @(x,u) x(1);
            
            sys = LPVcore.nlss(f, h, 2, 1, 1);
           
            % LPV conversion
            [syspv, eta] = LPVcore.nlss2lpvss(sys, opt, factorOpt);
            
            % Test point
            xTest = randn(2,1);
            uTest = randn(1);
            
            pTest = eta.map(xTest, uTest);
            
            % Compute LPV dynamics at test point
            sysLocal = extractLocal(syspv, pTest');

            % Compute responses at test point
            xdotTest = sysLocal.A*xTest + sysLocal.B*uTest;
            xdot = sys.f(xTest, uTest);

            % Verify
            testCase.verifyEqual(xdot, xdotTest, "AbsTol", 10 * eps);
        end

        function checkLtiCaseConversion(testCase, opt, factorOpt)
            rng(1);
            nx = 3;
            nu = 2;
            ny = 1;
            
            % CT
            sysCT = rss(nx, ny, nu);
            sysCTnl = LPVcore.nlss(@(x,u) sysCT.A * x + sysCT.B * u, ...
                                   @(x,u) sysCT.C * x + sysCT.D * u, ...
                                   nx, nu, ny);

            [syslpvCT, mapCT] = LPVcore.nlss2lpvss(sysCTnl, opt, factorOpt);
            testCase.verifyEqual(syslpvCT, sysCT);
            testCase.verifyEqual(mapCT, []);

            % DT
            sysDT = drss(nx, ny, nu);
            sysDTnl = LPVcore.nlss(@(x,u) sysDT.A * x + sysDT.B * u, ...
                                   @(x,u) sysDT.C * x + sysDT.D * u, ...
                                   nx, nu, ny, sysDT.Ts);
            [syslpvDT, mapDT] = LPVcore.nlss2lpvss(sysDTnl, opt, factorOpt);
            testCase.verifyEqual(syslpvDT, sysDT);
            testCase.verifyEqual(mapDT, []);
        end
        
        % Test if errors for certain systems
        function testError(testCase, errorsys)
            fun = @() LPVcore.nlss2lpvss(errorsys);
            testCase.verifyError(fun, 'LPVcore:systemCheck');
        end
    end
    
end