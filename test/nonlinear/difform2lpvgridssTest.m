classdef difform2lpvgridssTest < matlab.unittest.TestCase
    properties
        grid
        testIndex
        M
        testSystems
    end
    
    methods(TestClassSetup)
        % Shared setup for the entire test class
        function setupTestData(testCase)
            testCase.grid = struct('x1', -3:.5:3, 'x2', 1:1:5, 'u', -5:2:5);
            
            ind = {1 2 3};
            testCase.testIndex = ind;
            xdat = [testCase.grid.x1(ind{1}); testCase.grid.x2(ind{2})];
            udat = testCase.grid.u(ind{3});

            f = @(x,u) [-0.1 * x(2, :) + 10 * x(1,:); -0.3 * sin(x(1, :)) - x(2,:).^3 + u(1, :).^2];
            h = @(x,u) [cos(x(1, :)) - 1;x(1, :);x(2, :)];

            dsys1 = LPVcore.difform(LPVcore.nlss(f, h, 2, 1, 3));
            dsys2 = LPVcore.difform(LPVcore.nlss(f, h, 2, 1, 3, 0.1));

            testCase.M = [dsys1.A(xdat, udat), dsys1.B(xdat, udat);
                          dsys1.C(xdat, udat), dsys1.D(xdat, udat)];

            testCase.testSystems = {dsys1, dsys2};
        end
    end
    
    methods(Test)
        % Test conversion for different options
        function testNonlinearConversion(testCase)
            for i = 1:2
                syslpv = LPVcore.difform2lpvgridss(testCase.testSystems{i}, ...
                    testCase.grid);

                % Check CT/DT
                testCase.verifyEqual(syslpv.Ts, testCase.testSystems{i}.Ts);
                
                % Check if [A(x,u), B(x,u);C(x,u), D(x,u)] =  
                % [Ap(map(x,u)), Bp(map(x,u)); Cp(map(x,u)), Dp(map(x,u))]
                ind = testCase.testIndex;
                
                sysLocal = syslpv.ModelArray(:,:,ind{:});
                Mlocal = [sysLocal.A, sysLocal.B;sysLocal.C, sysLocal.D];

                testCase.verifyEqual(Mlocal, testCase.M); % Matrix values
            end
        end
    end
end