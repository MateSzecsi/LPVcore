classdef difform2lpvssTest < matlab.unittest.TestCase
    properties
        xData
        uData
        M
        testSystems
    end
    
    properties (TestParameter)
        factorOpt = {'element', 'factor'};
    end
    
    methods(TestClassSetup)
        % Shared setup for the entire test class
        function setupTestData(testCase)
            rng(1);

            xdat = randn(2,1);
            udat = randn(1);
            
            testCase.xData = xdat;
            testCase.uData = udat;

            f = @(x,u) [-0.1 * x(2, :) + 10 * x(1,:); -0.3 * sin(x(1, :)) - x(2,:).^3 + u(1, :).^2];
            h = @(x,u) [cos(x(1, :)) - 1;x(1, :);x(2, :)];

            dsys1 = LPVcore.difform(LPVcore.nlss(f, h, 2, 1, 3));
            dsys2 = LPVcore.difform(LPVcore.nlss(f, h, 2, 1, 3, 0.1));

            testCase.M = [dsys1.A(xdat, udat), dsys1.B(xdat, udat);
                          dsys1.C(xdat, udat), dsys1.D(xdat, udat)];

            testCase.testSystems = {dsys1, dsys2};
        end
    end
    
    methods(Test)
        % Test conversion for different options
        function testNonlinearConversion(testCase, factorOpt)
            for i = 1:2
                [syslpv, map] = LPVcore.difform2lpvss(testCase.testSystems{i}, ...
                    factorOpt);

                % Check CT/DT
                testCase.verifyEqual(syslpv.Ts, testCase.testSystems{i}.Ts);
                
                % Check if [A(x,u), B(x,u);C(x,u), D(x,u)] =  
                % [Ap(map(x,u)), Bp(map(x,u)); Cp(map(x,u)), Dp(map(x,u))]
                xdat = testCase.xData;
                udat = testCase.uData;

                pVal = eval(map, xdat, udat);
                
                sysLocal = extractLocal(syslpv, pVal');
                Mlocal = [sysLocal.A, sysLocal.B;sysLocal.C, sysLocal.D];

                testCase.verifyEqual(Mlocal, testCase.M); % Matrix values
            end
        end
    end
end