function tests = simulinkNonlinearTest
%simulinkNonlinearTest Test nonlinear simulink blocks.
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    rng(1);

    % Create nonlinear system (driven Van der Pol oscillator)
    mu = 3.0;

    f = @(x,u) [x(2); mu*(1-x(1)^2)*x(2)-x(1)+u];
    h = @(x,u) [cos(x(1));x(1);x(2)];
    
    sys = LPVcore.nlss(f, h, 2, 1, 3);

    Ts = 0.1;
    sysdt = c2d(sys, Ts, "eul");
    
    x0 = randn(2,1);

    testCase.TestData.mu = mu;
    testCase.TestData.Ts = Ts;
    testCase.TestData.sys = sys;
    testCase.TestData.sysdt = sysdt;
    testCase.TestData.x0 = x0;
end

%% NLSS block in CT
function testLpvssCt(testCase)
    x0 = testCase.TestData.x0;
    sys = testCase.TestData.sys;
    mu = testCase.TestData.mu;

    % Simulate
    simIn = Simulink.SimulationInput('simNlssCtTest');
    simIn = setVariable(simIn,'mu',mu);
    simIn = setVariable(simIn,'x0',x0);
    simIn = setVariable(simIn,'sys',sys);
    out = sim(simIn);
    
    % Check
	verifyLessThanOrEqual(testCase, norm(out.nlss - out.manual), 1e3 * eps);   
end

%% NLSS block in DT
function testLpvssDt(testCase)
    x0 = testCase.TestData.x0;
    sysdt = testCase.TestData.sysdt;
    mu = testCase.TestData.mu;
    Ts = testCase.TestData.Ts;

    % Simulate
    simIn = Simulink.SimulationInput('simNlssDtTest');
    simIn = setVariable(simIn,'mu',mu);
    simIn = setVariable(simIn,'Ts',Ts);
    simIn = setVariable(simIn,'x0',x0);
    simIn = setVariable(simIn,'sysdt',sysdt);
    out = sim(simIn);
    
    % Check
	verifyLessThanOrEqual(testCase, norm(out.nlss - out.manual), 1e2 * eps);   
end