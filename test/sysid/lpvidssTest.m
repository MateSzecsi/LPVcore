function tests = lpvidssTest
%LPVIDSSTEST Test behavior of LPVIDSS
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(~) % Called once before the tests in this file are run
    rng(1);
end

%% Test functions
function testCreation(testCase)
    p = preal('p', 'dt');
    nx = 3; nu = 4; ny = 5;
    A = ones(nx) * p;
    B = ones(nx, nu);
    K = ones(nx, ny);
    C = ones(ny, nx);
    D = ones(ny, nu);
    G = ones(nx);
    H = ones(ny);
    incorrect = ones(nx + nu + ny);
    % Creation with correct matrix
    sys_innovation = lpvidss(A, B, C, D, 'innovation', K, []);
    sys_general = lpvidss(A, B, C, D, 'general', G, H);
    % Test display
    disp(sys_innovation);
    disp(sys_general);
    % Creation with incorrect matrix sizes
    verifyError(testCase, @() lpvidss(incorrect, B, C, D, 'innovation', K, []), '');
    verifyError(testCase, @() lpvidss(A, incorrect, C, D, 'innovation', K, []), '');
    verifyError(testCase, @() lpvidss(A, B, incorrect, D, 'innovation', K, []), '');
    verifyError(testCase, @() lpvidss(A, B, C, incorrect, 'innovation', K, []), '');
    verifyError(testCase, @() lpvidss(A, B, C, D, 'innovation', incorrect, []), '');
    verifyError(testCase, @() lpvidss(A, B, C, D, 'general', incorrect, H), '');
    verifyError(testCase, @() lpvidss(A, B, C, D, 'innovation', G, incorrect), '');
end

function testLSim(testCase)
    N = 10;

    A = 0.9;
    B = 1.2;
    C = -1;
    D = 1.5;

    sys = lpvidss(A, B, C, D, 'innovation');
    sys_lti = idss(A, B, C, D);

    sys.NoiseVariance = 0;
    sys_lti.NoiseVariance = 0;

    u = randn(N, sys.Nu);
    p = zeros(N, sys.Np);
    x0 = randn(sys.Nx);

    [y, t, w, yp] = lsim(sys, p, u, [], x0);
    y_lti = lsim(sys_lti, u, [], x0);

    verifyTrue(testCase, norm(y - y_lti) < 1E-10);
    % y and yp should be equal if no noise is present
    verifyEqual(testCase, y, yp);
    % t should have spacing 1 if sampling time is not specified
    verifyEqual(testCase, t, (0:N-1).');
    % noise is 0 since NoiseVariance was set to 0
    verifyEqual(testCase, norm(w), 0);
end

function testFindStates(testCase)
    % Create equivalent IDSS and LPVIDSS objects and check whether the FINDSTATES
    % command returns (approximately) the same estimate for both (given
    % identical simulation input data).
    NoiseVariance = 0.0001;
    ny = 1;
    nu = 1;
    nx = 2;
    A = 0.4 * rand(nx);
    B = rand(nx, nu);
    K = rand(nx, ny);
    C = rand(ny, nx);
    D = rand(ny, nu);
    sys = idss(A, B, C, D, K);
    sys.NoiseVariance = NoiseVariance;
    lpvsys = lpvidss(A, B, C, D, 'innovation', K);
    lpvsys.NoiseVariance = NoiseVariance;
    
    % Simulation
    N = 100;
    u = randn(N, nu);
    p = zeros(N, lpvsys.Np);
    e = randn(N, ny);
    x0 = randn(nx, 1);
    y = sim(sys, u, simOptions('AddNoise', true, 'NoiseData', e, 'InitialCondition', x0));
    ylpv = lsim(lpvsys, p, u, [], x0, sqrt(NoiseVariance) * e);
    data = iddata(y, u);
    lpvdata = lpviddata(ylpv, p, u);
    x0est = findstates(sys, data);
    lpvx0est = findstates(lpvsys, lpvdata);

    verifyLessThan(testCase, norm(x0est - lpvx0est) / norm(x0est), 0.01);
    verifyLessThan(testCase, norm(x0 - lpvx0est) / norm(x0), 0.05);
end

function testPredict(testCase)
    N = 10;

    A = 0.1 * randn(2);
    B = randn(2, 1);
    C = randn(1, 2);
    D = randn;

    sys = lpvidss(A, B, C, D, 'innovation');
    sys_lti = idss(A, B, C, D);

    sys.NoiseVariance = 0;
    sys_lti.NoiseVariance = 0;

    u = randn(N, sys.Nu);
    p = zeros(N, sys.Np);
    x0 = randn(sys.Nx, 1);

    y = lsim(sys, p, u, [], x0);
    y_lti = lsim(sys_lti, u, [], x0);

    data = lpviddata(y, p, u);
    data_lti = iddata(y_lti, u);

    opt = predictOptions('InitialCondition','z');

    y_pred = predict(sys, data, 1);
    y_lti_pred = predict(sys_lti, data_lti, 1, opt);
    
    verifyTrue(testCase, norm(y_pred - y_lti_pred.y) < 1E-10);
end

function testTimemap(testCase)
    p = preal('p', 'dt');
    q = preal('q', 'dt');
    nx = 3; nu = 4; ny = 5;
    A = ones(nx);
    B = ones(nx, nu);
    C = ones(ny, nx);
    D = ones(ny, nu);
    K = ones(nx, ny);
    G = ones(nx);
    H = ones(ny);
    P = lpvidss(A*p,B,C*q,D,'innovation',K);
    P2 = lpvidss(A*p,B,C*q,D,'general',G,H);
    
    % Test setting properties
    propNames = {'Range','RateBound','Name','Unit','Map'};
    propValues = {{[-5, 5]; [2, 3]}, {[-10, 10]; [5, 9]}, ...
        {'a'; 'b'}, {'m'; 'km'}, randi([-10 0],size(P.SchedulingTimeMap.Map))};

    for i = 1:numel(propNames)
        P.SchedulingTimeMap.(propNames{i}) = propValues{i};
        P2.SchedulingTimeMap.(propNames{i}) = propValues{i};
    end

    for i = 1:numel(propNames)
        verifyEqual(testCase, P.SchedulingTimeMap.(propNames{i}), propValues{i});
        verifyEqual(testCase, P2.SchedulingTimeMap.(propNames{i}), propValues{i});
    end

    % Verify the system matrices have the same timemap
    verifyEqual(testCase, P.SchedulingTimeMap, P.A.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P.B.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P.C.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P.D.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P.K.timemap);

    verifyEqual(testCase, P.SchedulingTimeMap, P2.A.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P2.B.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P2.C.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P2.D.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P2.G.timemap);
    verifyEqual(testCase, P.SchedulingTimeMap, P2.H.timemap);

    check = @() timeMapPropSet(P, 'Range', {[-5, 5], [-3 3], [-3 3]});

    % Check for error if different dimension scheduling is given
    verifyError(testCase, check, 'MATLAB:InputParser:ArgumentFailedValidation')
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Local functions
function timeMapPropSet(P, prop, val)
    P.SchedulingTimeMap.(prop) = val;
end