function tests = rivTest
%RIVTEST Test identification using RIV
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(~) % Called once before the tests in this file are run
    rng(1);
end

%% Test functions
function testRiv(testCase)
    Ts = 0.1;  % s
    NoiseVariance = 1E-1;

    p = preal('p', 'dt');

    B = {1+p, 1+p};
    F = {1, 0.1+0.1*p};

    sys = lpvidpoly([], B, {1, 1}, {1, 1}, F, NoiseVariance, Ts);

    %% Data generation
    N = 1000;  % samples

    % Train
    uData = randn(N, sys.Nu);
    pData = rand(N, sys.Np);
    tData = Ts * (1:N);
    [y, ~, ~, yp] = lsim(sys, pData, uData, tData);
    fprintf('SNR is %.1f dB\n', LPVcore.snr(y, y - yp));

    data_est = lpviddata(y, pData, uData, Ts);

    %% Identification
    % The system will be identified with the provided template model
    % structure. Once with randomly initialized coefficients ('template')
    % and once with an ARX pre-estimation ('polypre')
    template_sys = randlpvidpoly(sys);
    initialization_methods = {'template', 'polypre'};

    for i=1:numel(initialization_methods)
        fprintf('Testing with initialization method %s\n', initialization_methods{i});
        tic;
        sys_est_laurain = lpvriv(data_est, template_sys, ...
            lpvrivOptions('Implementation', 'laurain', 'Display', 'off', ...
                'Initialization', initialization_methods{i}));
        toc;
        [~, fit] = compare(data_est, sys_est_laurain);
        fprintf('\tFit with Laurain implementation is %.5f%%\n', fit);
        verifyEqual(testCase, fit, 85.8, 'AbsTol', 5);
        
        tic;
        sys_est_lpvcore = lpvriv(data_est, template_sys, ...
            lpvrivOptions('Implementation', 'lpvcore', 'Display', 'off', ...
                'Initialization', initialization_methods{i}));
        toc;
        [~, fit] = compare(data_est, sys_est_lpvcore);
        fprintf('\tFit with LPVcore implementation is %.5f%%\n', fit);
        verifyEqual(testCase, fit, 85.8, 'AbsTol', 5);
    
        % Check whether the identified parameters are similar
        pvec_true = getpvec(sys, 'free');
        pvec_laurain = getpvec(sys_est_laurain, 'free');
        pvec_lpvcore = getpvec(sys_est_lpvcore, 'free');
        pvec_diff = norm(pvec_laurain - pvec_lpvcore) / norm(pvec_true);
        fprintf('\tRel. 2-norm difference in parameters between implementations: %d\n', ...
            pvec_diff);
    end

    progressgui.close()
    
    
    %% Invalid invocations
    % Too few arguments
    verifyError(testCase, @() lpvriv(data_est), 'MATLAB:narginchk:notEnoughInputs');
    % Inequal parametrizations
    template_sys = lpvidpoly([], {1, p}, {1, 1}, {1, 1}, {1, p}, NoiseVariance, Ts);
    verifyError(testCase, @() lpvriv(data_est, template_sys), '');
    % Non-constant noise process
    template_sys = lpvidpoly([], B, {1, p}, {1, 1}, F, NoiseVariance, Ts);
    verifyError(testCase, @() lpvriv(data_est, template_sys), '');
    % Continuous-time system
    template_sys = lpvidpoly([], {preal('p', 'ct')}, {1, 1}, {1, 1}, {1, preal('p', 'ct')}, NoiseVariance, 0);
    verifyError(testCase, @() lpvriv(data_est, template_sys), '');
end
