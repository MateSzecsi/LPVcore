function tests = lpvgridfrdTest
%LPVGRIDFRDTEST Test behavior of LPVGRIDFRD
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(~) % Called once before the tests in this file are run
end

%% Test functions
function testCreation(testCase)
    InputName = {'a', 'b'};
    OutputName = {'y1', 'y2', 'y3'};
    nu = numel(InputName);
    ny = numel(OutputName);
    % Each local system must have same number of frequencies
    f = logspace(-2, 3, 100);
    sys(:, :, 1) = frd(rss(10, ny, nu), f);
    sys(:, :, 2) = frd(rss(10, ny, nu), f);
    % grid and sys must be compatible
    verifyError(testCase, @() lpvgridfrd(sys, ...
        struct('p', [1, 2, 3])), '');
    verifyError(testCase, @() lpvgridfrd(sys, ...
        struct('p', [1, 2], 'q', [1, 2])), '');
    verifyError(testCase, @() lpvgridfrd(sys, ...
        struct('q', 1)), '');
    lpvsys = lpvgridfrd(sys, struct('p', [-1, 1]));
    % System plots
    bode(lpvsys);
    sigma(lpvsys);
    bodemag(lpvsys);
    close;
    disp(lpvsys);
end

function testInterconnection(testCase)
    % Test interconnection functions
    f = logspace(-2, 3, 100);
    sys(:, :, 2) = frd(rss(10, 1, 1), f);
    sys(:, :, 1) = frd(rss(10, 1, 1), f);
    % Define 2 lpvgridfrd objects with incompatible grid
    s1 = lpvgridfrd(sys, struct('p', [0, 1]));
    s2 = lpvgridfrd(sys, struct('q', [0, 1]));
    % Verify that the interconnection with s1 and s2 always fail (due to
    % incompatible grid)
    verifyError(testCase, @() [s1, s2], '');
    verifyError(testCase, @() [s1; s2], '');
    verifyError(testCase, @() blkdiag(s1, s2), '');
    verifyError(testCase, @() append(s1, s2), '');
    verifyError(testCase, @() lft(s1, s2), '');
    verifyError(testCase, @() feedback(s1, s2), '');
    verifyError(testCase, @() series(s1, s2), '');
    verifyError(testCase, @() parallel(s1, s2), '');

    % Define 2 lpvgridss objects with compatible grid
    s2 = lpvgridfrd(sys, struct('p', [0, 1]));
    % Check that interconnection works same as built-in commands
    funcs = {@horzcat, @vertcat, @blkdiag, @append, @lft, @feedback, @series, @parallel};
    for i=1:numel(funcs)
        func = funcs{i};
        s = func(s1, s2);
        verifyEqual(testCase, s.ModelArray, func(sys, sys));
    end
end

function testConnect(testCase)
    %% Example from GitLab issue #363
    % See https://gitlab.com/tothrola/LPVcore/-/issues/363

    rng(1);
    f = logspace(-2, 3, 100);
    G = frd(rss(2,1,1,3), f);
    grid = struct('p',1:3);
    G.InputName = 'u';
    G.OutputName = 'y';
    
    Gg = lpvgridfrd(G,grid);
    
    Pg = connect(Gg, sumblk('z = y + d'), {'d','u'}, {'z'});
    P = connect(G, sumblk('z = y + d'), {'d','u'}, {'z'});
    
    verifyTrue(testCase, all(hinfnorm(P-Pg.ModelArray) < 1E-10));
    verifyTrue(testCase, LPVcore.doSignalPropertiesMatch(P, Pg.ModelArray))

    %% Test case when first and third argument are lpvgridss
    G2 = G;
    G2.InputName = 'z';
    G2.OutputName = 'o';

    Gg2 = Gg;
    Gg2.InputName = 'z';
    Gg2.OutputName = 'o';

    Pg = connect(Gg, sumblk('z = y + d'), Gg2, {'d','u'}, {'o'});
    P = connect(G, sumblk('z = y + d'), G2, {'d','u'}, {'o'});


    verifyTrue(testCase, all(hinfnorm(P-Pg.ModelArray) < 1E-10));
    verifyTrue(testCase, LPVcore.doSignalPropertiesMatch(P, Pg.ModelArray))
end
