classdef schedmap
%SCHEDMAP Constructs a scheduling map object.
%
%   Syntax:
%       sched = schedmap(map, Np)
%
%   Inputs:
%       map (function_handle): Function handle of the form 
%          '@(x,u) [... x(1,:) ... x(2,:); ... u(1,:) ... x(1,:) ...; ...]'
%           which represents the scheduling
%           map.
%       Np (integer): Number of scheduling signals, i.e., the size of the
%           output of the scheduling map.
%
%   Example:
%       sched = LPVcore.schedmap(@(x,u) [sin(x(1,:)).^2; cos(x(2,:))], 2);
%
    
    properties
        map {mustBeCompatibleFunction}
        Np {mustBeInteger, mustBePositive}
    end
    
    methods
        function obj = schedmap(map, Np)
            narginchk(2,2);

            obj.map = map;
            obj.Np = Np;
        end
        
        function out = eval(obj, x, u)
            out = obj.map(x, u);
        end

        function disp(obj)
            fprintf('Scheduling-map:\n\n');
            fprintf('   p = η(x,u)\n\n');
            fprintf('with\n\n');
            fprintf('    Nr. of scheduling signals: %d\n', obj.Np);
        end
    end
end

function mustBeCompatibleFunction(f)
    if ~isempty(f)
        mustBeA(f, 'function_handle')
        if ~startsWith(func2str(f), '@(x,u)')
            eidType = 'LPVcore:funCheck';
            msgType = 'Custom function handle must start with "@(x,u)".';
            throwAsCaller(MException(eidType, msgType));
        end
    end
end