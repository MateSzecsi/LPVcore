function disp(obj)
% DISP Provide info on the nlss object
%
%   Syntax:
%       disp(sys)
%
%   Inputs:
%       sys: nlss object
%
    narginchk(1, 1);
    
    if isempty(obj)
        fprintf('Empty NL-SS model');
        return;
    end
    
    if isct(obj)
        Qstr  = '(d/dt)';
        emptyQstr = '      ';
    else
        Qstr  = '(q^-1)';
        emptyQstr = '      ';
    end
    
    % Model type
    if isct(obj)
        timeStr = 'Continuous-time';
    else
        timeStr = 'Discrete-time';
    end
    fprintf('%s NL-SS model\n\n', timeStr);
    
    % State equation
    fprintf('    %sx = f(x,u)\n', ...
        Qstr);
    % Output equation
    fprintf('    %sy = h(x,u)\n\n', ...
        emptyQstr);
    
    % Description of input-scheduling-output
    fprintf('with\n\n');
    fprintf('    Nr. of inputs:             %d\n', obj.Nu);
    fprintf('    Nr. of outputs:            %d\n', obj.Ny);
    fprintf('    Nr. of states:             %d\n', obj.Nx);
    fprintf('\n');
    
    % Sample time
    if ~isct(obj)
        if obj.Ts == -1; TsStr = 'unspecified';
        else;            TsStr = sprintf('%g s',obj.Ts); end
        
        fprintf('%s: %s.\n', 'Sample time', TsStr);
    end

end