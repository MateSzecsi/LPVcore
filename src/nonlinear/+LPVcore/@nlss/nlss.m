classdef nlss
%NLSS Nonlinear state-space system representation
%
%   Syntax:
%       sys = nlss(f, h, Nx, Nu, Ny) creates an object sys representing a
%       continuous-time state-space model:
%           dx/dt = f(x(t),u(t))
%            y(t) = h(x(t),u(t))
%       where f and h are function handles, the state-dimension is Nx, the
%       input-dimension is Nu and the output-dimension is Ny.
%
%       sys = nlss(f, h, Nx, Nu, Ny, Ts) creates an object sys representing
%       a discrete-time state-space model with sample time Ts (set Ts=-1 if
%       the sample time is undetermined):
%           x(t+1) = f(x(t),u(t))
%             y(t) = h(x(t),u(t))
%       where f and h are function handles, the state-dimension is Nx,
%       the input-dimension is Nu, the output-dimension is Ny.
%
%       sys = nlss(f, h, Nx, Nu, Ny, Ts, symopt) creates an nlss object 
%       which is internally not represented symbolically if symopt is 
%       false. This might be helpful if the system cannot be represented
%       symbolically (e.g., if it contains a lookup table). In this case
%       both the functions 'f' and 'h' must be of the form: 
%           @(x,u) F( x(1, :), u(1, :), ..., x(N, :), u(N, :) ).
%       Note that if the system is not represented symbolically internally
%       with 'symopt' set to 'false', the Jacobians of 'f' and 'h' of the
%       system cannot be computed. These are for example needed to compute 
%       the differential form of the system (using <a href="matlab:helpPopup LPVcore.difform">LPVcore.difform</a>) or to
%       convert it to an LPV representation (using <a href="matlab:helpPopup LPVcore.nlss2lpvss">LPVcore.nlss2lpvss</a>).
%
%   Inputs:
%       f (function_handle): Function handle representing the
%           state-transition map of the form '@(x,u) ...'
%       h (function_handle): Function handle representing the
%           output map of the form '@(x,u) ...'
%       Nx (int): State-dimension
%       Nu (int): Input-dimension
%       Ny (int): Output-dimension
%       Ts (numeric): Sample time
%       symopt (logical): Optional argument. Logical to enable or disable 
%           an internal symbolic representation of the system.
%
%   Example:
%       sys = LPVcore.nlss(@(x,u) [-x(2); 3.0*(1-x(1)^2)*x(2)-x(1) + u],...
%                          @(x,u) x(1), 2, 1, 1);
%

    properties (Dependent = true)
        f {mustBeCompatibleFunction}
        h {mustBeCompatibleFunction}
    end

    properties (Access = private)
        f_
        h_
    end

    properties
        Nx (1,1) {mustBeInteger, mustBeNonnegative}
        Nu (1,1) {mustBeInteger, mustBeNonnegative}
        Ny (1,1) {mustBeInteger, mustBeNonnegative}
        Ts (1,1) {mustBeCompatibleTs}
        TimeUnit {mustBeTextScalar} = 's'
        % To-do (05-02-2024): Implement following properties
%         StateName
%         StateUnit
%         InputName
%         InputUnit
%         OutputName
%         OutputUnit
    end

    properties (Hidden, SetAccess = protected)
        fSym    % Internal symbolic represenation of f
        hSym    % Internal symbolic represenation of h
        xSym
        uSym
        symbolic
    end

    methods
        function obj = nlss(f, h, Nx, Nu, Ny, Ts, symbolic)
            arguments
                f
                h
                Nx
                Nu
                Ny
                Ts = 0
                symbolic = true
            end

            % Set properties
            obj.Nx = Nx;
            obj.Nu = Nu;
            obj.Ny = Ny;
            obj.Ts = Ts;

            obj.symbolic = symbolic;

            % Compute symbolic representation of 'f' and 'h' (in the set
            % method of 'f' and 'h')
            obj.xSym = sym('x', [Nx, 1], 'real');
            obj.uSym = sym('u', [Nu, 1], 'real');
            
            obj.f = f;
            obj.h = h;
        end

        function obj = set.f(obj, fValue)
            if obj.symbolic
                try
                    obj.fSym = vpa(fValue(obj.xSym, obj.uSym));
                catch exception
                    eidType = 'LPVcore:nlss';
                    msg = sprintf("Error symbolically evaluating f:");
                    errorMsg = symbolicErrorMessage(exception);
                    msg = strjoin([msg, errorMsg], '\n\n');
                    throwAsCaller(MException(eidType, msg));
                end
                if ~(all(size(obj.fSym) == [obj.Nx, 1]))
                    eidType = 'LPVcore:nlss';
                    msgType = 'Outputsize of f does not match Nx.';
                    throwAsCaller(MException(eidType, msgType));
                end
    
                % As vars we take [obj.xSym;obj.xSym(1)] such that the function
                % is always vectorized (so that for a scalar x we get
                % @(x) x(1,:) instead of @(x) x).
                fTemp = matlabFunction(obj.fSym, "Vars", ...
                    {[obj.xSym;obj.xSym(1)], [obj.uSym;obj.uSym(1)]});
                % Replace (in1, in2) by (x, u)
                fTemp = str2func(replace(func2str(fTemp),{'in1','in2'}, ...
                    {'x','u'}));
                obj.f_ = fTemp;
            else
                obj.f_ = fValue;
            end
        end

        function value = get.f(obj)
            value = obj.f_;
        end

        function obj = set.h(obj, hValue)
            if obj.symbolic
                try
                    obj.hSym = vpa(hValue(obj.xSym, obj.uSym));
                catch exception
                    eidType = 'LPVcore:nlss';
                    msg = sprintf("Error symbolically evaluating h:");
                    errorMsg = symbolicErrorMessage(exception);
                    msg = strjoin([msg, errorMsg], '\n\n');
                    throwAsCaller(MException(eidType, msg));
                end
                if ~(all(size(obj.hSym) == [obj.Ny, 1]))
                    eidType = 'LPVcore:nlss';
                    msgType = 'Outputsize of h does not match Ny.';
                    throwAsCaller(MException(eidType, msgType));
                end
        
                % As vars we take [obj.xSym;obj.xSym(1)] such that the function
                % is always vectorized (so that for a scalar x we get
                % @(x) x(1,:) instead of @(x) x).
                hTemp = matlabFunction(obj.hSym, "Vars", ...
                    {[obj.xSym;obj.xSym(1)], [obj.uSym;obj.uSym(1)]});
                % Replace (in1, in2) by (x, u)
                hTemp = str2func(replace(func2str(hTemp),{'in1','in2'}, ...
                    {'x','u'}));
                obj.h_ = hTemp;
            else
                obj.h_ = hValue;
            end
        end

        function value = get.h(obj)
            value = obj.h_;
        end

        function out = isct(obj)
            out = (obj.Ts == 0);
        end

        function obj = c2d(obj, Ts, method)
            %C2D Convert continuous-time system to discrete-time system using specified
            %numerical integration method.
            %
            %   Syntax:
            %       obj = c2d(obj, Ts) converts the continuous-time NLSS object to a
            %       discrete-time system using Euler's method with sampling time Ts.
            %
            %       obj = c2d(obj, Ts, method) converts the continuous-time NLSS object
            %       to a discrete-time system using the specified numerical integration
            %       method. Valid input values for 'method' are 'eul' (Euler's method)
            %       and 'rk4' (4th order Runge-Kutta method).
            %
            %   Inputs:
            %       obj (nlss): input object representing the continuous-time system.
            %       Ts (numeric): sampling time used for discretization.
            %       method (char): (Optional) string input argument that specifies the
            %           numerical integration method to use for the conversion. Valid 
            %           input values are 'eul' (Euler's method) and 'rk4' (4th order
            %           Runge-Kutta method). Default value is 'eul'.
            %
            %   Outputs:
            %       obj: Discrete-time NLSS object.
            %

            arguments
                obj  {mustBeContinuousSystem}
                Ts (1,1) {mustBePositive}
                method {mustBeMember(method, {'eul','rk4'})} = 'eul';
            end

            switch method
                case 'eul'
                    obj.Ts = Ts;
                    obj.f = @(x,u) x + Ts * obj.f(x, u);
                case 'rk4'
                    obj.Ts = Ts;
                    phi1 = @(x,u) obj.f(x, u);
                    phi2 = @(x,u) obj.f(x + Ts/2 * phi1(x,u), u);
                    phi3 = @(x,u) obj.f(x + Ts/2 * phi2(x,u), u);
                    phi4 = @(x,u) obj.f(x + Ts * phi3(x,u), u);

                    obj.f = @(x,u) x + Ts/6 * (phi1(x, u) + ...
                        2 * phi2(x,u) + 2 * phi3(x, u) + phi4(x, u));
            end
        end

        function [y, tout, x] = sim(obj, u, t, x0, method)
            %SIM Simulate system response to input signal.
            %
            %   Syntax:
            %       [y, tout, x] = sim(obj, u, t, x0) simulates the system represented by
            %       the NLSS object 'obj' using the input signal 'u' and initial condition
            %       'x0'. The simulation is performed over the time vector (or time span) 
            %       't', and the output variables are the system response 'y', the time 
            %       vector 'tout', and the state vector 'x'.
            %
            %       [y, tout, x] = sim(obj, u, t, x0, method) to specify for a
            %       continuous-time system which solver needs to be used. By default
            %       'ode45' is used.
            %
            %   Inputs:
            %       obj (nlss): input object representing the system to simulate.
            %       u: input signal to the system. Can be either a function handle or a
            %           numeric vector (of size [Nt, Nu], where Nt is lenght of the
            %           time vector).
            %       t (numeric): time vector (or timespan) for simulation.
            %       x0 (numeric): initial condition for the state vector. Should have
            %           size [Nx x 1], where Nx is the number of states in 'obj'.
            %       method (string): Optional string used to indicate which solver is
            %           used to simulate a contiuous-time system, such as ode45, ode78,
            %           etc. By default: "ode45".
            %
            %   Outputs:
            %       y (numeric): Simulated output response of the system.
            %       tout (numeric): Time vector used for simulation. May differ
            %           from input 't' if the simulation involves numerical integration.
            %       x (numeric): Simulated state response of the system.
            %

            arguments
                obj
                u   {mustBeA(u,{'function_handle','numeric'})}
                t   {mustBeNumeric}
                x0  {mustBeNumeric}
                method {mustBeTextScalar} = "ode45";
            end
            narginchk(3,5)

            ct = isct(obj);

            if nargin == 3
                x0 = 0;
            end

            if numel(x0) == 1
                x0 = ones(obj.Nx, 1) * x0;
            end

            if isnumeric(u)
                nT = length(t);
                assert(all(size(u) == [nT, obj.Nu]),['Input vector size ' ...
                    'should be [lenght(t), Nu]'])
                ufun = @(tau) interp1(t, u, tau, 'previous')';
            else
                assert(all(size(u(0)) == [obj.Nu, 1]), ['Input ' ...
                    'function does not match input dimension, make ' ...
                    'sure it outputs a [Nu, 1] vector']);
                ufun = u;
            end

            assert(numel(x0) == obj.Nx, ['Initial condition size does ' ...
                'not match state dimension']);
            x0 = reshape(x0, [obj.Nx, 1]);

            simfun = @(t, x) obj.f(x, ufun(t));
            
            switch ct
                case 1
                    odeMethod = str2func(method);
                    [tout, x] = odeMethod(simfun, t, x0);
                case 0
                    [tout, x] = differenceSolve(simfun, t, obj.Ts, x0);
            end
            y = obj.h(x', ufun(reshape(tout,1,[])))';
            y = reshape(y, [], obj.Ny);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                         LOCAL FUNCTIONS                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mustBeCompatibleTs(Ts)
    mustBeNumeric(Ts)
    if ~(Ts == -1) && ~(Ts >= 0)
        eidType = 'LPVcore:TsCheck';
        msgType = ['Sampling Time must be -1 (undefined) or a ',...
                     'non-negative value.'];
        throwAsCaller(MException(eidType, msgType));
    end
end

function mustBeCompatibleFunction(f)
    mustBeA(f, 'function_handle')
    if ~startsWith(func2str(f), '@(x,u)')
        eidType = 'LPVcore:funCheck';
        msgType = 'Custom function handle must start with "@(x,u)".';
        throwAsCaller(MException(eidType, msgType));
    end
end

function mustBeContinuousSystem(sys)
    if ~isct(sys)
        eidType = 'LPVcore:sysCheck';
        msgType = 'System must be continuous-time.';
        throwAsCaller(MException(eidType, msgType));
    end
end

function [tout, x] = differenceSolve(fun, t, Ts, x0)
    if Ts == -1
        Ts = 1;
    end
    if numel(t) == 2
        assert(t(2) > t(1), 'End time should be larger than start time');
        assert(all(mod(t, Ts) == 0), ['Start and end time should be ' ...
            'a multiple of the sampling time of the system.']);
        tout = t(1):Ts:t(2);
        t = 0:Ts:(t(2)-t(1));
    else
        assert(all(diff(t) > 0), 'Time vector should be increasing.')
        tout = t;
        t = t - t(1);
    end
    tout = tout(:);
    
    index = round(t/Ts) + 1;
    nT = t(end)/Ts + 1;

    x = zeros(numel(x0), nT);
    x(:,1) = x0;

    for k = 1:nT-1
        x(:, k+1) = fun(t(k), x(:, k));
    end
    x = x(:,index)';
end

function errorMsg = symbolicErrorMessage(exception)
    errorMsg = strsplit(string(getReport(exception)),'\n');
    errorMsg = errorMsg(1:end-6);
    errorMsg(2:2:end-1) = arrayfun(@(x) strjoin([x,''],'\n'), errorMsg(2:2:end-1));
    errorMsg = strjoin(errorMsg,'\n');
end