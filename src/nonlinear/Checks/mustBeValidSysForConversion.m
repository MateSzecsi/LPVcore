function mustBeValidSysForConversion(sys)
    mustBeA(sys, 'LPVcore.nlss');

    if ~sys.symbolic
        eidType = 'LPVcore:systemCheck';
        msgType = ['To convert the system to an LPV representation its' ...
            ' Jacobian should be computable which requires a symbolic.' ...
            'internal representation of the system.'];
        throwAsCaller(MException(eidType, msgType));
    end
    if ~all(abs(sys.f(zeros(sys.Nx,1), zeros(sys.Nu, 1))) <= 10 * eps)
        eidType = 'LPVcore:systemCheck';
        msgType = ['To convert the system to an LPV representation it' ...
            ' should satisfy that f(0,0) = 0.'];
        throwAsCaller(MException(eidType, msgType));
    end
    if ~all(abs(sys.h(zeros(sys.Nx,1), zeros(sys.Nu, 1))) <= 10 * eps)
        eidType = 'LPVcore:systemCheck';
        msgType = ['To convert the system to an LPV representation it' ...
            ' should satisfy that h(0,0) = 0.'];
        throwAsCaller(MException(eidType, msgType));
    end
end