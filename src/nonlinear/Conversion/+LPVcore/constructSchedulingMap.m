function eta = constructSchedulingMap(symMap, sys, Np)
    % Convert in1 and in2 to x and u
    funConvert = @(fun) replace(func2str(matlabFunction(fun, ...
        "Vars", {[sys.xSym;sys.xSym(1)], [sys.uSym;sys.uSym(1)]})),...
        {'in1','in2', '@(in1,in2)'},{'x','u',''});
    eta = cellfun(funConvert, symMap, 'UniformOutput',false);

    % Add array valued option for numerical integrals
    eta = cellfun(@funInt, eta, 'UniformOutput', false);

    % Make vector
    strMap = cell(2*Np-1 + 2,1);
    strMap(1) = {'['};
    strMap(2:2:end-1) = eta;
    strMap(3:2:end-1) = {';'};
    strMap(end) = {']'};

    % Make function
    eta = LPVcore.schedmap(str2func(['@(x,u)',strMap{:}]), Np);
end

function fun = funInt(fun)
    if contains(fun, 'integral')
        fun = [fun(1:end-1),',ArrayValued=true)'];
    end
end