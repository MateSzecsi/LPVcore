function lpvsys = difform2lpvgridss(dfsys, grid)
%DIFFORM2LPVGRIDSS Convert a differential form of a nonlinear system to
%grid-based LPV representation.
%
%   Syntax:
%       lpvsys = difform2lpvss(dfsys, grid) converts difform object dfsys
%       to a grid-based LPV state-space model at the grid points in grid.
%
%   Inputs:
%       obj (difform): Nonlinear state-space representation to be converted.
%       grid (struct): Each field contains the grid points of the state
%           and/or input on which the differential form depends. For
%           example: struct('x1', -2:0.1:2, 'u', -3:0.5:3). Also see
%           <a href="matlab:helpPopup LPVcore.difform.simplify">simplify</a> to simplify the differential form dependency.
%
%   Outputs:
%       lpvsys: LPVGRIDSS object.
%
%
%   Example: 
%       >> sys = LPVcore.nlss(@(x,u) [x(2)^2 + u; x(1)], ...
%                             @(x,u) sin(x(1)), 2, 1, 1);
%       >> dfsys = LPVcore.difform(sys);
%       >> dependency(dfsys);
%         
%       State-space matrix dependency of differential form:
%       
%           A: 	( x2 )
%           B: 	None
%           C: 	( x1 )
%           D: 	None
%
%       >> dfsys = simplify(dfsys);     % To simplify the matrix functions
%                                       % to only depend on (x1, x2)
%
%       >> grid = struct('x1', -1:.1:1, 'x2', -1:.1:1);
%       >> lpvsys = LPVcore.difform2lpvgridss(dfsys, grid)
% 
%       lpvsys = 
%       
%       Continuous-time gridded LPV-SS model
%       
%           (d/dt)x = A(p) x + B(p) u
%                 y = C(p) x + D(p) u
%       
%       with
%       
%           Nr. of inputs:             1
%           Nr. of scheduling signals: 2
%           Nr. of outputs:            1
%           Nr. of states:             2
%       
%       Sampling grid:
%       
% 	      Nr. of samples:			   441
% 	              'x1': 21 grid points
% 	              'x2': 21 grid points
%
%See also LPVGRIDSS, LPVCORE.DIFFORM.
%

    arguments
        dfsys {mustBeA(dfsys,'LPVcore.difform')}
        grid {mustBeA(grid,'struct')}
    end

    % Checks
    if strcmp(dfsys.isSimplified, 'full')
        warning(['The differential form is fully simplified. ',...
            'Therefore each state-space matrix function ',...
            'might have different input arguments. '...
            'To prevent this, simplify the differential form, '...
            'using the partial option.'])
    end
    
    % Convert grid points
    [pointsCell, gridLength] = LPVcore.retrieveGridPoints(grid, ...
                                                      dfsys.Nx, dfsys.Nu);

    % Evaluate matrices at grid-points
    Av = LPVcore.gridFunEval(dfsys.A, pointsCell, gridLength);
    Bv = LPVcore.gridFunEval(dfsys.B, pointsCell, gridLength);
    Cv = LPVcore.gridFunEval(dfsys.C, pointsCell, gridLength);
    Dv = LPVcore.gridFunEval(dfsys.D, pointsCell, gridLength);

    sys = ss(Av, Bv, Cv, Dv, dfsys.Ts);

    % Reshape to get rid of single grid points
    index = gridLength ~= 1;

    sys = reshape(sys, [gridLength(index), 1]);
    
    gridNames = fieldnames(grid);
    gridCell = struct2cell(grid);

    grid = cell2struct(gridCell(index), gridNames(index));

    % Create system
    lpvsys = lpvgridss(sys, grid);
end
