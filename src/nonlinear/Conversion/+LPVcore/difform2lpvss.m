function [lpvsys, eta] = difform2lpvss(dfsys, factorOpt)
%DIFFORM2LPVSS Convert a differential form of a nonlinear system to an LPV
%representation.
%
%   Syntax:
%       [lpvsys, eta] = difform2lpvss(dfsys) converts difform object dfsys
%       to LPV state-space model with scheduling map eta.
%
%       [lpvsys, eta] = difform2lpvss(sys, factorOpt) converts difform 
%       object dfsys to LPV state-space model, where factorOpt can either 
%       'element'  (default) or 'factor' to specify if each nonlinear 
%       element of the matrix is taken as scheduling variable ('element') 
%       or an algorithm tries to split each element into a sum of 
%       scheduling variables ('factor'). While 'factor' can result in a 
%       simpler scheduling map and simpler affine state-space 
%       representation, it can result in more scheduling variables than 
%       'element'.
%
%   Inputs:
%       obj (difform): Differential form to be converted.
%       factorOpt (string): Matrix function factorization method, 
%           'element', or 'factor'. Default value: 'element'.
%
%   Outputs:
%       lpvsys: LPVCORE.LPVSS object.
%
%
%See also LPVCORE.LPVSS, LPVCORE.DIFFORM.
%

    arguments
        dfsys {mustBeA(dfsys,'LPVcore.difform')}
        factorOpt {mustBeMember(factorOpt, {'element','factor'})} = 'element';
    end
    M = [dfsys.Asym,dfsys.Bsym;dfsys.Csym,dfsys.Dsym];
    
    if isct(dfsys)
        timeStr = 'ct';
    else
        timeStr = 'dt';
    end
    
    [M0, Mi, funSym, Np] = LPVcore.makeAffineSym(M, factorOpt);
    eta = LPVcore.constructSchedulingMap(funSym, dfsys, Np);
    Mp = pmatrix(cat(3, Mi, M0), 'SchedulingDimension', Np, ...
        'SchedulingDomain', timeStr);
    
    Ap = Mp(1:dfsys.Nx,1:dfsys.Nx);
    Bp = Mp(1:dfsys.Nx,dfsys.Nx+1:end);
    Cp = Mp(dfsys.Nx+1:end,1:dfsys.Nx);
    Dp = Mp(dfsys.Nx+1:end,dfsys.Nx+1:end);
    
    lpvsys = LPVcore.lpvss(Ap, Bp, Cp, Dp, dfsys.Ts);
end