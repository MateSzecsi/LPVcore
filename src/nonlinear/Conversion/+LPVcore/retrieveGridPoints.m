function [pointsCell, gridLength] = retrieveGridPoints(grid, Nx, Nu)
    grid = orderfields(grid);

    names = fieldnames(grid);
    xIndex = contains(fieldnames(grid),'x');
    uIndex = contains(fieldnames(grid),'u');

    NxArg = sum(xIndex);
    NuArg = sum(uIndex);

    if NxArg + NuArg ~= length(names)
        eidType = 'LPVcore:grid';
        msgType = 'Unrecognized fields in grid structure';
        throwAsCaller(MException(eidType, msgType));
    end
    if NxArg ~= Nx
        eidType = 'LPVcore:grid';
        msgType = ['State dimension of grid is not equal to the state ' ...
            'dimension of the system'];
        throwAsCaller(MException(eidType, msgType))
    end
    if NuArg ~= Nu
        eidType = 'LPVcore:grid';
        msgType = ['Input dimension of grid is not equal to the input ' ...
            'dimension of the system'];
        throwAsCaller(MException(eidType, msgType))
    end

    % Retrieve grid points
    xGrid = cell(NxArg, 1);   xIt = 1;
    uGrid = cell(NuArg, 1);   uIt = 1;
    gridLengthX = cell(NxArg, 1);
    gridLengthU = cell(NuArg, 1);
    for i = 1:length(names)
        if xIndex(i)
            xGrid{xIt} = grid.(names{i});
            gridLengthX{xIt} = length(xGrid{xIt});
            xIt = xIt + 1;
        else
            uGrid{uIt} = grid.(names{i});
            gridLengthU{uIt} = length(uGrid{uIt});
            uIt = uIt + 1;
        end
    end
    gridLength = [gridLengthX{:},gridLengthU{:}];

    gridPoints = combineGrid(xGrid{:}, uGrid{:});
    nPoints = size(gridPoints, 2);
    xGridPoints = gridPoints(1:Nx, :); 
    if isempty(xGridPoints)
        xGridPointsCell = []; 
    else
        xGridPointsCell = mat2cell(xGridPoints, NxArg, ones(1,nPoints));
    end
    uGridPoints = gridPoints(Nx+1:Nx+Nu, :);
    if isempty(uGridPoints)
        uGridPointsCell = []; 
    else
        uGridPointsCell = mat2cell(uGridPoints, NuArg, ones(1,nPoints));
    end
    pointsCell = {xGridPointsCell, uGridPointsCell};
    pointsCell = pointsCell(~cellfun('isempty', pointsCell));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                         LOCAL FUNCTIONS                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Alternative (and faster) implementation of combvec for vectors
function out = combineGrid(varargin)
    n = nargin;
    l = cellfun(@length, varargin);
    N = prod(l);
    var = cellfun(@(x) x(:)', varargin, 'UniformOutput', false);
    out = zeros(n,N);
    out(1,:) = repmat(var{1}, 1, N/l(1));
    if n > 2
        for i = 2:n-1
            out(i,:) = repmat(reshape(repmat(var{i}, prod(l(1:i-1)), 1), ...
                1, []), 1, prod(l(i+1:end)));
        end
    end
    out(n, :) = reshape(repmat(var{n},prod(l(1:n-1)),1),1,[]);
end