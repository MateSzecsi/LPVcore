function lpvsys = nlss2lpvgridss(sys, grid)
%NLSS2LPVGRIDSS Convert a nonlinear state-space representation to a
%grid-based LPV representation using numerical integration of the 
%Jacobians.
%
%   Syntax:
%       lpvsys = nlss2lpvgridss(sys, grid) converts NLSS object to an
%       grid-based LPV state-space model.
%
%   Inputs:
%       obj (nlss): Nonlinear state-space representation to be converted.
%       grid (struct): Each field contains the grid points of the state
%           and/or input. For example: 
%           struct('x1', -2:0.1:2, 'u', -3:0.5:3). 
%
%   Outputs:
%       lpvsys: LPVGRIDSS object.
%
%   Example: 
%       >> sys = LPVcore.nlss(@(x,u) [x(2)^2 + u; x(1)], ...
%                             @(x,u) sin(x(1)), 2, 1, 1);
%
%
%       >> grid = struct('x1', -1:.1:1, 'x2', -1:.1:1, 'u', 0);
%
%       >> lpvsys = LPVcore.nlss2lpvgridss(sys, grid)
% 
%       lpvsys = 
%       
%       Continuous-time gridded LPV-SS model
%       
%           (d/dt)x = A(p) x + B(p) u
%                 y = C(p) x + D(p) u
%       
%       with
%       
%           Nr. of inputs:             1
%           Nr. of scheduling signals: 2
%           Nr. of outputs:            1
%           Nr. of states:             2
%       
%       Sampling grid:
%       
% 	      Nr. of samples:			   441
% 	              'x1': 21 grid points
% 	              'x2': 21 grid points
%
%See also LPVCORE.NLSS, LPVGRIDSS.
%

    arguments
        sys {mustBeValidSysForConversion}
        grid {mustBeA(grid,'struct')}
    end
    
    dfsys = LPVcore.difform(sys);

    % Retrieve grid points
    [pointsCell, gridLength] = LPVcore.retrieveGridPoints(grid, ...
                                                      sys.Nx, sys.Nu);

    % Evaluation functions
    Af = constructFun(dfsys.A);
    Bf = constructFun(dfsys.B);
    Cf = constructFun(dfsys.C);
    Df = constructFun(dfsys.D);

    % Evaluate matrices at grid-points
    Av = LPVcore.gridFunEval(Af, pointsCell, gridLength);
    Bv = LPVcore.gridFunEval(Bf, pointsCell, gridLength);
    Cv = LPVcore.gridFunEval(Cf, pointsCell, gridLength);
    Dv = LPVcore.gridFunEval(Df, pointsCell, gridLength);

    sys = ss(Av, Bv, Cv, Dv, dfsys.Ts);

    % Reshape to get rid of single grid points
    index = gridLength ~= 1;

    sys = reshape(sys, [gridLength(index), 1]);
    
    gridNames = fieldnames(grid);
    gridCell = struct2cell(grid);

    grid = cell2struct(gridCell(index), gridNames(index));

    % Create system
    lpvsys = lpvgridss(sys, grid);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                         LOCAL FUNCTIONS                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = constructFun(M)
    if isnumeric(M)
        out = M;
    else
        out = @(x,u) integral(@(lm) M(lm*x, lm*u), 0, 1, ...
            "ArrayValued", true);
    end
end