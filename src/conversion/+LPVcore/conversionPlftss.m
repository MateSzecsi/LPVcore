function sysout = conversionPlftss(sys)
%CONVERSIONLPVSS Converts a LPVcore.lpvss or lpvlfr object to a LPVTools 
%plftss object.
%
%   Syntax:
%       sysout = LPVcore.conversionPlftss(sys)
%
%   Inputs:
%       sys (LPVcore.lpvss/lpvlfr): LPV system to be converted.
%
%   Outputs:
%       sysout: Output LPV system as (LPVTools) plftss object.
%

    arguments
        sys {checkSys(sys)}
    end
    
    % Convert Delta block (assume affine depdendency of Delta)
    Delta = simplify(sys.Delta + 0);
    DeltaMat = Delta.matrices;

    DeltaT = DeltaMat(:,:,1);
    for i = 1:sys.Np
        pTemp = tvreal(sys.SchedulingTimeMap.Name{i}, ...
                       sys.SchedulingTimeMap.Range{i}, ...
                       sys.SchedulingTimeMap.RateBound{i});
        DeltaT = DeltaT + DeltaMat(:,:,i + 1) * pTemp;
    end

    % Create system
    sysout = lft(DeltaT, sys.G);
    sysout = copyProperties(sys, sysout);
end

function sysout = copyProperties(sys,sysout)
    propertyNames = {'StateName','StateUnit','TimeUnit','InputName',...
        'InputUnit','InputDelay','InputGroup','OutputName','OutputUnit',...
        'OutputGroup','Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        sysout.(propertyNames{i}) = sys.(propertyNames{i});
    end
end

function checkSys(sys)
    mustBeA(sys, {'LPVcore.lpvss','lpvlfr'});
    
    if any(isinf(cell2mat(sys.SchedulingTimeMap.Range)), "all")
        eidType = 'LPVcore:finiteRange';
        msgType = ['All system parameters must have finite range for ' ...
            'conversion to a plftss object to be possible.'];
        throwAsCaller(MException(eidType, msgType));
    end

    if ~isaffine(sys.Delta)
        eidType = 'LPVcore:affineDelta';
        msgType = ['The Delta block must have an affine scheduling ' ...
            'dependency for conversion to a plftss object to be possible'];
        throwAsCaller(MException(eidType, msgType));
    end
end