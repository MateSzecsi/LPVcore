function sysout = conversionLpvss(sys)
%CONVERSIONLPVSS Converts a LPVcore.lpvss or lpvlfr object to a (Control
%Systems Toolbox) lpvss object.
%
%   Syntax:
%       sysout = LPVcore.conversionLpvss(sys)
%
%   Inputs:
%       sys (LPVcore.lpvss/lpvlfr): LPV system to be converted.
%
%   Outputs:
%       sysout: Output LPV system as (Control Systems Toolbox) lpvss
%           object.
%
    arguments
        sys {mustBeA(sys,{'LPVcore.lpvss','lpvlfr'})}
    end

    ParamNames = sys.SchedulingTimeMap.Name;
    DataFcn = @(t,p) DataFunction(p,sys);

    sysout = lpvss(ParamNames, DataFcn, sys.Ts);

    sysout = copyProperties(sys, sysout);
end

function [A,B,C,D,E,d0,x0,u0,y0,Delays] = DataFunction(p,sys)
    sysout = lft(freeze(sys.Delta, reshape(p, 1, [])), sys.G);

    A = sysout.A;
    B = sysout.B;
    C = sysout.C;
    D = sysout.D;

    E      = [];
    d0     = [];
    x0     = [];
    u0     = [];
    y0     = [];
    Delays = [];
end

function sysout = copyProperties(sys,sysout)
    propertyNames = {'StateName','StateUnit','TimeUnit','InputName',...
        'InputUnit','InputGroup','OutputName','OutputUnit',...
        'OutputGroup','Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        sysout.(propertyNames{i}) = sys.(propertyNames{i});
    end
end