function sysout = conversionPss(sys)
%CONVERSIONPSS Converts a lpvgridss object to a LPVTools pss object.
%
%   Syntax:
%       sysout = LPVcore.conversionPss(sys)
%
%   Inputs:
%       sys (lpvgridss): LPV system to be converted.
%
%   Outputs:
%       sysout: Output LPV system as (LPVTools) pss object.
%

    arguments
        sys {mustBeA(sys, 'lpvgridss')}
    end

    %% Conversion
    % Create the RGRID object (LPVTools)
    IVName = fieldnames(sys.SamplingGrid);
    IVData = struct2cell(sys.SamplingGrid);
    Domain =  rgrid(IVName, IVData);
    
    % Assemble LPVTools representation
    sysout = pss(sys.ModelArray, Domain);
    sysout = copyProperties(sys, sysout);
end

function sysout = copyProperties(sys,sysout)
    propertyNames = {'StateUnit','InputName',...
        'InputUnit','OutputName','OutputUnit',...
        'Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        sysout.(propertyNames{i}) = sys.(propertyNames{i});
    end
end