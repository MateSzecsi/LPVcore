function sysout = plftssConversion(sys)
%PLFTSSCONVERSION Converts a LPVTools plftss object to a LPVcore lpvlfr 
%object.
%
%   Syntax:
%       sysout = LPVcore.plftssConversion(sys)
%
%   Inputs:
%       sys (plftss): LPV system to be converted.
%
%   Outputs:
%       sysout: Output LPV system as (LPVcore) lpvlfr object.
%

    arguments
        sys {mustBeA(sys, 'plftss')}
    end
    
    % Get LFR data
    [G,Delta] = lftdata(sys.Data);

    if G.Ts == 0
        domain = 'ct';
    else
        domain = 'dt';
    end

    % Convert Delta block
    parNames = fieldnames(sys.Parameter);
    nP = length(parNames);
    
    pVal = @(val) reshape([parNames';num2cell(val')],1,[]);

    p0 = pVal(zeros(nP,1));
    Delta0 = usubs(Delta,p0{:});


    DeltaConv = Delta0;
    
    I = eye(nP);
    
    for i = 1:nP
        pTemp = pVal(I(:,i));
        pConvTemp = preal(parNames{i}, domain, ...
            'Range', sys.Parameter.(parNames{i}).Range, ...
            'RateBound', sys.Parameter.(parNames{i}).RateBounds);
    
        DeltaConv = (usubs(Delta,pTemp{:}) - Delta0) * pConvTemp + DeltaConv;
    end
    
    DeltaConv = simplify(DeltaConv);
    
    % Create system
    sysout = lpvlfr(DeltaConv, G);
    sysout = copyProperties(sys, sysout);
end

function sysout = copyProperties(sys,sysout)
    propertyNames = {'StateName','StateUnit','TimeUnit','InputName',...
        'InputUnit','InputDelay','InputGroup','OutputName','OutputUnit',...
        'OutputGroup','Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        sysout.(propertyNames{i}) = sys.(propertyNames{i});
    end
end