function sysout = pssConversion(sys)
%PSSCONVERSION Converts a lpvgridss object to a LPVTools pss object.
%
%   Syntax:
%       sysout = LPVcore.pssConversion(sys)
%
%   Inputs:
%       sys (pss): LPV system to be converted.
%
%   Outputs:
%       sysout: Output LPV system as (LPVcore) lpvgridss object.
%

    arguments
        sys {mustBeA(sys, 'pss')}
    end

    %% Conversion
    % Create the RGRID object (LPVTools)
    grid = sys.Parameter;
    names = fieldnames(grid);
    for i = 1:length(names)
        grid.(names{i}) = reshape(grid.(names{i}).Grid,1,[]);
    end
    
    % Assemble LPVTools representation
    sysout = lpvgridss(sys.Data, grid);
    sysout = copyProperties(sys, sysout);
end

function sysout = copyProperties(sys,sysout)
    propertyNames = {'StateUnit','InputName',...
        'InputUnit','OutputName','OutputUnit',...
        'Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        sysout.(propertyNames{i}) = sys.(propertyNames{i});
    end
end