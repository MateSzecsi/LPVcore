function [K,Xcl] = transformationConstructController(mapping,genplant,synopts)
%TRANSFORMATIONCONSTRUCTCONTROLLER Performs nonlinear transformation in
%order to construct the controller.
%
%   Syntax:
%       [K,Xcl] = transformationConstructControllerCT(mapping,genplant,...
%                                                     synopts)
%
%   Inputs:
%       mapping (struct): Structure containing the variables that resulted
%           from the nonlinear transformation. See <a href=
%           "matlab:help transformationApply">transformationApply</a>.
%       genplant (struct): Generalized plant structure. See 
%           <a href="matlab:help lpvsynGeneralizedPlantStructure"
%           >lpvsynGeneralizedPlantStructure</a>.
%
%   Outputs:
%       K: Constructed controller as LPVcore.lpvss or lpvgridss object.
%       Xcl: Matrix of closed-loop Lyapunov function, i.e. the Lypunov
%           function is of the form V(x) = x'*Xcl*x. Either constant or
%           parameter dependent.
%
%   See also LPVSYN, LPVSYNSF.
%

%% Variables
sysType = synopts.systemType;
synProblem = genplant.Problem;

plant = genplant.Plant;
Ap = plant.A;

if synProblem == "OF"
    A = genplant.A;
    Bu = genplant.Bu;
    Cy = genplant.Cy;
    nx = genplant.nx;
    ny = genplant.ny;
    nu = genplant.nu;
    Ts = genplant.Ts;
end

isCT = genplant.isCT;
affine = genplant.affine;
grid = genplant.grid;

%% Construct controller matrices
if isCT
    %% Continuous-time
    switch synProblem
        case "OF"        
            % Bring to one matrix to only call rolmipToPmatrix once
            Omega = value([mapping.K,mapping.L;mapping.M,mapping.N]);
        
            % Construct closed-loop storage function
            % LMI Notes eq. (4.2.15) for construction Xcl with
            % Pick U, and V such that I-XY = UV'
            % resulting in:
            % Xcl = [X, U;U',-V\Y*U];
            % Either X and U need to be constant or Y and V, in order to
            % avoid the controller depending on the derivative of the 
            % scheduling-variable.
            X = value(mapping.X);  
            Y = value(mapping.Y);
            
            if isa(X, 'double')
                U = X; V = inv(X) - Y;
            elseif isa(Y, 'double')
                U = inv(Y) - X; V = Y;
            end
            Xcl = [X, U; U', -V\Y*U];
            
            % Construct controller matrices 
            XAY = [X;zeros(nu,nx)]*A*[Y,zeros(nx,ny)];
            LeftMat = [inv(U),-U\X*Bu;zeros(nu,nx),eye(nu)];    % == inv([U, X*Bu;zeros(nu,nx), eye(nu)])
            RightMat = [inv(V'),zeros(nx,ny);-Cy*Y/V',eye(ny)]; % == inv([V', zeros(nx,ny);Cy*Y, eye(ny)])
            ABCDk = LeftMat*(Omega-XAY)*RightMat;
        case "SF"
            W = value(mapping.W);
            F = value(mapping.F);
        
            Xcl = inv(W);
            K = F*Xcl; %#ok<MINV>
    end
else
    %% Discrete-time
    switch synProblem
        case "OF"
            Xt = value([mapping.Xx,mapping.Xy;mapping.Xy',mapping.Xz]);
            Gt = value([mapping.J, eye(nx);mapping.S, mapping.N]);
            Omega = value([mapping.U,mapping.V;mapping.W,mapping.X]);
            
            J = Gt(1:nx,1:nx);
            S = Gt(nx+1:end,1:nx);
            N = Gt(nx+1:end,nx+1:end);
        
            % Compute R & L
            [u,s,v] = svd(S-N*J);
            R = u*sqrt(s);
            L = sqrt(s)*v';
        
            % Construct closed-loop storage function
            Tinv = [eye(nx),-N'/R';zeros(nx),inv(R')]; % == inv([eye(nx),N';zeros(nx),R'])
            Xcl = Tinv'*Xt*Tinv;
            
            % Construct controller matrices
            NAJ = [N;zeros(nu,nx)]*A*[J,zeros(nx,ny)];
            LeftMat = [inv(R),-R\N*Bu;zeros(nu,nx),eye(nu)]; % == inv([R,N*Bu;zeros(nu,nx),eye(nu)])
            RightMat = [inv(L),zeros(nx,ny);-Cy*J/L,eye(ny)]; % == inv([L,zeros(nx,ny);Cy*J,eye(ny)])
            ABCDk = LeftMat*(Omega-NAJ)*RightMat;
        case "SF"
            G = value(mapping.G);
            W = value(mapping.W);
            F = value(mapping.F);
        
            if isa(W,'double') || isa(W,'gridvar')
                Xcl = inv(W);
            else
                Xcl = [];   % TODO 17/05/2023: Returning Xcl = inv(W) when, W varies
            end
            K = F*inv(G); %#ok<MINV>
    end
end

% For affine, convert rolmipvar to pmatrix as indexing does not properly
% work for rolmipvars;
switch sysType
    case 'affine'
        st = affine.st;
        switch synProblem
            case "OF"
                ABCDk = rolmipToPmatrix(ABCDk,Ap,st);
            case "SF"
                K = rolmipToPmatrix(K,Ap,st);
        end
        Xcl = rolmipToPmatrix(Xcl,Ap,st);
    case 'grid'
        ind = repmat({':'}, 1, numel(grid.dim));
        if synProblem == "SF"
            K = getMatrices(K);
            K = K(:,:, ind{:}, 1);
        end
        Xcl = getMatrices(value(Xcl));
        Xcl = Xcl(:, :, ind{:}, 1);
end

%% Construct output-feedback controller 
if synProblem == "OF"
    Ak = ABCDk(1:nx,1:nx);
    Bk = ABCDk(1:nx,end-ny+1:end);
    Ck = ABCDk(end-nu+1:end,1:nx);
    Dk = ABCDk(end-nu+1:end,end-ny+1:end);
    
    switch sysType
        case 'affine'
            K = LPVcore.lpvss(Ak,Bk,Ck,Dk,Ts);
        case 'grid'
            samplingGrid = plant.SamplingGrid;
    
            Ak = getMatrices(Ak);
            Bk = getMatrices(Bk);
            Ck = getMatrices(Ck);
            Dk = getMatrices(Dk);

            Ak = Ak(:, :, ind{:}, 1);
            Bk = Bk(:, :, ind{:}, 1);
            Ck = Ck(:, :, ind{:}, 1);
            Dk = Dk(:, :, ind{:}, 1);
    
            sys = ss(Ak,Bk,Ck,Dk,Ts);
            
            K = lpvgridss(sys,samplingGrid);
    end
end
end