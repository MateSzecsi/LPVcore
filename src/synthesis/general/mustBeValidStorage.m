function mustBeValidStorage(value)
% Check to see if template storage function is compatible for lpvsyn,
% lpvsynsf, and lpvnorm
    if ~isempty(value)
        mustBeA(value,{'pmatrix', 'logical', 'double'});
    end

    if isa(value, 'pmatrix')
        % Check if it is polynomial
        if ~ispoly(value) && isct(value)
            eidType = 'LPVcore:storagefunction';
            msgType = ['In continuous-time, the template storage function ' ...
                'can only have polynomial dependency.'];
            throwAsCaller(MException(eidType, msgType));
        end

        % Check finite rate-bounds
        if any(isinf(cell2mat(value.timemap.RateBound)), 'all')
            eidType = 'LPVcore:storagefunction';
            msgType = ['The scheduling-variables of the template ' ...
                'storage function should have finite rate-bounds'];
            throwAsCaller(MException(eidType, msgType));
        end
    end
end