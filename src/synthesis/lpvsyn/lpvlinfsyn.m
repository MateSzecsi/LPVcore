function [K,gam,Xcl] = lpvlinfsyn(P,ny,nu,synopts)
%LPVLINFSYN Linf-gain suboptimal output-feedback state-space LPV controller
%synthesis. 
%
%   Syntax:
%       [K, gam, Xcl] = lpvlinfsyn(P, ny, nu)
%
%       [K, gam, Xcl] = lpvlinfsyn(P, ny, nu, synopts)
%
%   Inputs:
%        P: LPV system as LPVcore.lpvss or lpvgridss object. In case of a
%           LPVcore.lpvss object, the scheduling dependency should be
%           affine.
%       ny (int): The number of measured variables, i.e. the last ny 
%           outputs of the plant P used as inputs to the controller K.
%       nu (int): The number of controlled variables, i.e. the last nu
%           inputs of the plant P that are connected to the output of the
%           controller K.
%       synopts (lpvsynOptions): Stucture containing LPV synthesis options,
%           see <a href="matlab:helpPopup lpvsynOptions">lpvsynOptions</a>.
%
%	Outputs:
%       K: Suboptimal output-feedback state-space LPV controller as 
%           LPVcore.lpvss or lpvgridss object.
%       gam: Upperbound for the gain of the closed-loop system.
%       Xcl: Matrix of closed-loop storage/Lyapunov function, i.e. the
%           Lypunov function is of the form V(x) = x'*Xcl*x. Either 
%           constant or parameter dependent.
%
%   See also LPVSYN.
%

arguments
    P             {mustBeCompatibleLPVsys}
    ny      (1,1) {mustBeNumeric, mustBePositive}
    nu      (1,1) {mustBeNumeric, mustBePositive}
    synopts (1,1) lpvsynOptions = lpvsynOptions
end

synopts.performance = 'linf';

[K,gam,Xcl] = lpvsyn(P,ny,nu,synopts);