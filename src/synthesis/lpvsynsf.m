function [K, gam, Xcl] = lpvsynsf(P, nu, synopts)
%LPVSYNSF Suboptimal state-feedback LPV controller synthesis. 
%
%   Syntax:
%       [K, gam, Xcl] = lpvsynsf(P, nu)
%
%       [K, gam, Xcl] = lpvsynsf(P, nu, synopts)
%
%   Inputs:
%       P: LPV system as LPVcore.lpvss or lpvgridss object. In case of a
%           LPVcore.lpvss object, the scheduling dependency should be
%           affine.
%       nu (int): The number of controlled variables, i.e. the last nu
%           inputs of the plant P that are connected to the output of the
%           state-feedback controller K.
%       synopts (lpvsynsfOptions): Stucture containing LPV state-feedback
%           synthesis options. See <a href="matlab:helpPopup 
%           lpvsynsfOptions">lpvsynsfOptions</a>.
%
%	Outputs:
%       K: State-feedback controller matrix as pmatrix object.
%       gam: Upperbound for the gain of the closed-loop system.
%       Xcl: Matrix of closed-loop storage/Lyapunov function, i.e. the
%           Lypunov function is of the form V(x) = x'*Xcl*x. Either 
%           constant or parameter dependent.
%

%% Input parser and checks
arguments
    P             {mustBeCompatibleLPVsys}
    nu      (1,1) {mustBeNumeric, mustBePositive}
    synopts (1,1) lpvsynsfOptions = lpvsynsfOptions
end

% Set system type
if isa(P,'lpvgridss')
    sysType = 'grid';
else
    sysType = 'affine';
end
synopts.SystemType = sysType;

%% Variables
perfType = synopts.performance;
backoff = synopts.backoff;
solOpt = synopts.solverOptions;
parVarStorage = synopts.parameterVaryingStorage;
imprCond = synopts.improveConditioning;
verbose = synopts.verbose;

nw = P.Nu-nu;
nz = P.Ny;

if any(parVarStorage == 1) && (P.Ts == 0) && strcmp(sysType, 'affine')
    warning(['Parameter-varying storage not yet supported in ' ...
        'continuous-time for ''LPVcore.lpvss'' models. ' ...
        'Switching to parameter independent ' ...
        'storage function.']);
    parVarStorage = 0;
    synopts.parameterVaryingStorage = parVarStorage;
end

%% Performance specific checks
switch perfType
    case 'h2'
        assert(verifyDclZero(P,nw),['D-matrix of the closed-loop is' ...
            ' not zero! Generalized H2 synthesis is not possible.']);
    case 'passive'
        assert(nw == nz,['Passivity based synthesis not possible, ' ...
            'closed-loop number of inputs and outputs are not the same']);
end

%% Obtain structure with all variables of generalized plant
if verbose; disp('Converting system to correct format...'); end
[genplant,synopts] = lpvsynGeneralizedPlantStructure(P,[],nu,synopts);

%% Synthesis
% Apply NL transformation
if verbose; disp('Applying NL transformation...'); end
[matrices, mapping] = transformationApply(genplant,synopts);

% Construct the synthesis LMIs
if verbose; disp('Setting up transformed synthesis LMI...'); end
[gam,LMI,var] = transformationConstructSynthesisLmi(matrices,genplant, ...
    synopts);

% Add pole constraints
LMI = lpvsynPoleConstraint(LMI,matrices.Xt,matrices.At,genplant, ...
    'standard',synopts);

% Solve synthesis LMIs
if verbose; disp('Solving transformed synthesis LMI...'); end
[diagnostics,LMI,gamValue] = lpvsynSolveLmi(LMI,gam,var,synopts);

[LMI, resolved] = lpvsynCheckLmiProblems(diagnostics,LMI,gam,gamValue, ...
    synopts,0);
gam = gamValue*backoff;

if verbose
    if ~isempty(gam)
        fprintf(['Transformed synthesis LMI solved ' ...
            'with gamma = %.4g\n'],gam); 
    else
        disp('Transformed synthesis LMI solved'); 
    end
end

% Numerical conditioning
if imprCond
    % Better conditioning
    [alph,LMI] = lpvsynConditioningLmi(LMI,mapping,genplant,...
        synopts,[],nu,'transformation-sf-1');
    
    if verbose; disp('Improving conditioning of solution...'); end
    diagnostics = optimize(LMI,alph,solOpt);

    lpvsynCheckLmiProblems(diagnostics,LMI,alph,value(alph),synopts,0);
elseif ~isempty(gam) && ~resolved
    % Reformulate LMI with (fixed) backed-off gamma
    if verbose; disp('Solving synthesis LMI with backed-off gamma...'); end
    diagnostics = optimize(LMI,[],solOpt);
    assert(diagnostics.problem == 0,lpvsynErrorMessage(diagnostics));
end

% Construct controller
if verbose; disp('Constructing controller...'); end
[K,Xcl] = transformationConstructController(mapping,genplant,synopts);

if verbose; disp('Done'); end
end

%% Local functions
%% Verifies whether the closed loop matrix Dcl = Dzw is zero (used for H2)
function flag = verifyDclZero(P,nw) 
Dzw = P.D(:,1:nw);
if isa(Dzw,'pmatrix')
    flag = all(simplify(Dzw,1e-10) == 0,'all');
else
    flag = all(abs(Dzw) <= 1e-10,'all');
end
end