function [out, X] = lpvnorm(P, type, options)
%LPVNORM computes the Linf, L2 or H2 norm of a LPV system or allows to 
%check if the system is passive. 
%
%   Syntax:
%       [out, X] = lpvnorm(P, ...)
%       
%       [out, X] = lpvnorm(P, type, ...)
%
%   Inputs: 
%       P: LPV state-space system as LPVcore.lpvss or lpvgridss object for
%           which the norm is to be computed.
%       type (char): Type of norm to be computed, supported norms: 'linf',
%           'l2', 'h2' and 'passive'. If type is not given, 'l2' is used.
%       
%       Optional name-value pair:
%           'SolverOptions': Structure containing the YALMIP solver 
%               settings. See <a href="matlab:help sdpsettings
%               ">sdpsettings</a>. 
%               Default value: sdpsettings('verbose',0,'solver','sdpt3');
%           'NumericalAccuracy': Numerical accuracy parameter. Used for, 
%               among other things, solving the LMIs (LMI <= NumAcc).
%               Default value: 1e-10;
%           'LinfOptions': Structure containing the options for Linf 
%               analysis. See <a href="matlab:help linfOptions
%               ">linfOptions</a>.
%           'ParameterVaryingStorage': Option to indicate whether a
%               parameter-varying storage function is considered (if at
%               least one of the scheduling-variables has finite
%               rate-bounds). Use 0 to indicate to use a parameter
%               independent storage function, 1 to indicate parameter
%               dependency on all parameters with finite rate-bounds. For
%               'lpvgridss' models, to use a varying storage function,a 
%               template storage function must be given which basis 
%               functions will be used for the computed storage function, 
%               e.g.:
%                   >> p = preal('p', 'ct', 'RateBound', [-1 1]');
%                   >> stor = 1 + p + p^2;
%                   >> lpvnorm(sys, 'l2', 'ParameterVaryingStorage', stor)
%               The parameters of the template storage function should have
%               ratebounds and should have the same names as the parameters
%               of the 'lpvgridss' model. In continuous-time, the template 
%               storage can only have polynomial dependency.
%               Default value: 1 ('LPVcore.lpvss'), 0 ('lpvgridss');
%           'Verbose': Turns progress messages on (1) or off (0);
%               Default value: 1;
%               
%   Outputs:
%       out: Value of the norm in case of the 'linf', 'l2' or 'h2' norm or
%           true in the case of a passivity check and the system is 
%           passive.
%       X: Value of quadratic storage function matrix.      
%

%% Checks
arguments
    P       {mustBeCompatibleLPVsys}
    type    {mustBeMember(type, {'linf','l2','h2','passive'})} = 'l2';
    
    options.SolverOptions           (1,1) struct = sdpsettings('verbose', 0,'solver','sdpt3','cachesolvers',1)
    options.NumericalAccuracy       (1,1) {mustBeNumeric, mustBePositive} = 1e-10
    options.LinfOptions             (1,1) linfOptions = linfOptions
    options.ParameterVaryingStorage {mustBeValidStorage} = defaultStorage(P);
    options.Verbose                 (1,1) logical = 1;
end

% Set system type
if isa(P,'lpvgridss')
    sysType = 'grid';
else
    sysType = 'affine';
end
options.sysType = sysType;

% Performance specific checks
switch type
    case 'h2'
        if ~checkValidH2Norm(P.D,1e-10)
            out = Inf;
            X = NaN;
            return;
        end
    case 'passive'
        if ~checkValidPassivity(P.Nu,P.Ny)
            out = NaN;
            X = NaN;
            return;
        end
end

% Set other options
options.SolverOptions.cachesolvers = 1; % enable caching of solvers for small speed-up
options.type = lower(type);             % add performance type to opt structure
options.isCT = isct(P);

if isa(options.ParameterVaryingStorage, 'pmatrix')
    options.ParameterVaryingStorageValue = options.ParameterVaryingStorage;
    options.ParameterVaryingStorage = true;

    if strcmp(sysType, 'affine')
        warning(['Setting a template storage function is not ' ...
                 'supported for affine based analysis. A storage ' ...
                 'function with affine dependency will be used ' ...
                 'automatically.']);
    end

    if ~((isct(options.ParameterVaryingStorageValue) && options.isCT) || ...
         (isdt(options.ParameterVaryingStorageValue) && ~options.isCT))
        error(['Storage function template should be in the same ' ...
            'domain (CT or DT) as the LPV system.'])
    end
else
    options.ParameterVaryingStorageValue = [];
    options.ParameterVaryingStorage = logical(options.ParameterVaryingStorage);
    if strcmp(sysType, 'grid')
        if options.ParameterVaryingStorage
            warning(['Template storage function should be ' ...
                'provided in order to use a varying ' ...
                'storage function with a lpvgridss system. Disabling ' ...
                'varying storage.'])
        end
        options.ParameterVaryingStorage = false;
    end
end

%% Checks and variables for parameter-varying storage function (grid)
if strcmp(sysType, 'grid')
    A = gridvar(P.A, 0);
    options.gridOpt.ng = A.Ng;
    options.gridOpt.dim = A.dim;
    storageTemplate = options.ParameterVaryingStorageValue;

    if ~isempty(storageTemplate)
        samplingGrid = P.SamplingGrid;
        gridNames = fieldnames(samplingGrid);
        storNames = options.ParameterVaryingStorageValue.timemap.Name;

        for i = 1:numel(storNames)
            if ~any(strcmp(gridNames, storNames{i}))
                error(['The scheduling-variable ''', storNames{i},''' of the template' ...
                ' storage function is not a scheduling-variable of the LPV model.'])
            end
        end

        npStorage = options.ParameterVaryingStorageValue.Np;
        options.gridOpt.npStorage = npStorage;

        gridValuesUnsorted = struct2cell(samplingGrid);
        gridValuesUnsorted = combineGrid(gridValuesUnsorted{:});
        nG = size(gridValuesUnsorted, 2);
        
        pValues = zeros(options.gridOpt.npStorage, nG);
        for i = 1:numel(storNames)
            ind = strcmp(storNames{i}, gridNames);
            pValues(i,:) = gridValuesUnsorted(ind,:);
        end
        
        rateBound = storageTemplate.timemap.RateBound;
        
        pdotValues = combineGrid(rateBound{:});
        
        options.gridOpt.rhoValues = combineGrid(pValues, pdotValues);
        if ~options.isCT
            % In DT, go from rho = [p(t);v(t)] to 
            % [p(t);p(t)+v(t)] = [p(t);p(t+1)]
            options.gridOpt.rhoValues(npStorage+1:end, :) = ...
                options.gridOpt.rhoValues(1:npStorage, :) + ...
                options.gridOpt.rhoValues(npStorage+1:end, :);
        end
    else
        options.gridOpt.npStorage = 0;
    end
    options.gridOpt.repMatrices = [repmat({1}, 1, numel(size(P.A))), ...
        repmat({2}, 1, options.gridOpt.npStorage)];
end

%% Construct variables
switch sysType
    case 'affine'
        [At,stFull{1}] = pmatrixToRolmip(P.A);
        [Bt,stFull{2}] = pmatrixToRolmip(P.B);
        [Ct,stFull{3}] = pmatrixToRolmip(P.C);
        [Dt,stFull{4}] = pmatrixToRolmip(P.D);
        
        % Get st from all matrices
        stID = find(~cellfun(@isempty,stFull),1); % find first non-empty element
        if isempty(stID)
            st = 0;
        else
            st = stFull{stID};
        end
        
        matrices.At = At;
        matrices.Bt = Bt;
        matrices.Ct = Ct;
        matrices.Dt = Dt;
    case 'grid'
        matrices.At = gridvar(repmat(P.A, options.gridOpt.repMatrices{:}));
        matrices.Bt = gridvar(repmat(P.B, options.gridOpt.repMatrices{:}));
        matrices.Ct = gridvar(repmat(P.C, options.gridOpt.repMatrices{:}));     
        matrices.Dt = gridvar(repmat(P.D, options.gridOpt.repMatrices{:}));
end

%% Checks and variables for parameter-varying storage function (affine)
switch sysType
    case 'affine'
        rhoRange = reshape(cell2mat(P.SchedulingTimeMap.Range),[],2);
        rhoRateBound = reshape(cell2mat(P.SchedulingTimeMap.RateBound),[],2);
        
        % Find rho's which have finite rate bounds
        infRateBound = any(isinf(rhoRateBound),2);
        
        % If all parameter rate bounds are infinite set paramVarStorage to 0
        if sum(infRateBound) == P.Np
            options.ParameterVaryingStorage = 0;
        end
        
        % Create variables when using parameter-varying storage
        if options.ParameterVaryingStorage
            rateBoundedRhoIndex = find(~infRateBound);
            infRateBoundIndex = find(infRateBound);
        
            % Create st variable which only selects ratebounded rho's
            stDep = st([1;rateBoundedRhoIndex+1]);
            rhoRateBoundDummy = rhoRateBound;               % Dummy variable used for diff. storage
            rhoRateBoundDummy(infRateBoundIndex,1) = -1;    % function in rolmip, as it is required 
            rhoRateBoundDummy(infRateBoundIndex,2) = 1;     % to include all parameters.
        else
            stDep = [];
            rhoRateBoundDummy = [];
            rateBoundedRhoIndex = [];
        end
        
        options.stStruct.stDep = stDep;
        options.stStruct.rhoRange = rhoRange;
        options.stStruct.rhoRateBoundDummy = rhoRateBoundDummy;
        options.stStruct.rateBoundedRhoIndex = rateBoundedRhoIndex;
end

%% Solve LMIs
[gam,LMI,Xt,var] = lpvnormConstructAnalysisLmi(matrices,P,options);
[out,X] = lpvnormSolveLmi(LMI,gam,Xt,var,P,options);
end

%% ########################## LOCAL FUNCTIONS ########################## %%
%% Check if D-matrix is zero for H2-norm
function out = checkValidH2Norm(D,numAcc)
    if isa(D,'pmatrix')
        out = all(abs(freeze(D,exp(1)))<=numAcc,'all');
    else
        out = all(abs(D)<=numAcc,'all');
    end
    if ~out
        warning('D-matrix is non-zero, hence the H2 norm is infinite.');
    end
end

%% Check if for passivity number of inputs and outpus are the same
function out = checkValidPassivity(nu,ny)
    out = (nu == ny);
    if ~out
        warning('Passivity check not possible, number of inputs and outputs are not the same.');
    end
end

function out = defaultStorage(P)
    if isa(P, 'lpvgridss')
        out = false;
    else
        out = true;
    end
end