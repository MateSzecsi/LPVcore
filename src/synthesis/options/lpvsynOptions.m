classdef (CaseInsensitiveProperties) lpvsynOptions < synthesisOptions
%LPVSYNOPTIONS Create LPV synthesis options object.
%
%   Syntax:
%       out = lpvsynOptions(...)
%   
%   Optional name-value pairs:
%       'Backoff': Correction factor for gamma, used in the synthesis to 
%           reduce numerical errors, since the true optimal solution is
%           close to singular. 
%           Default value: 1.01.
%       'Dependency': Array (1x4 or 4x1) indicating which controller
%           matrices, corresponding to [A,B,C,D], should have parameter-
%           dependency. In case of the transformation method, these apply
%           to the transformed controller matrices.
%           Default value: [1 1 1 1].
%       'DirectFeedthrough': Option indicating if controller has direct
%           feedthrough (1) or not (0).
%           Default value: 1;
%       'ImproveConditioning': Solves extra optimization problems to
%           improve the conditioning of found solutions.
%           Default value: 1;
%       'LinfOptions': LinfOptions object containing the options for Linf 
%           based synthesis. See <a href="matlab:helpPopup linfOptions"
%           >linfOptions</a>.
%           Default value: linfOptions().
%       'Method': Indicates what method to use for synthesis. Possible
%           values: 'transformation', 'projection'. 
%           Default value: 'transformation'.
%       'NumericalAccuracy': Numerical accuracy parameter. Used for solving
%           the LMIs (LMI <= -NumAcc). 
%           Default value: 1e-8.
%       ParameterVaryingStorage': Option to indicate whether a 
%           parameter-varying storage function is considered (if at least 
%           one of the scheduling-variables has finite rate-bounds). Use 0
%           to indicate to use a parameter independent storage function, 1 
%           to indicate parameter dependency on all parameters with finite 
%           rate-bounds, or [] to let the synthesis algorithm decide. For
%           'lpvgridss' models, to use a varying storage function, a 
%           template storage function must be given which basis functions 
%           will be used for the computed storage function, e.g.:
%               >> p = preal('p', 'ct', 'RateBound', [-1 1]');
%               >> stor = 1 + p + p^2;
%               >> lpvnorm(sys, 'l2', 'ParameterVaryingStorage', stor)
%           The parameters of the template storage function should have
%           ratebounds and should have the same names as the parameters of 
%           the 'lpvgridss' model. In continuous-time, the template storage
%           can only have polynomial dependency.
%           Default value: [] ('LPVcore.lpvss'), 0 ('lpvgridss');
%       'ParameterVaryingStorageOption': Option to choose between two 
%           different parameter-varying storage parameterizations. Neither
%           option is in general better and it will depend on the specific
%           control problem which option results in better performance.
%           This option currently only available for continous-time 
%           synthesis. Possible values: 1, 2. 
%           Default value: 1.
%       'Performance': Perfomance metric that is considered for synthesis.
%           Possible values: 'linf', 'l2', 'h2', 'passive'. 
%           Default value: 'l2'.
%       'PoleConstraint': PoleConstraintOptions object describing the
%           constraints on the closed-loop eigenvalues, see
%           <a href="matlab:helpPopup poleConstraintOptions"
%           >poleConstraintOptions</a>.
%           Default value: poleConstraintOptions().
%       'SolverOptions': Structure containing the YALMIP solver settings. 
%           See <a href="matlab:helpPopup sdpsettings">sdpsettings</a>. 
%           Default value: sdpsettings('verbose',0,'solver','sdpt3').
%       'Verbose': Turns progress messages on (1) or off (0);
%           Default value: 1;
%       
%   Outputs:
%       out: Object containing LPV synthesis options.
%   
%   See also LPVSYN.
%
    
    % Sorted alphabetically 
    properties (Dependent)
        DirectFeedthrough
        Method
    end

    % Properties set and used in lpvsyn
    properties (Hidden)
        MethodChangedFlag = false
    end

    properties (Access = protected, Hidden)
        DirectFeedthrough_
        Method_
    end
    
    methods
        function obj = lpvsynOptions(options)
            arguments
                options.Backoff
                options.DirectFeedthrough...
                    (1,1)   logical...
                                = 1
                options.Dependency...
                            logical {dependencyCheck}...
                                = [1 1 1 1]
                options.ImproveConditioning
                options.LinfOptions
                options.Method...
                            {mustBeMember(options.Method,...
                            {'transformation','projection'})}...
                                = 'transformation'
                options.NumericalAccuracy
                options.ParameterVaryingStorage
                options.Performance
                options.PoleConstraint
                options.SolverOptions
                options.ParameterVaryingStorageOption
                options.Verbose
            end

            argName = fieldnames(options);
            argValue = struct2cell(options);
            
            % Parse all arguments to superclass except for subclass
            % specific properties
            indexLogical = contains(argName,{'DirectFeedthrough', ...
                'Method','ParameterVaryingStorageOption'});
            ind = find(~indexLogical);
            args = reshape([argName(ind)';argValue(ind)'],1,[]);

            obj = obj@synthesisOptions(args{:});

            % Parse class specific properties
            for i = find(indexLogical)'
                 obj.(argName{i}) = argValue{i};
            end
        end

        % Method
        function out = get.Method(obj)
            out = obj.Method_;
        end

        function obj = set.Method(obj,value)
            value = lower(value);
            obj.Method_ = value;
            obj.MethodChangedFlag = true;
        end

        % DirectFeedthrough
        function out = get.DirectFeedthrough(obj)
            out = obj.DirectFeedthrough_;
        end

        function obj = set.DirectFeedthrough(obj,value)
            obj.DirectFeedthrough_ = logical(value);
        end
    end

    methods (Hidden)
        % Dependency (specific set method)
        function obj = setDependency(obj, value)
            if length(value) == 1
                value = ones(1,4) * value;
            end
            obj.Dependency_ = value;
        end
    end
end

function dependencyCheck(dep)
    if (length(dep) ~= 4) && (length(dep) ~= 1)
            eidType = 'LPVcore:lpvsynOptions:dependencyCheck';
            msgType = ['The dependency should be a scalar or a 4 ' ...
                'element vector.'];
            throwAsCaller(MException(eidType, msgType));
    end
end