classdef (CaseInsensitiveProperties) synthesisOptions
%SYNTHESISOPTIONS Super class for synthesis options

%   
%   Name-value pairs:
%       'Backoff': Correction factor for gamma, used in the synthesis to 
%           reduce numerical errors, since the true optimal solution is
%           close to singular. 
%           Default value: 1.01.
%       'Dependency': To set dependency of controller.
%       'ImproveConditioning': Solves extra optimization problems to
%           improve the conditioning of found solutions.
%           Default value: 1;
%       'LinfOptions': LinfOptions object containing the options for Linf 
%           based synthesis. See <a href="matlab:helpPopup linfOptions"
%           >linfOptions</a>.
%           Default value: linfOptions().
%       'NumericalAccuracy': Numerical accuracy parameter. Used for solving
%           the LMIs (LMI <= -NumAcc). 
%           Default value: 1e-8.
%       ParameterVaryingStorage': Option to indicate whether a 
%           parameter-varying storage function is considered (if at least 
%           one of the scheduling-variables has finite rate-bounds). Use 0
%           to indicate to use a parameter independent storage function, 1 
%           to indicate parameter dependency on all parameters with finite 
%           rate-bounds, or [] to let the synthesis algorithm decide. For
%           'lpvgridss' models, to use a varying storage function, a 
%           template storage function must be given which basis functions 
%           will be used for the computed storage function, e.g.:
%               >> p = preal('p', 'ct', 'RateBound', [-1 1]');
%               >> stor = 1 + p + p^2;
%               >> lpvnorm(sys, 'l2', 'ParameterVaryingStorage', stor)
%           The parameters of the template storage function should have
%           ratebounds and should have the same names as the parameters of 
%           the 'lpvgridss' model. In continuous-time, the template storage
%           can only have polynomial dependency.
%           Default value: [] ('LPVcore.lpvss'), 0 ('lpvgridss');
%       'ParameterVaryingStorageOption': Option to choose between two 
%           different parameter-varying storage parameterizations. Neither
%           option is in general better and it will depend on the specific
%           control problem which option results in better performance.
%           This option currently only available for continous-time 
%           synthesis. Possible values: 1, 2. 
%           Default value: 1.
%       'Performance': Perfomance metric that is considered for synthesis.
%           Possible values: 'linf', 'l2', 'h2', 'passive'. 
%           Default value: 'l2'.
%       'PoleConstraint': PoleConstraintOptions object describing the
%           constraints on the closed-loop eigenvalues, see
%           <a href="matlab:helpPopup poleConstraintOptions"
%           >poleConstraintOptions</a>.
%           Default value: poleConstraintOptions().
%       'SolverOptions': Structure containing the YALMIP solver settings. 
%           See <a href="matlab:helpPopup sdpsettings">sdpsettings</a>. 
%           Default value: sdpsettings('verbose',0,'solver','sdpt3').
%       'Verbose': Turns progress messages on (1) or off (0);
%           Default value: 1;
%
    
    % Sorted alphabetically 
    properties (Dependent)
        Backoff
        Dependency
        ImproveConditioning
        LinfOptions
        NumericalAccuracy
        ParameterVaryingStorage
        ParameterVaryingStorageOption
        Performance
        PoleConstraint
        SolverOptions
        Verbose
    end

    % Properties set and used in lpvsyn
    properties (Hidden)
        SystemType {mustBeMember(SystemType, {'affine','grid',''})} = ''
    end

    properties (Hidden, SetAccess = protected)
        ParameterVaryingStorageValue = []
    end

    properties (Access = protected, Hidden)
        Performance_
        Backoff_
        SolverOptions_
        NumericalAccuracy_
        ParameterVaryingStorage_
        LinfOptions_
        Dependency_
        Verbose_
        ImproveConditioning_
        PoleConstraint_
        ParameterVaryingStorageOption_
    end
    
    methods
        function obj = synthesisOptions(options)
            arguments
                options.Backoff...
                    (1,1)   {mustBeNumeric, ...
                             mustBeGreaterThan(options.Backoff, 1)}...
                                = 1.01;
                options.Dependency
                options.ImproveConditioning...
                    (1,1)   logical...
                                = 1
                options.LinfOptions...
                    (1,1)   linfOptions...
                                = linfOptions
                options.NumericalAccuracy...
                    (1,1)   {mustBeNumeric, mustBePositive}...
                                = 1e-8
                options.ParameterVaryingStorage ...
                            {mustBeValidStorage}...
                                = []
                options.Performance...
                            {mustBeMember(options.Performance,...
                            {'linf', 'l2', 'h2', 'passive'})}...
                                = 'l2'
                options.PoleConstraint...
                    (1,1)   poleConstraintOptions...
                                = poleConstraintOptions()
                options.SolverOptions...
                    (1,1)   struct...
                                = sdpsettings('verbose',0,'solver',...
                                              'sdpt3','cachesolvers',1)
                options.ParameterVaryingStorageOption...
                    (1,1)   {mustBeMember(options.ParameterVaryingStorageOption, ...
                             [1, 2])}...
                                = 1
                options.Verbose...
                    (1,1)   logical...
                                = 1;
            end

            % Names of options
            names = fieldnames(options);
            
            for i = 1:length(names)
                obj.(names{i}) = options.(names{i});
            end
        end
        
        % Performance
        function out = get.Performance(obj)
            out = obj.Performance_;
        end

        function obj = set.Performance(obj,value)
            value = lower(value);
            obj.Performance_ = value;
        end

        % Backoff
        function out = get.Backoff(obj)
            out = obj.Backoff_;
        end

        function obj = set.Backoff(obj,value)
            obj.Backoff_ = value;
        end

        % SolverOptions
        function out = get.SolverOptions(obj)
            out = obj.SolverOptions_;
        end

        function obj = set.SolverOptions(obj,value)
            value.cachesolvers = 1;     % force cache solvers to be 1 for
                                        % small speed up
            obj.SolverOptions_ = value;
        end

        % NumericalAccuracy
        function out = get.NumericalAccuracy(obj)
            out = obj.NumericalAccuracy_;
        end

        function obj = set.NumericalAccuracy(obj,value)
            obj.NumericalAccuracy_ = value;
        end

        % ParameterVaryingStorage
        function out = get.ParameterVaryingStorage(obj)
            out = obj.ParameterVaryingStorage_;
        end

        function obj = set.ParameterVaryingStorage(obj,value)
            if isa(value, 'pmatrix')
                obj.ParameterVaryingStorageValue = value;
                obj.ParameterVaryingStorage_ = 1;
            else
                obj.ParameterVaryingStorage_ = logical(value);
            end
        end

        % LinfOptions
        function out = get.LinfOptions(obj)
            out = obj.LinfOptions_;
        end

        function obj = set.LinfOptions(obj,value)
            obj.LinfOptions_ = value;
        end

        % Dependency
        function out = get.Dependency(obj)
            out = obj.Dependency_;
        end

        function obj = set.Dependency(obj,value)
            obj = setDependency(obj, value);
        end

        % Verbose
        function out = get.Verbose(obj)
            out = obj.Verbose_;
        end

        function obj = set.Verbose(obj,value)
            obj.Verbose_ = logical(value);
        end

        % ImproveConditioning
        function out = get.ImproveConditioning(obj)
            out = obj.ImproveConditioning_;
        end

        function obj = set.ImproveConditioning(obj,value)
            obj.ImproveConditioning_ = logical(value);
        end

        % PoleConstraint
        function out = get.PoleConstraint(obj)
            out = obj.PoleConstraint_;
        end

        function obj = set.PoleConstraint(obj,value)
            obj.PoleConstraint_ = value;
        end

        % SystemType
        function out = get.SystemType(obj)
            out = obj.SystemType;
        end

        function obj = set.SystemType(obj,value)
            value = lower(value);
            obj.SystemType = value;
        end

        % Varying Storage Option
        function out = get.ParameterVaryingStorageOption(obj)
            out = obj.ParameterVaryingStorageOption_;
        end

        function obj = set.ParameterVaryingStorageOption(obj,value)
            obj.ParameterVaryingStorageOption_ = value;
        end
    end
end