classdef (CaseInsensitiveProperties) lpvsynsfOptions < synthesisOptions
%LPVSYNSFOPTIONS Create LPV state-feedback synthesis options object.
%
%   Syntax:
%       out = lpvsynsfOptions(...)
%   
%   Optional name-value pairs:
%       'Backoff': Correction factor for gamma, used in the synthesis to 
%           reduce numerical errors, since the true optimal solution is
%           close to singular. 
%           Default value: 1.01.
%       'Dependency': Scalar indicating if the state-feedback controller
%           should have parameter dependency (1), or not (0). 
%           Default value: 1.
%       'ImproveConditioning': Solves extra optimization problems to
%           improve the conditioning of found solutions.
%           Default value: 1;
%       'LinfOptions': LinfOptions object containing the options for Linf 
%           based synthesis. See <a href="matlab:helpPopup linfOptions"
%           >linfOptions</a>.
%           Default value: linfOptions().
%       'NumericalAccuracy': Numerical accuracy parameter. Used for solving
%           the LMIs (LMI <= -NumAcc). 
%           Default value: 1e-8.
%       ParameterVaryingStorage': Option to indicate whether a 
%           parameter-varying storage function is considered (if at least 
%           one of the scheduling-variables has finite rate-bounds). Use 0
%           to indicate to use a parameter independent storage function, 1 
%           to indicate parameter dependency on all parameters with finite 
%           rate-bounds, or [] to let the synthesis algorithm decide. For
%           'lpvgridss' models, to use a varying storage function, a 
%           template storage function must be given which basis functions 
%           will be used for the computed storage function, e.g.:
%               >> p = preal('p', 'ct', 'RateBound', [-1 1]');
%               >> stor = 1 + p + p^2;
%               >> lpvnorm(sys, 'l2', 'ParameterVaryingStorage', stor)
%           The parameters of the template storage function should have
%           ratebounds and should have the same names as the parameters of 
%           the 'lpvgridss' model. In continuous-time, the template storage
%           can only have polynomial dependency.
%           Default value: [] ('LPVcore.lpvss'), 0 ('lpvgridss');
%       'ParameterVaryingStorageOption': Option to choose between two 
%           different parameter-varying storage parameterizations. Neither
%           option is in general better and it will depend on the specific
%           control problem which option results in better performance.
%           This option currently only available for continous-time 
%           synthesis. Possible values: 1, 2. 
%           Default value: 1.
%       'Performance': Perfomance metric that is considered for synthesis.
%           Possible values: 'linf', 'l2', 'h2', 'passive'. 
%           Default value: 'l2'.
%       'PoleConstraint': PoleConstraintOptions object describing the
%           constraints on the closed-loop eigenvalues, see
%           <a href="matlab:helpPopup poleConstraintOptions"
%           >poleConstraintOptions</a>.
%           Default value: poleConstraintOptions().
%       'SolverOptions': Structure containing the YALMIP solver settings. 
%           See <a href="matlab:helpPopup sdpsettings">sdpsettings</a>. 
%           Default value: sdpsettings('verbose',0,'solver','sdpt3').
%       'Verbose': Turns progress messages on (1) or off (0);
%           Default value: 1;
%       
%   Outputs:
%       out: Object containing LPV state-feedback synthesis options.
%   
%   See also LPVSYNSF.
%

    methods
        function obj = lpvsynsfOptions(options)
            arguments
                options.Backoff
                options.Dependency...
                            logical {dependencyCheck}...
                                = 1
                options.ImproveConditioning
                options.LinfOptions
                options.NumericalAccuracy
                options.ParameterVaryingStorage
                options.Performance
                options.PoleConstraint
                options.SolverOptions
                options.VaryingStorageOption
                options.Verbose
            end

            argName = fieldnames(options);
            argValue = struct2cell(options);
            args = reshape([argName';argValue'],1,[]);
            obj = obj@synthesisOptions(args{:});
        end    
    end

    methods (Hidden)
        % Dependency (specific set method)
        function obj = setDependency(obj, value)
            obj.Dependency_ = value;
        end
    end
end

function dependencyCheck(dep)
    if length(dep) ~= 1
            eidType = 'LPVcore:lpvsynOptions:dependencyCheck';
            msgType = 'The dependency should be a scalar.';
            throwAsCaller(MException(eidType, msgType));
    end
end