classdef (CaseInsensitiveProperties) linfOptions
%LINFOPTIONS Create Linf options structure used for Linf based analysis and
%controller synthesis.
%
%   Syntax:
%       out = linfOptions(...)
%   
%   Optional name-value pairs:
%       'AccuracyLambda': Maximum difference between lambda search interval
%           during optimization. 
%           Default value: 1e-3.
%       'LambdaGrid': Array of values for lambda used for initial line 
%           search. 
%           Default value: linspace(1e-8, 5, 15).
%       'MaxIterations': Maximum number of iterations for optimizing 
%           lambda.
%           Defaullt value: 25;
%   
%   Outputs:
%       out: Object containing Linf options.
%
%   See also LPVSYN, LPVNORM.
%
    
    % Sorted alphabetically 
    properties (Dependent)
        AccuracyLambda
        LambdaGrid
        MaxIterations
    end

    properties (Access = protected, Hidden)
        LambdaGrid_
        AccuracyLambda_
        MaxIterations_
    end
    
    methods
        function obj = linfOptions(options)
            arguments
                options.AccuracyLambda (1,1) {mustBeNumeric, mustBePositive} = 1e-3
                options.MaxIterations  (1,1) {mustBeInteger, mustBePositive} = 25
                options.LambdaGrid           {mustBeNumeric, mustBePositive, mustBeVector} = linspace(1e-8, 5, 15)
            end
            
            names = fieldnames(options);

            for i = 1:length(names)
                obj.(names{i}) = options.(names{i});
            end
        end
        
        % LambdaGrid
        function out = get.LambdaGrid(obj)
            out = obj.LambdaGrid_;
        end

        function obj = set.LambdaGrid(obj,value)
            obj.LambdaGrid_ = value;
        end

        % AccuracyLambda
        function out = get.AccuracyLambda(obj)
            out = obj.AccuracyLambda_;
        end

        function obj = set.AccuracyLambda(obj,value)
            obj.AccuracyLambda_ = value;
        end

        % MaxIterations
        function out = get.MaxIterations(obj)
            out = obj.MaxIterations_;
        end

        function obj = set.MaxIterations(obj,value)
            obj.MaxIterations_ = value;
        end
    end
end

