classdef (InferiorClasses = {?ss, ?tf, ?zpk}) lpvss < lpvlfr
%LPVSS LPV State-Space system representation
%
%   Syntax:
%           SYS = LPVcore.lpvss(A,B,C,D) creates an object SYS representing the
%      continuous-time state-space model
%           dx/dt = A(p(t)) x(t) + B(p(t)) u(t)
%            y(t) = C(p(t)) x(t) + D(p(t)) u(t)
%      The A, B, C, D matrices can be real matrices or pmatrix
%      objects. For D = [], the matrix will be set to a zero matrix of
%      appropriate dimension.
%  
%      SYS = LPVcore.lpvss(A,B,C,D,Ts) creates a discrete-time state-space model with
%      sample time Ts (set Ts=-1 if the sample time is undetermined) of the
%      following form:
%          x(t+1) = A(p(t)) x(t) + B(p(t)) u(t)
%            y(t) = C(p(t)) x(t) + D(p(t)) u(t)
%  
%      SYS = LPVcore.lpvss(D) specifies a static gain matrix D.
%
%      SYS = LPVcore.lpvss(A,B,C,D,Ts,'Property1',Value1,...,'PropertyN',ValueN)
%      will set the property value pairs. In addition to all properties
%      supported by LPVLFR, the following properties are available:
%
%           'SVDTolerance': sets the SVDTolerance property of the LPVSS
%               object.
%   
%   See also LPVIO, LPVIDPOLY, PMATRIX, TIMEMAP.
    
    properties
        % (Advanced) Tolerance for discarding small singular values
        %
        % LPVSS models are internally represented as LPVLFR models. In the
        % conversion process, a singular value decomposition is employed.
        % Without a tolerance on the singular values, the Delta block of
        % the corresponding LPVLFR representation grows quickly. All
        % singular vectors below this tolerance are discared to remedy
        % this. The default value is sqrt(eps).
        SVDTolerance
    end

    properties (Dependent)
        A       % nx-by-nx pmatrix or empty object
        B       % nx-by-nu pmatrix or empty object
        C       % ny-by-nx pmatrix or empty object
        D       % ny-by-nu pmatrix or empty object
    end
    
    methods
        function obj = lpvss(A, B, C, D, Ts, varargin)
            if nargin == 1
                if isnumeric(A) || isa(A, 'ss') || isa(A, 'tf') || isa(A, 'zpk')
                    sys = ss(A, 'minimal');
                    A = sys.A;
                    B = sys.B;
                    C = sys.C;
                    D = sys.D;
                    Ts = sys.Ts;
                elseif isa(A, 'LPVcore.lpvss')
                    sys = A;
                    A = sys.A;
                    B = sys.B;
                    C = sys.C;
                    D = sys.D;
                    Ts = sys.Ts;
                elseif isa(A, 'lpvlfr')
                    sys = A;
                    [A, B, C, D, Ts] = lpvlfr2ss_(sys);
                end
            end

            % Ensure compatible sizes of A, B, C, D
            if size(A, 1) ~= size(A, 2)
                error('The "A" matrix must be square');
            end
            nx = size(A, 1);
            % Infer IO dimensions
            if ~isempty(D)
                nu = size(D, 2);
                ny = size(D, 1);
            else
                nu = size(B, 2);
                ny = size(C, 1);
            end
            % Expand empty matrices to appropriate IO dimensions
            if isempty(B)
                B = zeros(nx, nu);
            end
            if isempty(C)
                C = zeros(ny, nx);
            end
            if isempty(D)
                D = zeros(ny, nu);
            end
            % Check compatibility of B, C, D with IO
            if size(B, 2) ~= nu || size(D, 2) ~= nu
                error('The "B" and "D" matrices must have the same number of columns.');
            end
            if size(C, 1) ~= ny || size(D, 1) ~= ny
                error('The "C" and "D" matrices must have the same number of rows.');
            end
            % Check compatibility of B, C with state
            if size(B, 1) ~= nx
                error('The "A"and "B" matrices must have the same number of rows.');
            end
            if size(C, 2) ~= nx
                error('The "A"and "C" matrices must have the same number of columns.');
            end
            
            % Convert to PMATRIX
            if isa(A, 'double'); A = double2pmatrix(A); end
            if isa(B, 'double'); B = double2pmatrix(B); end
            if isa(C, 'double'); C = double2pmatrix(C); end
            if isempty(D)
                D = pzeros(size(C, 1), size(B, 2));
            end
            if isa(D, 'double'); D = double2pmatrix(D); end
            
            [A, B, C, D, tm] = ...
                pmatrix.commonrho_(A, B, C, D);
            if nargin >= 3 && nargin <= 4
                if any(strcmp(tm.Domain, {'undefined', 'ct'}))
                    Ts = 0;
                else
                    Ts = -1;
                end
            end
            
            % Parse PV pairs
            parser = inputParser;
            parser.KeepUnmatched = true;
            addParameter(parser, 'SVDTolerance', sqrt(eps));
            parse(parser, varargin{:});
            svdtol = parser.Results.SVDTolerance;

            [Delta, G] = lpvss2lfr(A, B, C, D, Ts, svdtol);
            obj = obj@lpvlfr(Delta, G, varargin{:});
            % If 'sys' is defined, inherit signal properties from it
            if exist('sys', 'var')
                obj = LPVcore.copySignalProperties(obj, sys, 'full');
            end
            % Add SVD tolerance
            obj.SVDTolerance = svdtol;
        end

        function val = get.A(obj)
            val = obj.A0 + obj.Bw * obj.Delta * obj.Cz;
        end
        
        function val = get.B(obj)
            val = obj.Bu + obj.Bw * obj.Delta * obj.Dzu;
        end
        
        function val = get.C(obj)
            val = obj.Cy + obj.Dyw * obj.Delta * obj.Cz;
        end
        
        function val = get.D(obj)
            val = obj.Dyu + obj.Dyw * obj.Delta * obj.Dzu;
        end
        
        function obj = set.A(obj, A)
            if ~all(size(A) == [obj.Nx, obj.Nx])
                error('A must be Nx-by-Nx');
            end
            [obj.Delta_, obj.G_] = lpvss2lfr(A, obj.B, obj.C, obj.D, obj.Ts, obj.SVDTolerance);
        end
        
        function obj = set.B(obj, B)
            if isempty(B); B = zeros(obj.Nx, obj.Nu); end
            if ~all(size(B) == [obj.Nx, obj.Nu])
                error('B must be Nx-by-Nu');
            end
            [obj.Delta_, obj.G_] = lpvss2lfr(obj.A, B, obj.C, obj.D, obj.Ts, obj.SVDTolerance);
        end
        
        function obj = set.C(obj, C)
            if isempty(C); C = zeros(obj.Ny, obj.Nx); end
            if ~all(size(C) == [obj.Ny, obj.Nx])
                error('C must be Ny-by-Nx');
            end
            [obj.Delta_, obj.G_] = lpvss2lfr(obj.A, obj.B, C, obj.D, obj.Ts, obj.SVDTolerance);
        end
        
        function obj = set.D(obj, D)
            if isempty(D); D = zeros(obj.Ny, obj.Nu); end
            if ~all(size(D) == [obj.Ny, obj.Nu])
                error('D must be Ny-by-Nu');
            end
            [obj.Delta_, obj.G_] = lpvss2lfr(obj.A, obj.B, obj.C, D, obj.Ts, obj.SVDTolerance);
        end
    end
end

function [A, B, C, D, Ts] = lpvlfr2ss_(syslfr)
%LPVLFR2SS Convert lpvlfr to lpvss
%
%   Syntax:
%       sysss = lpvlfr2ss_(syslfr)
%

if ~all(syslfr.Dzw == 0, 'all')
    error('Cannot convert lpvlfr to lpvss because Dzw is not 0.');
end

Delta = syslfr.Delta;
A0 = syslfr.A0; Bu = syslfr.Bu; Bw = syslfr.Bw;
Cz = syslfr.Cz; Dzu = syslfr.Dzu;
Cy = syslfr.Cy; Dyw = syslfr.Dyw; Dyu = syslfr.Dyu;

A = A0 + Bw * Delta * Cz;
B = Bu + Bw * Delta * Dzu;
C = Cy + Dyw * Delta * Cz;
D = Dyu + Dyw * Delta * Dzu;
Ts = syslfr.Ts;

end
