function disp(obj)
% DISP Provide info on the lpvgridfrd object
%
%   Syntax:
%       disp(sys)
%
%   Inputs:
%       sys: lpvgridfrd object
%
narginchk(1, 1);

if isempty(obj)
    fprintf('Empty gridded LPV-FRD model');
    return;
end

% Model type
if isct(obj)
    timeStr = 'Continuous-time';
else
    timeStr = 'Discrete-time';
end
% Gridded Y/N
if isa(obj, 'lpvgridfrd')
    gridStr = 'gridded';
else
    gridStr = '';
end
fprintf('%s %s LPV-FRD model\n\n', timeStr, gridStr);

% Description of input-scheduling-output
fprintf('with\n\n');
fprintf('    Nr. of inputs:             %d\n', obj.Nu);
fprintf('    Nr. of scheduling signals: %d\n', obj.Np);
fprintf('    Nr. of outputs:            %d\n', obj.Ny);
fprintf('    Nr. of frequency points:   %d\n', obj.Nf);
fprintf('\n');

% Description of sampling grid (for lpvgridss)
if isa(obj, 'lpvgridfrd')
    fprintf('%s\n', gridString(obj));
end

% Sample time
if ~isct(obj)
    if obj.Ts == -1
        TsStr = 'unspecified';
    else
        TsStr = sprintf('%g %s',obj.Ts,obj.TimeUnit);
    end
    fprintf('%s: %s.\n', 'Sample time', TsStr);
end

end

%% Local functions
function s = gridString(obj)
    % String representation of SamplingGrid
    grid = obj.SamplingGrid;
    f = fieldnames(grid);
    N = 1;
    for i=1:numel(f)
        N = N * numel(grid.(f{i}));
    end
    s = sprintf(['Sampling grid:\n\n', ...
        '\tNr. of samples:\t\t\t   %i', ...
        '\n'], N);
    for i=1:numel(f)
        s = [s, sprintf('\t\t''%s'': %i grid points\n', f{i}, numel(grid.(f{i})))]; %#ok<AGROW> 
    end
end
