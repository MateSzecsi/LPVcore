function disp(obj)
% DISP Provide info on the lpvgridss object
%
%   Syntax:
%       disp(sys)
%
%   Inputs:
%       sys: lpvgridss object
%
narginchk(1, 1);

if isempty(obj)
    fprintf('Empty gridded LPV-SS model');
    return;
end

if isct(obj)
    Qstr  = '(d/dt)';
    emptyQstr = '      ';
else
    Qstr  = '(q^-1)';
    emptyQstr = '      ';
end

% Model type
if isct(obj)
    timeStr = 'Continuous-time';
else
    timeStr = 'Discrete-time';
end
% Gridded Y/N
if isa(obj, 'lpvgridss')
    gridStr = 'gridded';
else
    gridStr = '';
end
fprintf('%s %s LPV-SS model\n\n', timeStr, gridStr);

% Determine whether to display B and D
if obj.Nu > 0
    depB = determineDependence(obj, 'B');
    if depB == 0
        inputStrB = '';
    elseif depB == 1
        inputStrB = '+ B u';
    elseif depB == 2
        inputStrB = '+ B(p) u';
    end
    depD = determineDependence(obj, 'D');
    if depD == 0
        inputStrD = '';
    elseif depD == 1
        inputStrD = '+ D u';
    elseif depD == 2
        inputStrD = '+ D(p) u';
    end
else
    inputStrB = '';
    inputStrD = '';
end
% State equation
depA = determineDependence(obj, 'A');
if depA > 1
    fprintf('    %sx = %s x %s\n', ...
        Qstr, 'A(p)', inputStrB);
else
    fprintf('    %sx = %s x %s\n', ...
        Qstr, 'A', inputStrB);
end
% Output equation
depC = determineDependence(obj, 'C');
if depC > 1
    fprintf('    %sy = %s x %s\n\n', ...
        emptyQstr, 'C(p)', inputStrD);
else
    fprintf('    %sy = %s x %s\n\n', ...
        emptyQstr, 'C', inputStrD);
end

% Description of input-scheduling-output
fprintf('with\n\n');
fprintf('    Nr. of inputs:             %d\n', obj.Nu);
fprintf('    Nr. of scheduling signals: %d\n', obj.Np);
fprintf('    Nr. of outputs:            %d\n', obj.Ny);
nx = obj.Nx;
if numel(nx) == 1
    fprintf('    Nr. of states:             %d\n', nx);
else
    fprintf('    Nr. of states:             between %d and %d\n', nx(1), nx(2));
end
fprintf('\n');

% Description of sampling grid (for lpvgridss)
if isa(obj, 'lpvgridss')
    fprintf('%s\n', gridString(obj));
end

% Sample time
if ~isct(obj)
    if obj.Ts == -1
        TsStr = 'unspecified';
    else
        TsStr = sprintf('%g %s',obj.Ts,obj.TimeUnit);
    end
    fprintf('%s: %s.\n', 'Sample time', TsStr);
end

end

%% Local functions
function s = gridString(obj)
    % String representation of SamplingGrid
    grid = obj.SamplingGrid;
    f = fieldnames(grid);
    N = 1;
    for i=1:numel(f)
        N = N * numel(grid.(f{i}));
    end
    s = sprintf(['Sampling grid:\n\n', ...
        '\tNr. of samples:\t\t\t   %i', ...
        '\n'], N);
    for i=1:numel(f)
        g = grid.(f{i});
        if numel(g) > 1
            s = [s, sprintf('\t\t''%s'': %i grid points between %d and %d\n', f{i}, ...
                numel(g), min(g), max(g))]; %#ok<AGROW> 
        else
            s = [s, sprintf('\t\t''%s'': 1 grid point at %d\n', f{i}, g)]; %#ok<AGROW>
        end
    end
end

function dep = determineDependence(sys, propname)
    % Determine the type of scheduling dependence of a given property of sys.
    % The dependence type is returned as one of the following:
    %   dep = 0: all coefficients have the same size and are 0
    %   dep = 1: all coefficients have the same size and are equal
    %   dep = 2: coefficients do not have the same or are inequal

    % If the requested property is NOT D (but A, B, C, E), then we need to
    % first check if the state dimension is consistent across all grid
    % points. Otherwise, calling sys.(propname) directly gives an error.
    if ~strcmp(propname, 'D')
        nx = sys.Nx;
        % If the state dimension is inconsistent, we know that the
        % coefficient has a dependence on the scheduling signal.
        if numel(nx) > 1
            dep = 2;
            return;
        end
    end
    arr = sys.(propname);
    if norm(arr, 'fro') == 0
        dep = 0;
    else
        if norm(diff(arr, 1, 3), 'fro') == 0
            dep = 1;
        else
            dep = 2;
        end
    end
end
