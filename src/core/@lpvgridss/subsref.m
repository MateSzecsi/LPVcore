function varargout = subsref(obj, S)
%SUBSREF Subscripted referencing for lpvgridss objects.
%
%   F = P(i:j, k:l)
%
%   Inputs:
%       P: indexed lpvgridss object.
%       S: indexing structure.
%
%   Outputs:
%       F: result of indexing expression.
%
%   This method overloads the builtin SUBSREF functionality for the case of
%   2-dimensional parentheses indexing. In this case, it indexes the
%   input and output channels of the lpvgridss object.
%
%   See also LPVGRIDSS.

% Only subsref of the following form are overloaded: P(x, y)
% This implies that S.type == '()' with 2 subscripts S.subs.
if numel(S) == 1 && strcmp(S.type, '()') && numel(S.subs) >= 2
    ysubs = S.subs{1};
    usubs = S.subs{2};
    ModelArray = subsref(obj.ModelArray, S);
    sz = size(ModelArray);
    if prod(sz(3:end)) >= 2
        % Each dimension after the 3rd of S.subs corresponds to a dimension
        % on the sampling grid.
        samplingGrid_ = obj.SamplingGrid;
        if numel(S.subs) > 2
            % Check that the number of dimensions of S.subs matches
            % SamplingGrid
            fields = fieldnames(obj.SamplingGrid);
            assert(numel(S.subs) - 2 == numel(fields), ...
                ['Linear indexing for LPVGRIDSS objects ', ...
                'with a multi-dimensional grid is not supported.']);
            for i=1:numel(fields)
                field = fields{i};
                grid = obj.SamplingGrid.(field);
                % Note the +2 because first 2 dimension correspond to IO
                % channels
                samplingGrid_.(field) = grid(S.subs{i+2});
            end
        end
        sysout = lpvgridss(ModelArray, samplingGrid_);
    else
        sysout = ModelArray;
    end
    sysout.InputName = obj.InputName(usubs);
    sysout.InputUnit = obj.InputUnit(usubs);
    sysout.InputDelay = obj.InputDelay(usubs);
    sysout.OutputName = obj.OutputName(ysubs);
    sysout.OutputUnit = obj.OutputUnit(ysubs);
    sysout.OutputDelay = obj.OutputDelay(ysubs);
    varargout{1} = sysout;
else
    [varargout{1:nargout}] = builtin('subsref', obj, S);
end