function sysd = c2d(sysc, Ts, method, opts)
%C2D Convert model from continuous to discrete time
%
% Syntax:
%   sysd = c2d(sysc, Ts)
%   sysd = c2d(sysc, Ts, method)
%   sysd = c2d(sysc, Ts, method, opts)
%
% Inputs:
%   sysc: continuous-time LPVLFR object
%   Ts: sampling time in TimeUnit (the sysc.TimeUnit property)
%   method: character vector denoting the conversion method to employ.
%       Options:
%           - 'exact-zoh-euler': the Rectangular (Euler's Forward) Method
%             in the exact zero-order hold (ZOH) setting. See eq. (23) and beyond of 
%             [1] for details.
%           - 'exact-zoh-poly': the Polynomial (Hanselmann) Method
%             in the exact zer-order hold (ZOH) setting. See eq. (27) and
%             beyond of [1] for details. For this method, an additional
%             integer must be specified in the options argument that
%             provides the order of the approximation. Note that this method
%             is a special case of 'exact-zoh-euler' with n = 1.
%           - 'exact-zoh-tustin': the Tusin Method in the exact zero-order hold (ZOH)
%               setting. See eq. (38) of [1] for details.
%
% References:
%   [1] "On the Discretization of Linear Fractional Representations of LPV
%   Systems", R. Toth et. al. (IEEE Transactions on Control Systems
%   Technology), 2012.
%   

%% Input validation
narginchk(2, 4);
if nargin <= 2
    method = 'exact-zoh-euler';
end
assert(any(strcmp(method, {'exact-zoh-euler', 'exact-zoh-poly', 'exact-zoh-tustin'})), ...
    'Discretization method ''%s'' is not supported.', method);
assert(isreal(Ts) && isscalar(Ts) && Ts > 0 && ~isinf(Ts), ...
    'Sampling time ''Ts'' must be a positive scalar');
assert(hasStaticSchedulingDependence(sysc), ...
    '''sysc'' must have static scheduling dependence');
assert(isct(sysc), '''sysc'' must be a continuous-time model');
% Validate options
if strcmp(method, 'exact-zoh-poly')
    if nargin < 4
        opts = 1;  % order of approximation ("n" in (27) in [1])
    end
    assert(isint(opts) && opts >= 1, 'Approximation order must be positive integer');
else
    if nargin >= 4
        error('Options argument is not supported for method ''%s''', method);
    end
end


%% Pre-processing
% Convert Delta to discrete-time
Delta = c2d(sysc.Delta);
% Extract G
G = sysc.G;
% exact-zoh-euler is special case of exact-zoh-poly with n = 1
if strcmp(method, 'exact-zoh-euler')
    method = 'exact-zoh-poly';
    opts = 1;
end

%% Exact ZOH - Polynomial (Hanselmann) Method
% See [1] equation (27) and beyond
if strcmp(method, 'exact-zoh-poly')
    nx = sysc.Nx;
    ny = sysc.Ny;
    nw = sysc.Nw;
    nz = sysc.Nz;

% Approximation order ("n" in (27) in [1])
n = opts;

% Construct matrices (28) in [1]

% Pre-compute powers of G.A as they are frequently needed
Apow = zeros(nx, nx, n+1);
Apow(:,:,1) = eye(nx);
for i=2:n+1
    Apow(:,:,i) = Apow(:,:,i-1) * G.A;
end

% Construct "A" matrix (top-left block of (28))
T = permute(arrayfun(@(l) Ts^l / factorial(l), 0:n),[1 3 2]);
Ad = sum(pagemtimes(T, Apow), 3);

% Construct "Bw" = "B1" matrix (top-middle block of (28))
ABw = pagemtimes(Apow(:,:,1:end-1), sysc.Bw);
Bdw = cell(1, n);
for i = 1:n
    Bdw{i} = sum(pagemtimes(T(:,:,(i+1):end), ABw(:, :, 1:(end-i+1))), 3);
end
Bdw = cell2mat(Bdw);

% Construct "Bu" = "B1" matrix (top-right block of (28))
ABu = pagemtimes(Apow(:,:,1:end-1), sysc.Bu);
Bdu = sum(pagemtimes(T(:,:,2:end), ABu), 3);
Bd = [Bdw, Bdu];

% Construct "Cz" = "C1" matrix (left-middle block of (28))
CzA = pagemtimes(sysc.Cz, Apow(:,:,1:end-1));
Cdz = reshape(permute(CzA, [1 3 2]), [nz * n, nx]);

% Construct "Cy" = "C2" matrix (left-bottom block of (28))
Cdy = sysc.Cy;
Cd = [Cdz; Cdy];

% Construct "Dzw" = "D11" matrix (middle block of (28))
Ddzw = cell(1, n);
Ddzw{1} = [sysc.Dzw;Cdz(1:end-nz,:)*sysc.Bw];
for i = 2:n
    Ddzw{i} = [zeros(nz * (i-1), nw); Ddzw{1}(1:(end-((i-1)*nz)),:)];
end
Ddzw = cell2mat(Ddzw);

% Construct "Dzu" = "D12" matrix (right-middle block of (28))
Ddzu = [sysc.Dzu;Cdz(1:end-nz,:)*sysc.Bu];

% Construct "Dyw" = "D21" matrix (bottom-middle block of (28))
Ddyw = [sysc.Dyw, zeros(ny, (n-1) * nw)];

% Construct "Dyu" = "D22" matrix (bottom-right block of (28))
Ddyu = sysc.Dyu;

Dd = [Ddzw, Ddzu; Ddyw, Ddyu];

Gd = ss(Ad, Bd, Cd, Dd);
Gd.Ts = Ts;

% Construct Delta (see just below (27))
Deltad = kron(eye(n), Delta);

%% Exact ZOH - Tustin Method
elseif strcmp(method, 'exact-zoh-tustin')

Td = Ts;  % Consistency with paper notation

A = G.A;
B1 = G.B(:, 1:sysc.Nw);
B2 = G.B(:, sysc.Nw+1:end);
C1 = G.C(1:sysc.Nz, :);
C2 = G.C(sysc.Nz+1:end, :);
D11 = G.D(1:sysc.Nz, 1:sysc.Nw);
D12 = G.D(1:sysc.Nz, sysc.Nw+1:end);
D21 = G.D(sysc.Nz+1:end, 1:sysc.Nw);
D22 = G.D(sysc.Nz+1:end, sysc.Nw+1:end);

I = eye(sysc.Nx);
Psi_i = I - (Td / 2) * A;
Ad = (I + (Td / 2) * A) / Psi_i;
B1d = sqrt(Td) * (Psi_i \ B1);
B2d = sqrt(Td) * (Psi_i \ B2);
C1d = sqrt(Td) * (C1 / Psi_i);
C2d = sqrt(Td) * (C2 / Psi_i);
D11d = (Td / 2) * (C1 / Psi_i) * B1 + D11;
D12d = (Td / 2) * (C1 / Psi_i) * B2 + D12;
D21d = (Td / 2) * (C2 / Psi_i) * B1 + D21;
D22d = (Td / 2) * (C2 / Psi_i) * B2 + D22;

Bd = [B1d, B2d];
Cd = [C1d; C2d];
Dd = [D11d, D12d; D21d, D22d];

Gd = ss(Ad, Bd, Cd, Dd);
Gd.Ts = Ts;

% Construct Delta (unchanged)
Deltad = Delta;

end

%% Post-processing
% Assemble DT model
sysd = lpvlfr(Deltad, Gd);
% Copy signal properties (excluding Ts) from sysc to sysd
sysd = LPVcore.copySignalProperties(sysd, sysc);

% Return (if possible) LPVcore.lpvss object if input system is 
% LPVcore.lpvss object
if isa(sysc, 'LPVcore.lpvss')
    try
        sysd = LPVcore.lpvss(sysd);
    catch
        warning(['Discretized system cannot be represented as ' ...
            '"LPVcore.lpvss", returning "lpvlfr".'])
    end
end

end

