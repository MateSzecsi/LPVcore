function x0 = findstates(sys, data, ~)
%FINDSTATES Estimate initial states of model
%
% Syntax:
%   For LPVLFR / LPVCORE.LPVSS / LPVIDSS (state-space models):
%       x0 = findstates(sys, data)
%   returns 'x0': a 1-by-Nx double vector with the initial state.
%   
%   For LPVIO (input-output models):
%       y0 = findstates(sys, data)
%   returns 'y0': a Na-by-Ny double matrix with the output values at the Na
%       initial time steps.
%
% Inputs:
%   sys: LPV model
%   data: LPVIDDATA object
%

%% Input validation
% TODO: add support for other model structures to expand scope of
% findstates to lpvidss and lpvidpoly.
assert(isa(sys, 'lpvlfr') || isa(sys, 'lpvio') || isa(sys, 'lpvidss'), ...
    '''findstates'' is only supported for ''lpvlfr'', ''lpvidss'' and ''lpvio'' objects');
assert(isdt(sys), '''sys'' must be discrete-time');
assert(isa(data, 'lpviddata'), '''data'' must be an ''lpviddata'' object');

%% Differentiation between model structures
% This section contains code that is specific to each supported model
% structure
if isa(sys, 'lpvlfr') || isa(sys, 'lpvidss')
    nx = sys.Nx;
elseif isa(sys, 'lpvio')
    nx = sys.Na * sys.Ny;
end

%% Estimation
% Number of time samples
N = size(data.y, 1);

% y_measured = y_free + y_forced
%     |           |         |
%  ------    ----------   ----------------------
%  data.y    Gamma * x0   Simulation with data.u

% y_measured
y_measured = reshape(data.y.', [], 1);
% y_free
Gamma = NaN(sys.Ny * N, nx);
for j=1:nx
    if isa(sys, 'lpvlfr')
        x0j = zeros(1, nx); x0j(j) = 1;
        y = lsim(sys, data.p, zeros(size(data.u)), data.t, 'x0', x0j);
    elseif isa(sys, 'lpvio')
        y0j = zeros(sys.Na, sys.Ny); y0j(j) = 1;
        y = lsim(sys, data.p, zeros(size(data.u)), data.t, 'y0', y0j);
    elseif isa(sys, 'lpvidss')
        x0j = zeros(1, nx); x0j(j) = 1;
        % Take 4th output of LSIM (deterministic output without noise)
        [~, ~, ~, y] = lsim(sys, data.p, zeros(size(data.u)), data.t, x0j);
    end
    Gamma(:, j) = reshape(y.', sys.Ny * N, 1);
end
% y_forced
if isa(sys, 'lpvlfr') || isa(sys, 'lpvio')
    y_forced = lsim(sys, data.p, data.u, data.t);
elseif isa(sys, 'lpvidss')
    [~, ~, ~, y_forced] = lsim(sys, data.p, data.u, data.t);
end
y_forced = reshape(y_forced.', [], 1);

% y_measured = Gamma * x0 + y_forced
% (y_measured - y_forced) = Gamma * x0
x0 = Gamma \ (y_measured - y_forced);

%% Post-process results
if isa(sys, 'lpvio')
    x0 = reshape(x0, sys.Na, sys.Ny);
end

end

