function varargout = subsasgn(A, S, B)
%SUBSASGN Subscripted assignment for pmatrix
%
%   Syntax:
%       A = subsasgn(A, S, B)
% 
%   See also: SUBSREF, PMATRIX
%

% subsasgn is implemented as an addition. Consider this assignment:
%   B(x, y) = C
% This can be written as a summation:
%   B = CLifted + Mask .* B
% Where Mask(x, y) = 0, 1 everywhere else.
% Followed by simplify to group identical basis functions:
%   B = simplify(B, 'poly')

if numel(S) == 1 && strcmp(S.type, '()') && numel(S.subs) <= 2
    % Size of target pmatrix segment
    [sz1, sz2] = size(A);
    % Linear index (compute padding)
    if numel(S.subs) == 1
        if islogical(S.subs{1})
            % convert logical to linear index
            index = find(S.subs{1});
            S.subs{1} = index;
        end
        if S.subs{1} == ':'
            row = 1:sz1;
            col = 1:sz2;

            S.subs = {row, col};
            padding1 = 0;
            padding2 = 0;
        else
            [row, col] = ind2sub([sz1, sz2], S.subs{1});
            padding1 = max(row) - sz1;
            padding2 = max(col) - sz2;
        end
    elseif S.subs{1} == ':'
        S.subs{1} = 1:sz1;
        padding1 = 0;
        padding2 = max(S.subs{2})-sz2;
    elseif S.subs{2} == ':'
        S.subs{2} = 1:sz2;
        padding1 = max(S.subs{1})-sz1;
        padding2 = 0;
    else
        padding1 = max(S.subs{1})-sz1;
        padding2 = max(S.subs{2})-sz2;
    end 
    % Add padding
    A = [A, zeros(sz1, padding2); zeros(padding1, sz2), zeros(padding1, padding2)];
    % Compute mask
    Mask = builtin('subsasgn', ones(size(A)), S, 0);

    % Lift B into the size of the target pmatrix segment
    if isa(B, 'pmatrix')
        [A, B] = commonbfuncs_(A, B);

        BLifted = B;
        mat = B.matrices;
        o = size(B, 3);
        matLifted = zeros(size(A.matrices));
        for i=1:o
            matLifted(:, :, i) = builtin('subsasgn', zeros(sz1, sz2), S, mat(:, :, i));
        end
        BLifted.matrices = matLifted;
    else
        % B is already a matrix, simply lift into target size
        BLifted = builtin('subsasgn', zeros(sz1, sz2), S, B);
    end  
    varargout{1} = simplify(Mask .* A + BLifted);
else
    [varargout{1:nargout}] = builtin('subsasgn', A, S, B);
end

end


