function [sysr, mapping_fnc, info] = lpvdnnred(sys, nrho, data, nnopt)
%LPVDNNRED DNN-based reduction of scheduling signal dimension for LPVLFR
%models based on: "Scheduling Dimension Reduction of LPV Models - A Deep 
%Neural Network Approach" by P.J.W. Koelewijn and R. Toth (ACC, 2020).
%
%   Syntax:
%       sysr = lpvdnnred(sys, nrho, data)
%
%       sysr = lpvdnnred(sys, nrho, data, ...)
%
%       [sysr, mapping_fnc, info] = __
%
%   Inputs:
%       sys: Original model (data) as one of the following:
%           1) LPVLFR object
%           2) State-space array, where the third dimension represent the
%               model dynamics at the scheduling points in 'data'
%       nrho (int): Dimension of reduced scheduling signal.
%       data: Scheduling data specified as one of the following:
%           1) LPVIDDATA object with scheduling-output-input data. Only the
%               scheduling data is used.
%           2) An N-by-np numeric matrix with N the number of time samples
%               and np the number of scheduling signals.
%
%   Optional name-value pairs:
%       'hiddenLayerSize': Number and size of the hidden layers of the
%           neural network as vector. For example, 
%           hiddenLayerSize = [5 10], specifies a neural network with 2 
%           hidden layers, where the first layers has a width of 5 and the
%           second a with of 10. 
%           Default value: [nrho^2].
%       'activationFunction': The type of activation function used in the
%           neural network specified as character array. See 
%           'help nntransfer' for a list of available functions.
%           Default value: 'tansig'.
%       'trainingFunction': Type of training algorithm used by MATLAB to
%            train the neural network specified as character array. See 
%           'help nntrain' for a list of available functions.
%           Default value: 'trainlm'.
%       'trainingOptions': Structure of options corresponding to the
%           options of the used training functions. For example, for
%           'trainlm', 'help trainlm' for the available options. Example
%           use: trainingOptions = struct('epochs', 1e3). An empty
%           structure corresponds to using the default options.
%           Default value: struct();
%       'useGPU': Option to specify if GPU should be used for training of
%           the neural network ('yes') or not ('no'). The options
%           corresponds to the 'useGPU' option of the 'network/train'
%           command, see also 'doc network/train' for more details.
%           Default value: 'no'.
%       'useParallel': Option to specify if parrallel computation should be
%           used for training of the neural network ('yes') or not ('no'). 
%           The options corresponds to the 'useParallel' option of the 
%           'network/train' command, see also 'doc network/train' for more
%           details.
%           Default value: 'yes' (if parallel toolbox is installed).
%       'l2Regularization': Option to set the L2-regularization weight for
%           training of the neural network.
%           Default value: 1e-3. 
%
%   Outputs:
%       sysr: Model with reduced scheduling signal dimension as 
%           LPVCORE.LPVSS object.
%       mapping_fnc: function handle that maps a scheduling signal
%           trajectory from the original model 'sys' to the reduced-
%           complexity model 'sysr'. Example usage:
%           >> [sysr, mapping_fnc] = lpvdnnred(sys, 1, data)
%           >> alpha = randn(N, 10)         % Original scheduling signal
%           >> rho = mapping_fnc(alpha)     % Reduced scheduling signal
%           >> lsim(sys, alpha, ...)        % Simulate original model
%           >> lsim(sysr, rho, ...)         % Simulate reduced model
%       info: struct containing the following fields:
%           'eta': Accuracy index. A lower value indicates a better match
%               between the data matrix of the original model and reduced
%               model.
%

% TODO: add support for non-static dependence
arguments
    sys   {mustBeValidSys}
    nrho  (1,1) {mustBePositive, mustBeInteger}
    data  {mustBeValidData}
    nnopt.hiddenLayerSize {mustBeValidHiddenLayer} = [];
    nnopt.activationFunction = 'tansig';
    nnopt.trainingFunction = 'trainlm';
    nnopt.trainingOptions = struct();
    nnopt.useGPU = 'no';
    nnopt.useParallel = parallelCheck;
    nnopt.l2Regularization (1,1) {mustBeNonnegative} = 1e-3;
end

%% Create input data
if isa(data, 'lpviddata')
    pDat = data.p;
else
    pDat = data;
end
np = size(pDat, 2);

% Normalize scheduling data
[pDat_bar, pNormalize] = normalizeData(pDat');

% Remove constant rows
[inputData, removeConstantSchedSettings] = removeconstantrows(pDat_bar);

% Create matrix such that inputData = Ip * pDat_bar;
nIn = size(inputData,1);

Ip = eye(np);
Ip(removeConstantSchedSettings.remove, :) = [];

%% Create output data
if isa(sys, 'lpvlfr')
    % Compute matrix data along trajectory
    [Pi_alpha, L0] = create_Pi(pDat, sys);

    nx = sys.Nx;
    nu = sys.Nu;
    ny = sys.Ny;

    nPi = (nx + nu) * (nx + ny);
else
    % State-space array is provided
    sys = sys(:,:,:);
    [ny, nu, N] = size(sys);
    if size(pDat, 1) ~= N
        error(['Number of scheduling points and (local) ' ...
            'state-space models is not equal.'])
    end
    nx = size(sys.A, 1);

    Pi_alpha = reshape([sys.A,sys.B;sys.C,sys.D],[], N);
    
    nPi = (nx + nu) * (nx + ny);

    L0 = zeros(nPi, 1);
end

% Normalize data
[Pi_alpha_bar, ~, Ncal_inv, normalMat, normalBias] = normalizeData(Pi_alpha);

% Remove constant rows from data
[outputData, removeConstantPiSettings] = removeconstantrows(Pi_alpha_bar);

% Create matrix such that Pi_alpha_bar = Imat * outputData;
nOut = size(outputData, 1);

Imat = zeros(nPi, nOut);
Imat(sub2ind([nPi,nOut],removeConstantPiSettings.keep,1:nOut)) = 1;
Imat0 = zeros(nPi,1); 
Imat0(removeConstantPiSettings.remove) = removeConstantPiSettings.constants;

%% Create ANN
% MATLAB also offers the feedforwardnet command to create a similar
% network. However, this internally also does things such as normalization.
% In order to be in control of these things and know what is going on, we
% create the network architecture manually.
if isempty(nnopt.hiddenLayerSize)
    layerSizes = [nIn^2, nrho];
else
    layerSizes = [nnopt.hiddenLayerSize, nrho];
end
hiddenSize = numel(layerSizes);
trainFun = nnopt.trainingFunction;
actFun = nnopt.activationFunction;

% Specify network
dnn = network(1, hiddenSize + 1, ones(hiddenSize + 1, 1), ...
    [1;zeros(hiddenSize,1)], ...
    [zeros(1, hiddenSize + 1);eye(hiddenSize),zeros(hiddenSize,1)], ...
    [zeros(1, hiddenSize), 1]);
for i = 1:hiddenSize
    dnn.layers{i}.size = layerSizes(i);
    dnn.layers{i}.transferFcn = actFun;
end

% Configure io sizes
dnn = configure(dnn, inputData, outputData);

% Set options
dnn.trainFcn = trainFun;
dnn.adaptFcn = 'adaptwb';
dnn.divideFcn = 'dividerand';
dnn.plotFcns = [{'plotperform'},{'plottrainstate'},...
                {'ploterrhist'},{'plotregression'}];
dnn.performParam.regularization = nnopt.l2Regularization;

names = fieldnames(nnopt.trainingOptions);
if ~isempty(names)
    for i = 1:numel(names)
        dnn.trainParam.(names{i}) = nnopt.trainingOptions.(names{i});
    end
end

% Weight + bias initialization
for i = 1:hiddenSize+1
    dnn.layers{i}.initFcn = 'initnw';
end
dnn.initFcn = 'initlay';
dnn = init(dnn);

%% Train network
dnnTrained = train(dnn, inputData, outputData, ...
    useParallel = nnopt.useParallel, useGPU = nnopt.useGPU);

%% Construct reduced LPV system
WLast = dnnTrained.LW{hiddenSize + 1, hiddenSize};
bLast = dnnTrained.b{hiddenSize + 1};

Lvh0 = normalMat .* (Imat * bLast + Imat0) + normalBias + L0(:);
Lvhi = normalMat .* (Imat * WLast);

Lh0 = reshape(Lvh0, [nx + ny, nx + nu]);
Lhi = reshape(Lvhi, [nx + ny, nx + nu, nrho]);

if sys.Ts == 0
    timeStr = 'ct';
else
    timeStr = 'dt';
end
Lmat = pmatrix(cat(3, Lhi, Lh0), 'SchedulingDimension', nrho, ...
    'SchedulingDomain', timeStr);

A = Lmat(1:nx, 1:nx);
B = Lmat(1:nx, nx+1:end);
C = Lmat(nx+1:end, 1:nx);
D = Lmat(nx+1:end, nx+1:end);

sysr = LPVcore.lpvss(A, B, C, D, sys.Ts);
if isa(sys, 'lpvlfr')
    sysr = LPVcore.copySignalProperties(sysr, sys, 'full');
end

%% Construct mapping
% Mapping is constructed by creating an empty ANN and copying over the
% trained weights and biases
mapping = network(1, hiddenSize, ones(hiddenSize, 1), ...
    [1;zeros(hiddenSize-1,1)], ...
    [zeros(1, hiddenSize);eye(hiddenSize-1),zeros(hiddenSize-1,1)], ...
    [zeros(1, hiddenSize-1), 1]);
for i = 1:hiddenSize
    mapping.layers{i}.size = layerSizes(i);
    mapping.layers{i}.transferFcn = actFun;
end
mapping = configure(mapping, inputData(:,1:2), randn(nrho, 2));

mapping.IW = dnnTrained.IW(1:end-1);
mapping.LW = dnnTrained.LW(1:end-1,1:end-1);
mapping.b = dnnTrained.b(1:end-1);

mapping_fnc = @(p) mapping(Ip * pNormalize(p'))';

%% Compute (weighted) Frobenius norm error
Pi_rho = Ncal_inv(Imat * dnnTrained(inputData));
W = diag(1 ./ normalMat);

eta = norm(W * (Pi_alpha - Pi_rho), 'fro');
info = struct('eta', eta);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%
%                            LOCAL FUNCTIONS                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vectorized matrix data function
function [Pi, L0] = create_Pi(p, sys)
    N = size(p, 1);
    nz = size(sys.Delta, 2);

    % Compute part that does not vary with the scheduling-variable
    sys0 = extractLocal(sys, zeros(1,size(p,2)));
    L0 = [sys0.A, sys0.B; sys0.C, sys0.D];

    % The calculation for LPVSS and LPVLFR differs to avoid loss of
    % precision if LPVSS state-space coefficient values are calculated from
    % an LPVLFR representation (involves SVD with possible truncations)
    if isa(sys, 'LPVcore.lpvss')
        A = sys.A; B = sys.B;
        C = sys.C; D = sys.D;

        L = [A, B; C, D];

        L_ = feval(L, p) - L0;
        Pi = reshape(L_, [], N);
    else
        A0 = sys.A0; Bw  = sys.Bw;  Bu  = sys.Bu;
        Cz = sys.Cz; Dzw = sys.Dzw; Dzu = sys.Dzu;
        Cy = sys.Cy; Dyw = sys.Dyw; Dyu = sys.Dyu;

        Delta_ = feval(sys.Delta, p);

        IDzwDelta = eye(nz) - pagemtimes(Dzw, Delta_);
        BwDelta   = pagemtimes(Bw,Delta_);
        DywDelta  = pagemtimes(Dyw, Delta_);

        BwDelta_IDzwDelta  = pagemrdivide(BwDelta,  IDzwDelta);
        DywDelta_IDzwDelta = pagemrdivide(DywDelta, IDzwDelta);

        A_ = A0  + pagemtimes(BwDelta_IDzwDelta,  Cz);
        B_ = Bu  + pagemtimes(BwDelta_IDzwDelta,  Dzu);
        C_ = Cy  + pagemtimes(DywDelta_IDzwDelta, Cz);
        D_ = Dyu + pagemtimes(DywDelta_IDzwDelta, Dzu);

        L_ = [A_, B_; C_, D_] - L0;
        Pi = reshape(L_, [], N);
    end
end

% Data normalization function
function [dataBar, Ncal, Ncal_inv, N, n] = normalizeData(data)
    data_mean = mean(data, 2);
    dataDiff = max(data,[], 2) - min(data,[],2);
    dataDiff(dataDiff == 0) = 1;
    
    Ncal = @(x) (x - data_mean) ./ dataDiff;
    Ncal_inv = @(x) dataDiff .* x + data_mean;
    
    N = dataDiff;
    n = data_mean;
    
    dataBar = Ncal(data);
end

% Check input lpv system function
function mustBeValidSys(P)
    mustBeA(P, {'lpvlfr', 'ss'});
    
    if isa(P, 'lpvlfr')
        if ~hasStaticSchedulingDependence(P)
            eidType = 'LPVcore:staticDependencyCheck';
            msgType = ['The system has dynamic scheduling dependency, ' ...
                'which is currently not supported.'];
            throwAsCaller(MException(eidType, msgType));
        end
    end
end

% Check input data function
function mustBeValidData(data)
    mustBeA(data, {'lpviddata', 'double'});

    if isa(data,'double')
        mustBeNumeric(data);
        if ~ismatrix(data)
            eidType = 'LPVcore:datatype';
            msgType = ['''data'' must be an ''lpviddata''' ...
                ' object or a numeric matrix'];
            throwAsCaller(MException(eidType, msgType));
        end
        mustBeFinite(data);
        mustBeNonNan(data);
    end
end

% Check hiddenLayerSize function
function mustBeValidHiddenLayer(vec)
    mustBeInteger(vec);

    if ~isscalar(vec) && ~isvector(vec) && ~isempty(vec)
        eidType = 'LPVcore:datatype';
        msgType = ['''hiddenLayerSize'' must be a vector or scalar' ...
            ' of integers'];
        throwAsCaller(MException(eidType, msgType));
    end
end

% Check for parallel toolbox
function out = parallelCheck
    out = 'no';
    if contains(struct2array(ver), 'Parallel Computing Toolbox')
        out = 'yes';
    end
end