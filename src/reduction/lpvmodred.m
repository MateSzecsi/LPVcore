function [sysr, info] = lpvmodred(sys, rx, rd, method, methodOpts)
%LPVMODRED State and parameter reduction
%
% Computes a reduced-order approximation of a given LPV model object.
% Syntax:
%
%   [sysr, info] = lpvmodred(sys, rx, rd, method, methodOpts)
%
% Inputs:
%   sys: LPV state-space model object (specified as an LPVCORE.LPVSS or 
%       LPVLFR object).
%   rx: integer denoting the maximum state-order of the G block (see
%       diagram below). Set to empty or Inf to leave state-order unchanged.
%       Note that the actual state-order of reduced-order model returned by
%       this function can be smaller than rx (for example, with the
%       'momentmatching' reduction method).
%   rd: integer denoting the maximum size of the Delta block (see diagram
%       below). Set to empty or Inf to leave size of Delta block
%       unchanged.
%   method: character vector or string denoting the method. Possible
%       choices:
%         State reduction:
%           'ltibalred' (default): balanced reduction of LTI part (G).
%           'momentmatching': moment-matching reduction for state-space models.
%         Delta block reduction:
%           'deltasvdred': SVD-based reduction of Delta block for LPV-SS models.
%           'deltabalred': balanced reduction of Delta block for models
%               with squares Delta blocks. Based on interchanging role of
%               state and latent variables (see documentation below).
%   methodOpts: options object for the chosen method:
%           'ltibalred': pass a BALREDOPTIONS object
%           'deltasvdred': not used
%           'deltabalred': pass a BALREDOPTIONS object
%           'momentmatching': pass a struct with the following options:
%               'mode': passed as 'mode' input arguments to LPVMMRED. See LPVMMRED documentation.
%               'verbose': logical scalar denoting whether to print the
%                   iterative procedure determining the maximum number of
%                   time steps that can be matched to produce a ROM of the
%                   specified state dimension.
%
% Outputs:
%   rsys: reduced-order LPV state-space model
%   info: structure containing details on the reduction procedure. The
%       exact content of the structure depends on the selected reduction
%       method:
%           'ltibalred': the same 'info' structure returned by the LTI
%               method BALRED. See doc BALRED for details.
%           'deltasvdred':
%               'DeltaSingularValues': a column vector of size
%                   N*(nx + min(ny,nu)) estimating the contribution of
%                   each row/column of the Delta block on the dynamics of
%                   the system. Can be used to find an appropriate
%                   reduction order for the Delta block.
%           'deltabalred': contains the following fields:
%               'DeltaHankelSingularValues': a column vector of size nz
%                   containing the Hankel Sinulgar Values (HSV) of the
%                   system obtained by interchanging the role of the state
%                   and latent variable (for more information, see
%                   documentation on the method below).
%               'T' / 'Ti': the projection matrices describing the projection of the
%                   Delta block as Deltar = Ti * Delta * T, with the other
%                   state matrices of G appropriately projected. For
%                   example: Dzu_r = Ti * Dzu.
%           'momentmatching': contains the following fields:
%               'NumStepsMatched': the number of time steps for which the full-order
%                   and reduced-order model exactly match in terms of
%                   input-output behavior, if excited with the same
%                   scheduling sequence. Only for discrete-time models.
%
% After reduction, the order rx and rd denote the maximum size of the
% state-space model in terms of the following LFR form:
%
%            ┌──────────┐           
%       ┌────│ Delta(p) │◀───┐ 
%       │    │ (size rd)│    │
%       │    └──────────┘    │      
%     w │                    │ z    
%       │    ┌──────────┐    │      
%       └───▶│    G     │────┘      
%            │(state    │           
%  u ───────▶│  dim rx) │────────▶ y
%            └──────────┘           
%
% Algorithm details:
%
%   'momentmatching': Algorithm 3 from "Moment Matching Based Model Reduction for LPV
%       State-Space Systems" by M. Bastug et al. (2015)
%
%   'deltasvdred': Singular Value Decomposition (SVD) based reduction of Delta
%       block for LPVCORE.LPVSS model objects.
%
% This algorithm is only available for LPVCORE.LPVSS model objects, i.e., 
% LPVLFR model objects for which Dzw = 0. For an LPVCORE.LPVSS object, the 
% state-space coefficients of the system representation can be expressed in
% the following form:
%
%   [A, B; C, D](rho) = S0 + S1 * phi1(rho) + ... + SN * phiN(rho)
%
% with phi1, ..., phiN the scheduling-dependent basis functions and S1,
% ..., SN the matrix coefficients corresponding to the scheduling dependent dynamics.
% S0 is the matrix coefficient corresponding to the scheduling independent
% dynamics. As such, it does not contribute to the size of the Delta block.
% By default, the Delta block
% size corresponding to this form is N*(nx + nu)-by-N*(nx + ny). However,
% this size can be reduced by embedding the LPVSS dynamics into an
% approximate LPVLFR form based on an SVD of each S1, ..., SN:
%
%   Si * phi1(rho) = Si_left * phii(rho) * (Sigmai * Si_right)
%                    -------               -------------------
%                       Li                          Ri
%
% with [Si_left, Si_right, Sigmai] an SVD of Si. Truncation of the smallest
% singular values of the SVD form then leads to an approximation with a
% smaller Delta block size. When reducing the Delta block size from its
% original size to the requested size, the algorithm first calculates all
% singular values of each block Si and concatenates the results into a
% single vector of singular values. Based on this vector and the requested
% max. Delta block size, a cut-off threshold is selected. Subsequently,
% another iteration over the blocks S1, ..., SN is performed, truncating
% any singular values below the threshold.
%
% Note A current limitation of the implementation is that
% it is possible that the size of the Delta block is smaller than
% the requested size rd if multiple Si blocks share the same singular
% values.
%
%   'deltabalred': Delta block reduction based on interchanging order of
%       state and latent variables
%
% This heuristic method applies the standard balanced reduction technique
% of LTI systems to a transformed system that interchanges the role of the
% state and latent variables. Disregarding the external inputs and outputs,
% the evolution of the state (x) can be described by the following system of
% algebraic-differential equations:
% 
%   [xdot; z] = [A0, Bw; Cz, Dzw] * [x; w]
%           w = Delta(p) * z
%
% To reduce the size of the Delta block, we consider the alternate system
% obtained by disconnected the Delta block, disconnecting the state integrator,
% and connecting an integrator linking w and z as follows:
%
%   [z; xy] = [Dzw, Cz; Bw, A0] * [w; xu]
%      wdot = z
%
% with xy, xu now exogeneous outputs and inputs, respectively, and the
% evolution of the latent variable w considered the state. Note that this
% requires that the dimensions of z and w are equal. In other words: the
% Delta block must be square. Finally, balanced truncation is performed on
% the above alternate system to reduce the dimension of the new state w,
% i.e.:
%
%   w ~ T * wr
%
% with T an nz-by-rd matrix and Ti * T = I_{rd}.
% The Delta block is projected according to this projection matrix:
%
%   Deltar = Ti * Delta * T
%

%% Input validation
narginchk(1, 5);
nargoutchk(0, 2);
assert(isa(sys, 'lpvlfr'), '''sys'' must be an ''lpvlfr'' object');
if nargin <= 1 || isempty(rx) || isinf(rx)
    rx = sys.Nx;
end
if nargin <= 2 || isempty(rd) || isinf(rd)
    rd = size(sys.Delta, 1);
end
assert(isint(rd) && rd >= 0, ...
    '''rd'' must be empty or a non-negative integer');
assert(isint(rx) && rx >= 0, ...
    '''rx'' must be empty or a non-negative integer');
% Method
if nargin <= 3
    method = 'ltibalred';
end
assert(ismember(method, {'ltibalred', 'momentmatching', 'deltasvdred', 'deltabalred'}), ...
    'Unsupported reduction method ''%s'', method');
% Options
if nargin <= 4 || isempty(methodOpts)
    if ismember(method, {'ltibalred', 'deltabalred'})
        methodOpts = balredOptions;
    end
    if ismember(method, 'momentmatching')
        methodOpts = struct('mode', 'T', 'verbose', true);
    end
end
if ismember(method, {'ltibalred', 'deltabalred'})
    assert(isa(methodOpts, 'ltioptions.balred'), ...
        '''methodOpts'' must be an instance of ''balredOptions'' for ''method''=''%s''', method);
end


%% Reduction
if strcmp(method, 'ltibalred')
    G = sys.G;
    [Gr, info] = balred(G, rx, methodOpts);
    sysr = sys;
    sysr.G = Gr;
elseif strcmp(method, 'momentmatching')
    verbose = getfielddef(methodOpts, 'verbose', true);
    mode = getfielddef(methodOpts, 'mode', 'T');
    % Loop over number of moments to match
    for N=1:rx
        if verbose; tic; fprintf('Calling LPVMMRED with N = %i... ', N); end
        sysr_candidate = lpvmmred(sys, N, mode);
        if verbose; toc; fprintf('\tResulting Nx: %i\t(max: %i)\n', ...
                sysr_candidate.Nx, rx); end
        if sysr_candidate.Nx <= rx
            % State dimension constraint not violated: update ROM
            sysr = sysr_candidate;
        else
            % State dimension constraint violated: stop search
            N = N - 1; %#ok<FXSET>
            if N > 0
                if verbose
                    fprintf('Stopping iterative search since Nx exceeds rx.\nFinal ROM dimension: %i\n', sysr.Nx);
                end
            else
                error('No reduced-order model found without exceeding rx.\nTry again with rx >= %i or use a different method.\n', sysr_candidate.Nx);
            end
            break;
        end
    end
    info = struct();
    if isdt(sys)
        info.NumStepsMatched = N;
    end
elseif strcmp(method, 'deltasvdred')
    assert(isa(sys, 'LPVcore.lpvss'), '''sys'' must be an ''LPVcore.lpvss'' object for method ''svddeltared''');
    % Find singular values
    [~, ~, DeltaSingularValues] = lpvss2lfr(sys.A, sys.B, sys.C, sys.D, sys.Ts, 0);
    info = struct('DeltaSingularValues', DeltaSingularValues);
    % Based on singular values, determine threshold
    threshold = DeltaSingularValues(rd) - eps;
    [Deltar, Gr] = lpvss2lfr(sys.A, sys.B, sys.C, sys.D, sys.Ts, threshold);
    sysr = lpvlfr(Deltar, Gr);
    sysr = LPVcore.copySignalProperties(sysr, sys, 'io');
elseif strcmp(method, 'deltabalred')
    assert(issquare(sys.Delta), '''sys'' must have a square Delta block for method ''deltabalred''');
    Delta = sys.Delta;
    Dzw = sys.Dzw;
    Dyu = sys.Dyu;
    Dyw = sys.Dyw;
    Dzu = sys.Dzu;
    Cz = sys.Cz;
    Cy = sys.Cy;
    Bw = sys.Bw;
    Bu = sys.Bu;
    A0 = sys.A0;
    S = ss(Dzw, Cz, Bw, A0);
    [~, HSV, T, Ti] = balreal(S);
    info.DeltaHankelSingularValues = HSV;
    info.T = T;
    info.Ti = Ti;
    T = T(:, 1:rd);
    Ti = Ti(1:rd, :);
    Dzw_r = Ti * Dzw * T;
    Dyu_r = Dyu;
    Dyw_r = Dyw * T;
    Dzu_r = Ti * Dzu;
    Cz_r = Ti * Cz;
    Cy_r = Cy;
    Bw_r = Bw * T;
    Bu_r = Bu;
    A0_r = A0;
    G_r = ss(A0_r, [Bw_r, Bu_r], [Cz_r; Cy_r], ...
        [Dzw_r, Dzu_r; Dyw_r, Dyu_r], sys.Ts);
    Delta_r = Ti * Delta * T;
    sysr = lpvlfr(Delta_r, G_r);
    sysr = LPVcore.copySignalProperties(sysr, sys, 'io');
end

end

