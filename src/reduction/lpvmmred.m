function sysr = lpvmmred(sys, N, mode)
%LPVMMRED Reduction of LPV-SS systems by moment matching
%
% Reference: [1] "Moment Matching Based Model Reduction for LPV State-Space
%   Models" (2015) by M. Bastug et al.
%
% Reduce a high-order (denoted full-order model FOM) state-space
% representation of the following form:
%
%   q x[k] = A x[k] + B u[k]
%     y[k] = C x[k] + D u[k]
%
% with A, B, C, D possibly scheduling-dependent matrix-valued functions,
% and q the time-shift operator (q x[k] = x[k+1]) in discrete-time or the
% derivative operator in continuous-time.
% See LPVSS for more details regarding this representation.
%
% The model reduction method implemented in this function returns a
% reduced-order state-space model of the same structure with lower order.
% For a discrete-time input, the reduced-order model is a so-called 
% "N-partial realization", i.e., its output matches the
% output of the FOM given the same input-scheduling data {u[k],
% p[k]}_{k=1}^{N}. Under certain rank conditions, a 2N-partial realization
% is returned if passing mode = 'T'.
%
% Note that the reduction order r and the realization horizon N are not
% necessarily equal. However, increasing the realization horizon increases
% the reduction order or keeps it constant. Thus, if the resulting ROM is
% too high-dimensional, N must be decreased.
%
% Syntax:
%   sysr = lpvmmred(sys, N)
%   sysr = lpvmmred(sys, N, mode)
%
% Inputs:
%   sys: full-order model, specified as a discrete-time LPVSS object
%   N: the numbers of moments to match. If mode = 'R' or 'O', then N is the
%       number of time samples that the ROM is guaranteed to match the FOM
%       given the same input-scheduling data. If mode = 'T', then this
%       number of samples is doubled to 2*N.
%   mode: reduction mode, one of:
%       'R': return reachable ROM which is an N-partial realization of sys.
%       'O': return observable ROM which is an N-partial realization of sys.
%       'T': return reachable and observable (=minimal) ROM. Requires the
%           following rank condition on the Krylov subspaces to hold:
%               rank(V) = rank(W) = rank(WV)
%           See Algorithm 3 in [1] for more details. If the rank condition
%           is not satisfied, then the mode defaults to 'R', returning an
%           N-partial realization of the FOM.
%

%% Input validation
narginchk(2, 3);
nargoutchk(0, 1);

assert(isa(sys, 'LPVcore.lpvss'), '''sys'' must be an LPVSS object');
assert(isint(N) && N > 0, '''N'' must be a positive integer');

if nargin <= 2
    mode = 'T';
end
assert(ismember(mode, {'T', 'O', 'R'}), 'Unsupported mode %s', mode);

%% Algorithm 1: calculate matrix representation of R_N
A = sys.A.matrices;
B = sys.B.matrices;
V = Algorithm1(A, B, N);

%% Algorithm 2: calculate matrix representation of O_N
C = sys.C.matrices;
W = Algorithm1(pagetranspose(A), pagetranspose(C), N).';

%% Algorithm 3: Moment matching for LPV-SS representations
if (rank(V) == rank(W)) && (rank(V) == rank(W * V)) && strcmp(mode, 'T')
    Ar_mat = pagemtimes(pagemtimes(W, A), V / (W * V));
    Br_mat = pagemtimes(W, B);
    Cr_mat = pagemtimes(C, V / (W * V));
elseif strcmp(mode, 'T')
    warning('Rank condition not satisfied for ''mode=T'', switching to ''mode=R''');
    mode = 'R';
end

% PAGEMLDIVIDE and PAGEMRDIVIDE were introduced in R2022a
if strcmp(mode, 'R')
    r = rank(V);
    AV = pagemtimes(A, V);
    Cr_mat = pagemtimes(C, V);
    if isMATLABReleaseOlderThan('R2022a')
        Ar_mat = NaN(r, r, size(A, 3));
        Br_mat = NaN(r, sys.Nu, size(B, 3));
        for i=1:size(AV, 3)
            Ar_mat(:, :, i) = V \ AV(:, :, i);
            Br_mat(:, :, i) = V \ B(:, :, i);
        end
    else
        Ar_mat = pagemldivide(V, AV);
        Br_mat = pagemldivide(V, B);
    end
elseif strcmp(mode, 'O')
    r = rank(W);
    if isMATLABReleaseOlderThan('R2022a')
        AdivW = NaN(sys.Nx, r, size(A, 3));
        Cr_mat = NaN(sys.Ny, r, size(C, 3));
        for i=1:size(AdivW, 3)
            AdivW(:, :, i) = A(:, :, i) / W;
            Cr_mat(:, :, i) = C(:, :, i) / W;
        end
    else
        AdivW = pagemrdivide(A, W);
        Cr_mat = pagemrdivide(C, W);
    end
    Ar_mat = pagemtimes(W, AdivW);
    Br_mat = pagemtimes(W, B);
end

%% Construct sysr
Ar = sys.A; Ar.matrices = Ar_mat;
Br = sys.B; Br.matrices = Br_mat;
Cr = sys.C; Cr.matrices = Cr_mat;
Dr = sys.D;

sysr = LPVcore.lpvss(Ar, Br, Cr, Dr, sys.Ts);
sysr = LPVcore.copySignalProperties(sysr, sys);

end

function V = Algorithm1(A, B, N)
    nx = size(A, 1);
    np = size(A, 3);
    nu = size(B, 2);
    
    U0 = orth(reshape(B, [nx, np*nu]));
    V = U0;
    for k=1:N
        for l=1:np
            V = orth([V, A(:, :, l) * V]);
        end
    end
end

