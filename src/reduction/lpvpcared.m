function [sysr, mapping_fnc, info] = lpvpcared(sys, nrho, data)
%LPVPCARED PCA-based reduction of scheduling signal dimension for LPVLFR
% models [1]
%
% Reference:
%   This function implements the algorithm in Section 3 ("Parameter set
%   mapping") of:
%       [1] "Affine linear parameter-varying embedding of non-linear models with
%   improved accuracy and minimal overbounding" by A. Sadeghzadeh, B.
%   Sharif and R. Toth (IET Control Theory & Applications, 2021)
%
% Syntax:
%   sysr = lpvpcared(sys, nrho, data)
%   [sysr, mapping_fnc, info] = __
%   TODO: show singular value plot if called without output arguments
%
% Inputs:
%   sys: original model (LPVLFR object)
%   nrho: dimension of reduced scheduling signal
%   data: simulation data specified as one of the following:
%         1) LPVIDDATA object with scheduling-output-input data. Only the
%           scheduling data is used.
%         2) An N-by-np numeric matrix with N the number of time samples
%           and np the number of scheduling signals.
%
% Outputs:
%   sysr: model with reduced scheduling signal dimension (LPVCORE.LPVSS object)
%   mapping_fnc: function handle that maps a scheduling signal
%       trajectory from the original model 'sys' to the reduced-complexity
%       model 'sysr'. Example usage:
%           >> [sysr, mapping_fnc] = lpvpcared(sys, 1, data)
%           >> alpha = randn(N, 10)         % Original scheduling signal
%           >> rho = mapping_fnc(alpha)     % Reduced scheduling signal
%           >> lsim(sys, alpha, ...)        % Simulate original model
%           >> lsim(sysr, rho, ...)         % Simulate reduced model
%   info: struct containing the following fields:
%       'DataMatrixSingularValues': singular values of the data matrix
%           constructed from the 'data' input argument.
%       'eta': accuracy index (16). A lower value indicates a better match
%           between the data matrix of the original model and reduced model.
%
% Algorithm: consult [1, Section 3] for algorithmic details. Here, a basic
% overview is provided:
%
%   The algorithm approximates an LPVLFR model by an LPVCORE.LPVSS model with
%   a reduced scheduling signal dimension. The basic procedure underlying
%   the technique is a data-driven projection of the scheduling signals to 
%   a lower-dimensional subspace.
%
%   The projection is determined from a data matrix constructed of
%   trajectories of state-space coefficients of the original model.
%
%       L(alpha(t)) = [A(alpha(t)), B(alpha(t));
%                      C(alpha(t)), D(alpha(t))]       (12)
%
%   Note that A, B, C, D are not necessarily affine function of alpha(t).
%   In fact, when the model is an LPVLFR object with Dzw ~= 0, they are
%   rational functions.
%
%       Gamma(alpha(t)) = vec(L(alpha(t))
%
%           with vec denoting row-wise vectorization
%
%       Pi_alpha = [Gamma(alpha(t1)), ..., Gamma(alpha(tN))]
%
%           with N the number of time domain samples.
%

% TODO: add support for non-static dependence
assert(isa(sys, 'lpvlfr'), '''sys'' must be an ''lpvlfr'' object');
assert(hasStaticSchedulingDependence(sys), ...
    '''sys'' must have static dependence on the scheduling signal');

nx = sys.Nx;
ny = sys.Ny;
nu = sys.Nu;

if isa(data, 'lpviddata')
    alpha = data.p;
else
    assert(isnumeric(data) && ismatrix(data), ...
        '''data'' must be an ''lpviddata'' object or a numeric matrix');
    alpha = data;
end

Pi_alpha = create_Pi(alpha, sys);

% Normalize Phi_alpha by subtracting mean
Pi_alpha_mean = mean(Pi_alpha, 2);
Pi_alpha_std = std(Pi_alpha, 0, 2);
% Replace 0 in vector of std deviations by eps to avoid division by 0
Pi_alpha_std(Pi_alpha_std == 0) = eps;
% Normalization function calligraphic N (19) and its inverse
Ncal = @(x) (x - Pi_alpha_mean) ./ Pi_alpha_std;
Ncal_inv = @(x) Pi_alpha_std .* x + Pi_alpha_mean;

Pi_alpha_bar = Ncal(Pi_alpha);

[U, S, ~] = svd(Pi_alpha_bar, 'econ', 'vector');

Urho = U(:, 1:nrho);

% Extract constant part from Phi_alpha_mean
L0_ = reshape(Pi_alpha_mean, nx+ny, nx+nu);
A = L0_(1:nx, 1:nx);
B = L0_(1:nx, nx+1:end);
C = L0_(nx+1:end, 1:nx);
D = L0_(nx+1:end, nx+1:end);
% Each column in Urho corresponds to the state-space coefficients of a
% reduced scheduling signal as can be seen from (23).
for i=1:nrho
    Gammarho_ = Pi_alpha_std .* Urho(:, i);
    Lrho_ = reshape(Gammarho_, nx+ny, nx+nu);  % [A, B; C, D]
    A_ = Lrho_(1:nx, 1:nx);
    B_ = Lrho_(1:nx, nx+1:end);
    C_ = Lrho_(nx+1:end, 1:nx);
    D_ = Lrho_(nx+1:end, nx+1:end);
    rhoi = preal(sprintf('rho(%i)', i), sys.SchedulingTimeMap.Domain);
    A = A + A_ * rhoi; B = B + B_ * rhoi;
    C = C + C_ * rhoi; D = D + D_ * rhoi;
end
sysr = LPVcore.lpvss(A, B, C, D, sys.Ts);
sysr = LPVcore.copySignalProperties(sysr, sys, 'full');

mapping_fnc = @(p) (Urho.' * Ncal(create_Pi(p, sys))).';  % (22)
rho = mapping_fnc(alpha);

Pi_rho = Ncal_inv(Urho * rho.');
W = diag(1 ./ (Pi_alpha_std));

eta = norm(W * (Pi_alpha - Pi_rho), 'fro');  % (16)

info = struct('eta', eta, ...
    'DataMatrixSingularValues', S);

end

function Pi = create_Pi(p, sys)
    N = size(p, 1);
    nz = size(sys.Delta, 2);

    % The calculation for LPVSS and LPVLFR differs to avoid loss of
    % precision if LPVSS state-space coefficient values are calculated from
    % an LPVLFR representation (involves SVD with possible truncations)
    if isa(sys, 'LPVcore.lpvss')
        A = sys.A; B = sys.B;
        C = sys.C; D = sys.D;

        L = [A, B; C, D];

        L_ = feval(L, p);
        Pi = reshape(L_, [], N);
    else
        A0 = sys.A0; Bw  = sys.Bw;  Bu  = sys.Bu;
        Cz = sys.Cz; Dzw = sys.Dzw; Dzu = sys.Dzu;
        Cy = sys.Cy; Dyw = sys.Dyw; Dyu = sys.Dyu;

        Delta_ = feval(sys.Delta, p);

        IDzwDelta = eye(nz) - pagemtimes(Dzw, Delta_);
        BwDelta   = pagemtimes(Bw,Delta_);
        DywDelta  = pagemtimes(Dyw, Delta_);

        BwDelta_IDzwDelta  = pagemrdivide(BwDelta,  IDzwDelta);
        DywDelta_IDzwDelta = pagemrdivide(DywDelta, IDzwDelta);

        A_ = A0  + pagemtimes(BwDelta_IDzwDelta,  Cz);
        B_ = Bu  + pagemtimes(BwDelta_IDzwDelta,  Dzu);
        C_ = Cy  + pagemtimes(DywDelta_IDzwDelta, Cz);
        D_ = Dyu + pagemtimes(DywDelta_IDzwDelta, Dzu);

        L_ = [A_, B_; C_, D_];
        Pi = reshape(L_, [], N);
    end
end

