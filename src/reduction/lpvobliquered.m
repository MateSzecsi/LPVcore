function [sysr, info] = lpvobliquered(sys, rx, opts)
%LPVOBLIQUERED Model order reduction by parameter-varying oblique
%projection
%
% This function implements the LPV model reduction technique proposed in:
%
%   [1] "LPV Model Order Reduction by Parameter-Varying Oblique Projection"
%       by J. Theis, P. Seiler and H. Werner (2018)
%
% Syntax:
%   sysr = lpvobliquered(sys, rx)
%   sysr = lpvobliquered(sys, rx, opts)
%   [sysr, info] = lpvobliquered(__)
%
% Inputs:
%   sys: LPVGRIDSS object containing frozen models and their corresponding
%       operating points. The reduction reduces separately each frozen
%       model in a coherent manner, such that the returned reduced-order
%       frozen models can be interpolated in the same way as sys (e.g.,
%       through piecewise-linear interpolation).
%   rx: integer denoting the reduction order.
%   opts: options structure:
%       'Verbose': command line output level. 0 = no output (default). 1 =
%           output.
%

%% Input validation
narginchk(2, 3);
nargoutchk(0, 2);
assert(isa(sys, 'lpvgridss'), '''sys'' must be an ''lpvgridss'' object');
assert(isint(rx) && rx > 0, '''rx'' must be a positive integer');
if nargin <= 2
    % TODO: expand
    opts = struct();
end
assert(isstruct(opts), '''opts'' must be a structure');
verbose = getfielddef(opts, 'Verbose', 0);

%% Algorithm 1 of [1]
% Number of local models
nx = sys.Nx;
nz = rx;  % rx is renamed as nz to stay consistent with [1]
ng = numel(sys.ModelArray(1, 1, :));
Lc = NaN(nx, nx, ng);
Lo = NaN(nx, nx, ng);
Qbar = NaN(nx, nz);
HSV = NaN(nx, ng);

% Loop over local models
for k=1:ng
    if verbose >= 1; fprintf('Finding Gramians of local model %i / %i\n', k, ng); end
    Lc(:, :, k) = lyapchol(sys.A(:, :, k), sys.B(:, :, k)).';
    Lo(:, :, k) = lyapchol(sys.A(:, :, k).', sys.C(:, :, k).').';
    [U, Sigma, ~] = svd(Lc(:, :, k).' * Lo(:, :, k));
    HSV(:, k) = diag(Sigma);
    [Ubar, ~, ~] = svd(Lc(:, :, k) * U(:, 1:nz));
    Qbar(:, 1+nz*(k-1):(nz*k)) = Ubar(:, 1:nz);
end

[Q, ~, ~] = svd(Qbar);
V = Q(:, 1:nz);

W = NaN(nx, nz, ng);
sysrarray = sys.ModelArray;

% Loop over local models
for k=1:ng
    if verbose >= 1; fprintf('Finding projection space of local model %i / %i\n', k, ng); end
    [Q, R] = qr(Lo(:, :, k).' * V);
    W(:, :, k) = Lo(:, :, k) * Q / (R.');
    Ared = W(:, :, k).' * sys.A(:, :, k) * V;
    Bred = W(:, :, k).' * sys.B(:, :, k);
    Cred = sys.C(:, :, k) * V;
    Dred = sys.D(:, :, k);
    sysrarray(:, :, k) = ss(Ared, Bred, Cred, Dred, sys.Ts);
end

info = struct('HSV', HSV);
sysr = lpvgridss(sysrarray, sys.SamplingGrid);

end

