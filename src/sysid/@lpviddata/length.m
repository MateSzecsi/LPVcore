function value = length(obj)
%LENGTH The number of data points in the data set
%
%   m = length(A), returns a scalar number containing the
%   number of data points in the data set
%
%   See also LPVIDDATA/SIZE, LPVIDDATA

% 10/11/2018
%
% (c) Copyright 2018 - Control systems group, University of Eindhoven
% dr. Roland Toth (r.toth@tue.nl).
% 
% Original developer of the script: dr. Pepijn B. Cox (p.b.cox@tue.nl).

    value = obj.lengthDataSet;
end