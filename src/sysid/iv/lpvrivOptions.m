classdef lpvrivOptions < lpvsysidOptions
%LPVRIVOPTIONS  Creates option set for the LPVRIV command.
%
%   OPT = lpvrivOptions returns the default options for LPVRIV.
%
%   OPT = lpvrivOptions('Option1',Value1,'Option2',Value2,...) uses name/value
%   pairs to override the default values for 'Option1','Option2',...
%
%   The supported options are:
%
%   Initialization:  Choose between initialization methods. Specify as one of:
%       * 'template' (default): initialization using the coefficients of the
%           provided template system.
%       * 'polypre': pre-estimation using an LPV-ARX model structure.
%
%   MaxIterations: maximum number of iterations. Default: 100.
%
%   Display: whether to display the progress window ('off' or 'on').
%
%   Tolerance: tolerance on the relative variations of the parameter
%       vector. Default: 1E-4.
%
%   Implementation: Choose between two implementations:
%       * 'lpvcore' (default): implementation specifically written for
%           LPVcore. It has support for MIMO systems.
%       * 'laurain': implementation contributed to LPVcore by Vincent
%           Laurain. It only supports SISO systems, but is faster.
%
%   Focus: The type of error that is used to determine whether the algorithm
%       has converged, and, if no convergence is reached, which iterate
%       to return. The error is always the average 2-norm error of one of
%       the following quantities:
%           * 'simulation' (default and required if
%           Implementation='laurain'): the simulation error.
%           * 'prediction': the 1-step ahead prediction error.
%
    
    properties
        % 'on' to display progress window, 'off' to disable
        Display
        % Max. number of iterations
        MaxIterations
        % Initialization method
        Initialization
        % Tolerance on relative variations of the estimated parameters to stop the identification
        Tolerance
        % Selection of implementation ('LPVcore' or contribution from
        % Vincent 'Laurain')
        Implementation
        % Selection of 'prediction' or 'simulation' (default) focus for
        % displaying cost and selecting best model if no convergence is
        % reached
        Focus
    end
    
    methods
        function obj = lpvrivOptions(varargin)
            %LPVRIVOPTIONS Construct an instance of this class
            %   Detailed explanation goes here
            obj.Display = 'off';
            obj.MaxIterations = 100;
            obj.Initialization = 'template';
            obj.Tolerance = 1E-4;
            obj.Implementation = 'LPVcore';
            obj.Focus = 'simulation';
            obj.CommandName = 'lpvriv';
            obj = initOptions(obj, varargin{:});
        end

        function obj = set.Initialization(obj, val)
            assert(any(strcmp(val, {'template', 'polypre'})), ...
                'Initialization method must be one of: ''template'', ''polypre''');
            obj.Initialization = val;
        end

        function obj = set.MaxIterations(obj, val)
            assert(isint(val) && val >= 0, 'MaxIterations must be a non-negative integer');
            obj.MaxIterations = val;
        end

        function obj = set.Focus(obj, val)
            assert(ismember(val, {'simulation', 'prediction'}), ...
                'Invalid value for ''Focus'': %s', val);
            obj.Focus = val;
        end
    end
end

