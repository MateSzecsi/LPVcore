function [sys, x0] = lpvssest(data, template_sys, options)
%LPVSSEST Estimating an LPV-SS model from data    
%
%
% Syntax:
%
%
%   sys = lpvssest(data, template_sys)
%       estimates an LPV-SS model with state dimension and scheduling dependency
%       the same as the provided TEMPLATE_SYS using the
%       input-scheduling-output data DATA.
%
%   sys = lpvssest(___, options)
%
%   OPTIONS specifies the options of the estimation methodology. Use the
%      "lpvssestOptions" command to configure OPTIONS.
%
%   [sys, x0] = lpvssest(__)
%
%   x0 is the estimated initial state.
%
% See also PMATRIX, LPVSSESTOPTIONS

% 10/11/2018
%
% (c) Copyright 2018 - Control systems group, University of Eindhoven
% dr. Roland Toth (r.toth@tue.nl).
% 
% Original developer of the script: dr. Pepijn B. Cox (p.b.cox@tue.nl).

    narginchk(2,inf);     % Validate correct number of inputs

    ni = nargin;
    
    if ni > 2
        if ~isa(options, 'lpvssestOptions')
            LPVcore.error('LPVcore:general:InvalidInputArgument','third','lpvssestOptions object','lpvssest');
        end
    else
        options = lpvssestOptions; 
    end

    % Create GUI if Display is on
    gui = progressgui(template_sys, data, options);

    initialization = options.Initialization;
    initializationOptions = options.InitializationOptions;
    if strcmp(initialization, 'template')
        progressmsg(gui, 'Initialization using the provided template system');
        progressmsg(gui, '');
        init_sys = template_sys;
    else
        progressmsg(gui, 'Initialization using OE method');
        % Turn off Display 
        initializationOptions.Display = 'off';
        % Construct template IO system from template state-space system
        assert(hasStaticSchedulingDependence(template_sys), ...
            '''sys'' must have static scheduling dependence for ''oe'' initialization');
        NoiseVariance = template_sys.NoiseVariance;
        Ts = template_sys.Ts;
        % Collect all basis functions
        % TODO: Use G and H as well
        commonBfuncs = pmatCellCommonBfuncs({template_sys.A, template_sys.B, template_sys.C, template_sys.D});
        bfuncs = commonBfuncs{1}.bfuncs;
        % Determine order of IO model
        ny = template_sys.Ny;
        nu = template_sys.Nu;
        Na = template_sys.Nx + 1;
        Nb = Na;
        C0 = pmatrix(zeros(ny, nu, numel(bfuncs)), bfuncs, 'SchedulingTimeMap', template_sys.SchedulingTimeMap);
        B = cell(1, Nb);
        F = cell(1, Na);
        for i=1:Na
            if i > 2
                F{i} = C0;
            else
                F{i} = eye(ny);
            end
        end
        for i=1:Nb
            B{i} = C0;
        end
        progressmsg(gui, 'Step 1/3: Estimation of LPV-OE model');
        template_sys_io = lpvidpoly([], B, [], [], F, NoiseVariance, Ts, 'ZeroIsNonFree', false);
        % Identify using OE
        init_sys_io = lpvoe(data, template_sys_io, initializationOptions);
        % Realize as state-space model (returns an LPVIDSS object with K = 0)
        progressmsg(gui, 'Step 2/3: Realization of LPV-OE model using LPVIOREAL');
        init_sys_idss = lpvioreal(init_sys_io);
        init_sys_ss = LPVcore.lpvss(init_sys_idss.A, init_sys_idss.B, init_sys_idss.C, ...
            init_sys_idss.D, init_sys_idss.Ts);
        % Reduce to desired order
        progressmsg(gui, sprintf('Step 3/3: Reducing state order (%i) to state order of provided template model structure (%i)', ...
            init_sys_ss.Nx, template_sys.Nx));
        init_sys = lpvmodred(init_sys_ss, template_sys.Nx, [], 'ltibalred');
        init_sys = LPVcore.copySignalProperties(init_sys, template_sys);
    end

    if ~isa(data, 'lpviddata')
        LPVcore.error('LPVcore:general:InvalidInputArgument','first','lpviddata object','lpvssest');
    end
    
    if ~isdt(init_sys)
        LPVcore.error('LPVcore:general:InvalidInputArgument','first','discrete-time LPV SS representation','lpvssest');
    end
    
    if init_sys.Ts ~= 1
        assert(init_sys.Ts == data.Ts, ...
            'Sampling time Ts of data and template system must match');
    end
    
    if (isempty(init_sys.A) && isempty(init_sys.B) && isempty(init_sys.K)) || isempty(init_sys.C)
        LPVcore.error('LPVcore:general:InvalidInput','The initial LPV-SS model has either no state dynamics or the state is not observed in the output (C is empty).','lpvidss/lpvssest_');
    end
    
    if ~isa(init_sys, 'LPVcore.lpvss') && ~isa(init_sys, 'lpvidss')
        LPVcore.error('LPVcore:general:InvalidInputArgument','second','LPVcore.lpvss or lpvidss object','lpvssest');
    end
    
    [~,ny,np,nu] = size(data);

    if ~isequal(size(init_sys),[ny nu]) || np ~= init_sys.SchedulingDimension
        LPVcore.error('LPVcore:general:InvalidInput','Data and model object should have equivalent input, scheduling, and output dimension','lpvssest');
    end
    
    if ny == 0
        LPVcore.error('LPVcore:general:InvalidInput','There is no output defined and, therefore, minimizing w.r.t. the output is not possible.','lpvssest');
    end
    
    if isa(init_sys, 'LPVcore.lpvss')
        nx = init_sys.Nx; ny = init_sys.Ny;
        init_sys = lpvidss(init_sys.A, init_sys.B, init_sys.C, init_sys.D, ...
            'innovation', zeros(nx, ny), []);
    end
    
    if strcmp(options.SearchMethod,'em') && strcmp(typeNoiseModel(init_sys),'innovation') && ~isempty(init_sys.K)
        LPVcore.error('LPVcore:general:InvalidInputArgument','first','lpvidss object with general noise model','lpvssest');
    elseif ~strcmp(options.SearchMethod,'em') && strcmp(typeNoiseModel(init_sys),'general')
        LPVcore.error('LPVcore:general:InvalidInputArgument','first','lpvidss object with innovation noise model','lpvssest');
    end
    
    if strcmp(typeNoiseModel(init_sys),'general') && (ispmatrix(init_sys.G) || ispmatrix(init_sys.H))
        LPVcore.error('LPVcore:general:InvalidInputArgument','first','lpvidss object with parameter independent general noise model','lpvssest');
    end

    [sys, x0] = lpvssest_(init_sys,data,options);
    
end

