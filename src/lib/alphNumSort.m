function [out, I] = alphNumSort(str)
%ALPHNUMSORT sorts string arrays or cell arrays containing characters
%alpha numerically, e.g., ["p9", "p10", "p1", "q1", "q10", "q2"], gets
%sorted as ["p1", "p9", "p10", "q1", "q2", "q10"]. With normal
%alphabetical sorting this would become ["p1", "p10", "p9", "q1", ...
%"q10", "q2"].
%
%   Syntax:
%       [strOut, I] = alphNumSort(str)
%
%   Inputs:
%       str: String array or cell array of characters arrays.
%
%   Outputs:
%       strOut: Sorted string array or cell array
%       I:  Index specifiying how the str is sorted to obtain strOut.
%

    if isempty(str)
        out = [];
        I = [];
    elseif numel(str) == 1
        out = str;
        I = 1;
    else
        % Convert to string array if cell
        if isa(str, "cell")
            str = string(str);
            cellFlag = 1;
        else
            cellFlag = 0;
        end

        %% (Normal) alphabetical sorting
        [str0, I0] = sort(str);
    
        %% Numerical sorting
        % Find solely numerical strings
        I1 = I0;
        indNum = matches(str0, digitsPattern);
    
        % Sort solely numerical strings
        [~, indI0] = sort(str2double(str0(indNum)));
    
        % Compute new sorting index
        I0num = I0(indNum);
        I1(indNum) = I0num(indI0);
    
        % Sort strings (based on computed index) 
        str1 = str(I1);
        
        %% Alphanumerical sorting
        % Find alphanumerical strings
        % See https://en.wikipedia.org/wiki/List_of_Unicode_characters
        patAlph = asManyOfPattern(characterListPattern('!', '/') | ...  % Punctuation symbols
            characterListPattern(':', '@') | ...
            characterListPattern('A', 'Z') | ...
            characterListPattern('[', '`') | ...
            characterListPattern('a', 'z') | ...
            characterListPattern('{', '~'), 1);
        patNum = digitsPattern;
        pat = patAlph + patNum;
        
        alphNumIndex = find(contains(str1, pat));
        strAlphNum = str1(alphNumIndex);
        
        % Sort alphanumerical strings
        alph = strings(1, numel(strAlphNum));
        % Loop over the alphanumerical strings
        for i=1:numel(strAlphNum)
            % For each alphanumerical string, we will only sort by the
            % first text pattern. For example:
            %   cos(q2_diff)
            %       --> "cos(q", "_diff)"    (2 patterns)
            %       --> "cos(q"              (only first pattern stored)
            alph_ = extract(strAlphNum(i), patAlph);
            alph(i) = alph_(1);
        end
        num = str2double(extract(strAlphNum, patNum));
        
        uniqueAlph = unique(alph);
        
        I2 = zeros(1,numel(strAlphNum));
        I2ind = 1;
        for i = 1:numel(uniqueAlph)
            indAlph = find(alph == uniqueAlph(i));      % index of same letter/word
            N = numel(indAlph);                         % # of numbers for specifc letter/word
            [~, i2Temp] = sort(num(indAlph));           % sort the numbers
            I2(I2ind:(I2ind+N-1)) = indAlph(i2Temp);    % save the sorting order
            I2ind = I2ind + N;                          
        end
        
        % Combine new sorting index
        I = I1;
        IalphNum = I(alphNumIndex);
        I(alphNumIndex) = IalphNum(I2);
        
        % Sort strings (based on computed index)
        out = str(I);

        % If input is a cell, output a cell
        if cellFlag
            out = cellstr(out);
        end
    end
end