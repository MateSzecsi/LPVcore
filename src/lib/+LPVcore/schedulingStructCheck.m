function ssorted = schedulingStructCheck(obj, s)
%SCHEDULINGSTRUCTCHECK Check structure array compatibility with system
% representation or other scheduling-dependent object,
% and sorts fields of structure array to match system
% representation scheduling timemap.

%
% Syntax:
%   ssorted = LPVcore.schedulingStructCheck(sys, s)
%
% Example:
%   Suppose sys has scheduling time map {'a', 'b'} and s has field names
%   {'b', 'a'}, then ssorted will have its field names permuted to match
%   {'a', 'b'}.
%
% The function can also be called for PMATRIX and TIMEMAP objects.
%

%% Input validation
narginchk(2, 2);
nargoutchk(0, 1);
assert(isa(obj, 'lpvrep') || isa(obj, 'pmatrix') || isa(obj, 'timemap'), ...
    '''obj'' must be a lpvrep/pmatrix/timemap object');
assert(isa(s, 'struct'), '''s'' must be struct array');

if isa(obj, 'lpvrep')
    tm = obj.SchedulingTimeMap;
elseif isa(obj, 'pmatrix')
    tm = obj.timemap;
elseif isa(obj, 'timemap')
    tm = obj;
end
np = numel(tm.Name);

%% Perform check
% Sort field names to match scheduling time map
names = fieldnames(s);
sortedNames = alphNumSort(names);   % sorting used by the timemap
ssorted = orderfields(s, sortedNames);
if ~isequaln(fieldnames(ssorted), fieldnames(s)) && nargout == 0
    warning('Structure array passed as input is not sorted. However, no output arguments are used.');
end
f = fieldnames(ssorted);
assert(numel(f) == np, ...
    sprintf('Number of fields in structure array (%i) must match number of scheduling signals in object (%i)\n', ...
        numel(f), np));

% Check each element of struct is a scheduling signal
for i=1:numel(f)
    assert(strcmp(f{i}, tm.Name{i}), ...
        sprintf('Field name ''%s'' is not a scheduling signal of system representation\n', f{i}));
end


end

