function [Delta, G, SingularValues] = lpvss2lfr(A, B, C, D, Ts, tol)
%LPVSS2LFR Convert LPVCORE.LPVSS representation to LPVLFR representation
%
%   Syntax:
%       [Delta, G] = lpvss2lfr(A, B, C, D, Ts)
%       [Delta, G] = lpvss2lfr(A, B, C, D, Ts, tol)
%       [Delta, G, SingularValues] = lpvss2lfr(__)
%
% Inputs:
%   A, B, C, D: state-space matrices
%   Ts: sampling time
%   tol: accuracy tolerance for the conversion.
%       If set to -1, an exact conversion is performed.
%       If set >= 0, then the conversion is based on a singular value
%           decomposition, discarding small singular values in the creation of
%           the Delta block. This can help with obtaining a minimal
%           realization.
%       Default: -1 (exact conversion).
%
% Outputs:
%   Delta, G: description of LPVLFR system in terms of Delta block
%       (pmatrix) and LTI block (G)
%   SingularValues: vector of doubles sorted in decreasing order containing all
%       singular values of the matrix coefficients of [A, B; C, D]
%       (excluding the constant term). Can be used to determine the cut-off
%       threshold for Delta block reduction. Only returned if tol >= 0.
%

%
%   Algorithm outline:
%
%   Matrices of lpvss depend on p as follows:
%
%       S = S0 + S1 * phi1(p) + ... + S(N-1) * phi(N-1)(p)
%
%   We want to write this expression in lpvlfr format, i.e.:
%
%       S = S0 + SL * Delta(p) * SR
%
%   To achieve this, we use SVD of S1, ..., S(N-1) in case tol > 0:
%
%       S1 * phi1(p) = SL1 * (phi1(p) * I) * SR1
%
%   By discarding singular values smaller than tol, dimensionality
%   of z & w can be reduced. If tol = 0, then we simply take the original
%   matrix to the left:
%
%       S1 + phi1(p) = S1 * (phi1(p) * I) * I
%

if nargin <= 5
    tol = -1;
end

S = [A, B; ...
    C, D];

[n, m, N] = size(S);
S0 = zeros(n, m);

% TODO: optimize allocation of SL, SR
SL = [];
SR = [];
SingularValues = [];
% Count number of non-constant scheduling dependent basis functions
% Either N or N - 1 if S is simplified
N_nonconst = sum(cellfun(@(x) ~isa(x, 'pbconst'), S.bfuncs));

% "idx" is incremented for each non-constant scheduling dependent basis
% function encountered, since constant basis functions do not contribute to
% Delta.
idx = 1;
% "s" stores the nr. of singular values associated with the matrix
% coefficient of each non-constant basis function.
s = NaN(1, N_nonconst);
% "bf" stores the non-constant basis functions of "S".
bf = cell(1, N_nonconst);

% Loop over basis functions of S.
for i=1:N
    if isa(S.bfuncs{i}, 'pbconst')
        S0 = S0 + S.matrices(:, :, i);
    else
        % Perform SVD (if tol >= 0)
        if tol >= 0
            [U, Sigma, V] = svd(S.matrices(:, :, i), 'econ');
            Sigma = diag(Sigma);  % Vectorizes Sigma (to column vector)
            SingularValues = [SingularValues; Sigma]; %#ok<AGROW> 
            % Discard small singular values
            smallSigmaIndex = find(Sigma < tol);
            U(:, smallSigmaIndex) = [];
            V(:, smallSigmaIndex) = [];
            Sigma(smallSigmaIndex) = [];
            % Diagonalize Sigma again
            Sigma = diag(Sigma);
            % Construct SL and SR
            SL = [SL, U * Sigma];   %#ok<AGROW>
            SR = [SR; V'];          %#ok<AGROW>
            % For creating Delta: store nr. of singular values
            % (for creating the matrices) and the basis function.
            s(idx) = size(Sigma, 1);
        else
            SL = [SL, S.matrices(:, :, i)]; %#ok<AGROW>
            SR = [SR; eye(size(S.matrices(:, :, i), 2))]; %#ok<AGROW>
            s(idx) = size(SR, 2);
        end
        bf{idx} = S.bfuncs{i};
        idx = idx + 1;
    end
end

SingularValues = sort(SingularValues, 1, 'descend');

% Construct Delta pmatrix object
mat = zeros(sum(s), sum(s), N_nonconst);
idx = 1;
for i=1:N_nonconst
    mat(idx:idx+s(i)-1, idx:idx+s(i)-1, i) = eye(s(i));
    idx = idx + s(i);
end
Delta = pmatrix(mat, bf, 'SchedulingTimeMap', S.timemap);

% Construct G
nx = size(A, 1);
nz = size(Delta, 1);

A = S0(1:nx, 1:nx);
Bu = S0(1:nx, nx+1:end);
Cy = S0(nx+1:end, 1:nx);
Dyu = S0(nx+1:end, nx+1:end);
if nz > 0
    Bw = SL(1:nx, :);
    Cz = SR(:, 1:nx);
    Dzw = zeros(nz);
    Dzu = SR(:, nx+1:end);
    Dyw = SL(nx+1:end, :);
else
    Bw = zeros(nx, 0);
    Cz = zeros(0, nx);
    Dzw = zeros(0, 0);
    Dzu = zeros(0, size(Bu, 2));
    Dyw = zeros(size(Cy, 1), 0);
end

G = ss(A, [Bw, Bu], [Cz; Cy], [Dzw, Dzu; Dyw, Dyu], Ts);

end
