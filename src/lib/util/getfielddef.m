function v = getfielddef(s, f, default)
%GETFIELDDEF Get field value from struct or return default 
%
% Returns value from specified field in struct, or a default value if the
% field does not exist.
%
% Syntax:
%   v = getfield(s, f, default)
%
% Inputs:
%   s: structure
%   f: field name
%   default: default value to return if field f does not exist in struct s
%
% Outputs:
%   v: requested value
%

%% Input validation
narginchk(3, 3);
nargoutchk(0, 1);
if ~(isstruct(s) && isscalar(s))
    error('''s'' must be a scalar struct');
end
if ~(ischar(f) || isstring(f))
    error('''f'' must be a field name specified as a char. vector or string');
end

%% Get value
if isfield(s, f)
    v = s.(f);
else
    v = default;
end

end