function data_mat = getScheduling(obj, data_struct)
%GETSCHEDULING View scheduling information and prepare data matrices
%
% The GETSCHEDULING function, when called with no ouput arguments, displays
% the order of the scheduling signals of a given scheduling-dependent
% object (such as LPVREP model objects, TIMEMAP objects, PMATRIX objects
% and LPVIDDATA objects):
%
%   getScheduling(obj)
%
% prints the scheduling signals that obj depends on (and their order).
% The syntax
%
%   data_mat = getScheduling(obj, data_struct)
%
% is a convenient way of preparing a data matrix (data_mat) containing
% trajectories of the scheduling signals specified in the structure
% (data_struct). Each field in data_struct must have the name of 1 of the
% scheduling signals. The function then automatically orders the fields and
% converts the structure object into a matrix that can be used for, e.g.,
% simulation. Example:
%
%   >> p = preal('p', 'dt')
%   >> q = preal('q', 'dt')
%   >> obj = rand(2) * q + rand(2) * p
%   >> getScheduling(obj) % Prints the scheduling information
%   % Create the structure (note that each trajectory must have the same
%   % length and be specified as a column vector)
%   >> data_struct = struct('q', rand(100, 1), 'p', rand(100, 1))
%   >> data_mat = getScheduling(obj, data_struct)
%   % 'data_mat' is a 100-by-2 matrix which can be used for simulation
%   >> obj_feval = feval(obj, data_mat)
%

%% Input validation
narginchk(1, 2);
nargoutchk(0, 1);

if isa(obj, 'timemap')
    tm = obj;
elseif isa(obj, 'pmatrix')
    tm = obj.timemap;
elseif isa(obj, 'lpvrep')
    tm = obj.SchedulingTimeMap;
end

if nargout == 0
    disp(tm);
end

if nargin > 1
    % TODO: add support for dynamic dependence on scheduling signals
    assert(isequal(tm.Map, 0), ...
        ['Converting data structure to data matrix is currently only supported ', ...
        'with static dependence, i.e., no shifted/differentiated scheduling signals']);
    % Check structure and sort fields by scheduling time map
    data_struct = LPVcore.schedulingStructCheck(obj, data_struct);
    field = fieldnames(data_struct);
    data_mat = [];
    for i=1:numel(field)
        data_mat = [data_mat, data_struct.(field{i})]; %#ok<AGROW>
    end
end

end

