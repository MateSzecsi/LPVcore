function r = lintdir(dirName, base)
%LINTDIR Recursively lints a directory.
%
%   Syntax:
%       r = lintdir(dirName, base)
%
%   Inputs:
%       dirName: name of the directory to lint (full or relative path).
%       base: base path to the directory.
%
%   Outputs:
%       r: boolean indicating whether linter errors were found.
%

    if nargin <= 1
        base = dirName;
    end

    r = true;
    cd(dirName);
    
    l = dir;
    for i=1:numel(l)
        if l(i).isdir && ~strcmp(l(i).name(1), '.')
            % recurse
            if ~lintdir(l(i).name, [base, '/', l(i).name])
                r = false;
            end
        elseif ~l(i).isdir && endsWith(l(i).name, '.m')
            % Ignore file "src/synthesis/general/gridbased/@gridvar/subsasgn.m"
            % In R2023a a code warning needs to be surpressed,
            % starting R2023b not. Since we also support older versions,
            % the linter can only pass by ignoring the file.
            if strcmp(l(i).name, 'subsasgn.m') ...
                    && strcmp(base, 'src/synthesis/general/gridbased/@gridvar') ...
                    && isMATLABReleaseOlderThan('R2023b')
                continue;
            end

            c = checkcode(l(i).name);
            if numel(c) ~= 0
                r = false;
                for j=1:numel(c)
                    disp(['<a href="matlab: opentoline(''', pwd, '/', l(i).name, ''', ', ...
                        int2str(c(j).line), ')">', ...
                        int2str(c(j).line), '@', base, '/', l(i).name, ...
                        '</a>: ', c(j).message]);
                end
            end
        end
    end
    
    cd ..
end
