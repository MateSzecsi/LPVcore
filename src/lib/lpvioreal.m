function sysss = lpvioreal(sysio)
%LPVIOREAL State-space realization of LPV input-output form
%
% Converts LPV input-output models to state-space form, conserving static
% dependency on the scheduling signal. Input-output model objects
% (LPVIO/LPVIDPOLY) of the form
%
%   yd(k) + A1 yd(k-1) + ... + Ana yd(k-na) = B0 u(k) + ... + Bnb u(k-nb)
%   y(k) = yd(k) + e(k)
%
% where A1, ..., Ana, B0, ..., Bnb are scheduling dependent matrix-valued
% coefficients (with static dependency), yd is the deterministic output
% and y is the model output. e = 0 for LPVIO model objects and e is white
% noise with the specified noise covariance for LPVIDPOLY objects (thus
% representing an LPV-OE model structure). Note that if na < nb, then Ai =
% 0 for i = na+1, ..., nb is used in this script.
% 
% The above input-output form is converted into state-space
% model objects (LPVCORE.LPVSS/LPVIDSS) of the form
%
%   x(k+1) = A x(k) + B u(k)
%     y(k) = C x(k) + D u(k) + e(k)
%
% with again e = 0 for LPVIO inputs.
%
% The state-space coefficients are constructed as follows:
%
%   A = [A11, A12; ...
%        0, A22]        is (na*ny)+(nb*nu) by (na*ny)+(nb*nu)
%
% with
%
%   A11 = [-A1, ..., -A(na-1), -Ana; ...
%           I, 0, ...,   0   ,   0 ; ...
%           0, I, ...,   0   ,   0 ; ...
%                       ...       
%           0, ...,      I   ,   0] with I is the ny-by-ny identity
%
%   A12 = [B1, ..., Bnb; ...
%           0, ...,  0 ; ...
%               ...   
%           0, ...,  0]
%
%   A22 = [0, ..., 0, 0; ...
%          I, ..., 0, 0; ...
%               ...
%          0, ..., I, 0] with I is the nu-by-nu identity
%
%   B = [B1.', B2.'].'
%
% with
%
%   B1 = [B0; 0; ...; 0] is na*ny by nu
%   B2 = [I;  0; ...; 0] is nb*nu by nu
%
%   C = [-A1, ..., -Ana, B1, ..., Bnb] is ny by (na*ny)+(nb*nu)
%
%   D = B0  is ny by nu
%
% Syntax:
%   sysss = lpvioreal(sysio)
%

%% Input validation
narginchk(1, 1);
nargoutchk(0, 1);
assert(isa(sysio, 'lpvio') || isa(sysio, 'lpvidpoly'), ...
    '''sysio'' must be an input-output LPV model object (LPVIO or LPVIDPOLY)');
assert(isdt(sysio), '''sysio'' must be discrete-time');
assert(sysio.Nu > 0 && sysio.Ny > 0, ...
    '''sysio'' must have at least 1 input and 1 output');
assert(hasStaticSchedulingDependence(sysio), ...
    '''sysio'' must have static scheduling dependence');
% Check if LPV-OE model structure in case of LPVIDPOLY
if isa(sysio, 'lpvidpoly')
    assert(isoe(sysio), '''sysio'' must correspond to an LPV-OE model structure');
end

%% Extract input-output coefficients
% Aio = {A1, ..., Ana} (without monic term)
% Bio = {B0, ..., Bnb}
if isa(sysio, 'lpvio')
    Aio = sysio.A(2:end);
    Bio = sysio.B;
else
    Aio = sysio.F.MatNoMonic;
    Bio = sysio.B.Mat;
end

na = numel(Aio);
nb = numel(Bio) - 1;
ny = size(sysio, 1);
nu = size(sysio, 2);

%% Construct state-space coefficients
% Avec = [A1, ..., Ana]
% Bvec = [B1, ..., Bnb]
Avec = cat(2, Aio{:});
% If na < nb, then pad Avec with 0
if na < nb
    Avec = [Avec, zeros(ny, (nb-na)*ny)];
    na = nb;
end
Bvec = cat(2, Bio{2:end});

if na > 0
    A11 = [-Avec; ...
            eye((na-1)*ny), zeros((na-1)*ny, ny)];
else
    A11 = [];
end
A12 = [Bvec; ...
        zeros((na-1)*ny, nb*nu)];
A22 = [zeros(nu, nb*nu); ...
       eye((nb-1)*nu), zeros((nb-1)*nu, nu)];
A = [     A11,     A12; ...
     zeros(nb*nu, na*ny), A22];

if na > 0
    B1 = [Bio{1}; zeros((na-1)*ny, nu)];
else
    B1 = zeros(0, nu);
end
if nb > 0
    B2 = [eye(nu); zeros((nb-1)*nu, nu)];
else
    B2 = zeros(0, nu);
end
 
B = [B1; B2];
C = [-Avec, Bvec];
D = Bio{1};

%% Create state-space model object
if isa(sysio, 'lpvio')
    sysss = LPVcore.lpvss(A, B, C, D, sysio.Ts);
else
    sysss = lpvidss(A, B, C, D, 'innovation', zeros(size(A, 1), sysio.Ny), [], ...
        sysio.NoiseVariance, sysio.Ts);
end
sysss = LPVcore.copySignalProperties(sysss, sysio, 'io');
   
end

